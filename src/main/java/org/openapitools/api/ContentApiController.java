package org.openapitools.api;

import org.openapitools.model.ContentV1BarcodesPost200Response;
import org.openapitools.model.ContentV1BarcodesPostRequest;
import org.openapitools.model.ContentV1CardsErrorListGet200Response;
import org.openapitools.model.ContentV1CardsFilterPost200Response;
import org.openapitools.model.ContentV1CardsFilterPostRequest;
import org.openapitools.model.ContentV1CardsListPost200Response;
import org.openapitools.model.ContentV1CardsListPostRequest;
import org.openapitools.model.ContentV1CardsUpdatePost200Response;
import org.openapitools.model.ContentV1CardsUpdatePostRequestInner;
import org.openapitools.model.ContentV1CardsUploadAddPostRequest;
import org.openapitools.model.ContentV1CardsUploadPostRequestInnerInner;
import org.openapitools.model.ContentV1DirectoryBrandsGet200Response;
import org.openapitools.model.ContentV1DirectoryCollectionsGet200Response;
import org.openapitools.model.ContentV1DirectoryColorsGet200Response;
import org.openapitools.model.ContentV1DirectoryConsistsGet200Response;
import org.openapitools.model.ContentV1DirectoryContentsGet200Response;
import org.openapitools.model.ContentV1DirectoryCountriesGet200Response;
import org.openapitools.model.ContentV1DirectoryKindsGet200Response;
import org.openapitools.model.ContentV1DirectorySeasonsGet200Response;
import org.openapitools.model.ContentV1DirectoryTnvedGet200Response;
import org.openapitools.model.ContentV1MediaSavePostRequest;
import org.openapitools.model.ContentV1ObjectAllGet200Response;
import org.openapitools.model.ContentV1ObjectCharacteristicsListFilterGet200Response;
import org.openapitools.model.ContentV1ObjectCharacteristicsObjectNameGet200Response;
import org.openapitools.model.ContentV1ObjectParentAllGet200Response;
import java.util.List;
import org.openapitools.model.ResponseBodyError400;
import org.openapitools.model.ResponseBodyError403;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.CookieValue;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.context.request.NativeWebRequest;

import javax.validation.constraints.*;
import javax.validation.Valid;

import java.util.List;
import java.util.Map;
import java.util.Optional;
import javax.annotation.Generated;

@Generated(value = "org.openapitools.codegen.languages.SpringCodegen", date = "2022-10-16T20:47:05.606+03:00[Europe/Moscow]")
@Controller
public class ContentApiController implements ContentApi {

    private final NativeWebRequest request;

    @Autowired
    public ContentApiController(NativeWebRequest request) {
        this.request = request;
    }

    @Override
    public Optional<NativeWebRequest> getRequest() {
        return Optional.ofNullable(request);
    }

}
