package org.openapitools.api;

import org.openapitools.model.ApiV2OrdersGet200Response;
import org.openapitools.model.ApiV2OrdersGet400Response;
import org.openapitools.model.ApiV2OrdersGetRequestInner;
import org.openapitools.model.ApiV2OrdersStickersPdfPost200Response;
import org.openapitools.model.ApiV2OrdersStickersPost200Response;
import org.openapitools.model.ApiV2StocksDelete200Response;
import org.openapitools.model.ApiV2StocksDelete200Response1;
import org.openapitools.model.ApiV2StocksDelete200Response2;
import org.openapitools.model.ApiV2StocksDeleteRequestInner;
import org.openapitools.model.ApiV2StocksDeleteRequestInner1;
import org.openapitools.model.ApiV2SuppliesGet200Response;
import org.openapitools.model.ApiV2SuppliesGet201Response;
import org.openapitools.model.ApiV2SuppliesGet400Response;
import org.openapitools.model.ApiV2SuppliesGet409Response;
import org.openapitools.model.ApiV2SuppliesIdBarcodeGet200Response;
import org.openapitools.model.ApiV2SuppliesIdOrdersGet200Response;
import org.openapitools.model.ApiV2SuppliesIdPutRequest;
import org.openapitools.model.ApiV2WarehousesGet200ResponseInner;
import org.springframework.format.annotation.DateTimeFormat;
import org.openapitools.model.DetailReportItem;
import org.openapitools.model.ExcItem;
import org.openapitools.model.IncomesItem;
import java.util.List;
import java.time.LocalDate;
import org.openapitools.model.OrdersItem;
import org.openapitools.model.RequestBodyStickers;
import org.openapitools.model.RespBodyStocks;
import org.openapitools.model.ResponseBodyStickersError;
import org.openapitools.model.SalesItem;
import org.openapitools.model.StocksItem;
import org.openapitools.model.SuppliesIdRespBody;
import org.openapitools.model.SuppliesIdRespBody2;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.CookieValue;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.context.request.NativeWebRequest;

import javax.validation.constraints.*;
import javax.validation.Valid;

import java.util.List;
import java.util.Map;
import java.util.Optional;
import javax.annotation.Generated;

@Generated(value = "org.openapitools.codegen.languages.SpringCodegen", date = "2022-10-16T20:47:05.606+03:00[Europe/Moscow]")
@Controller
public class ApiApiController implements ApiApi {

    private final NativeWebRequest request;

    @Autowired
    public ApiApiController(NativeWebRequest request) {
        this.request = request;
    }

    @Override
    public Optional<NativeWebRequest> getRequest() {
        return Optional.ofNullable(request);
    }

}
