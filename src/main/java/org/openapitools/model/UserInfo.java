package org.openapitools.model;

import java.net.URI;
import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonTypeName;
import org.openapitools.jackson.nullable.JsonNullable;
import java.time.OffsetDateTime;
import javax.validation.Valid;
import javax.validation.constraints.*;
import io.swagger.v3.oas.annotations.media.Schema;


import java.util.*;
import javax.annotation.Generated;

/**
 * Информация о клиенте.
 */

@Schema(name = "userInfo", description = "Информация о клиенте.")
@JsonTypeName("userInfo")
@Generated(value = "org.openapitools.codegen.languages.SpringCodegen", date = "2022-10-16T20:47:05.606+03:00[Europe/Moscow]")
public class UserInfo {

  @JsonProperty("userId")
  private Integer userId;

  @JsonProperty("fio")
  private String fio;

  @JsonProperty("phone")
  private String phone;

  public UserInfo userId(Integer userId) {
    this.userId = userId;
    return this;
  }

  /**
   * Идентификатор пользователя.
   * @return userId
  */
  
  @Schema(name = "userId", example = "123", description = "Идентификатор пользователя.", required = false)
  public Integer getUserId() {
    return userId;
  }

  public void setUserId(Integer userId) {
    this.userId = userId;
  }

  public UserInfo fio(String fio) {
    this.fio = fio;
    return this;
  }

  /**
   * ФИО.
   * @return fio
  */
  
  @Schema(name = "fio", example = "Иванов Иван Иванович", description = "ФИО.", required = false)
  public String getFio() {
    return fio;
  }

  public void setFio(String fio) {
    this.fio = fio;
  }

  public UserInfo phone(String phone) {
    this.phone = phone;
    return this;
  }

  /**
   * Номер телефона.
   * @return phone
  */
  
  @Schema(name = "phone", example = "79991112233", description = "Номер телефона.", required = false)
  public String getPhone() {
    return phone;
  }

  public void setPhone(String phone) {
    this.phone = phone;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    UserInfo userInfo = (UserInfo) o;
    return Objects.equals(this.userId, userInfo.userId) &&
        Objects.equals(this.fio, userInfo.fio) &&
        Objects.equals(this.phone, userInfo.phone);
  }

  @Override
  public int hashCode() {
    return Objects.hash(userId, fio, phone);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class UserInfo {\n");
    sb.append("    userId: ").append(toIndentedString(userId)).append("\n");
    sb.append("    fio: ").append(toIndentedString(fio)).append("\n");
    sb.append("    phone: ").append(toIndentedString(phone)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

