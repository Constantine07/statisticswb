package org.openapitools.model;

import java.net.URI;
import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonTypeName;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import org.openapitools.jackson.nullable.JsonNullable;
import org.openapitools.model.ApiV2OrdersStickersPost200ResponseDataInner;
import org.openapitools.jackson.nullable.JsonNullable;
import java.time.OffsetDateTime;
import javax.validation.Valid;
import javax.validation.constraints.*;
import io.swagger.v3.oas.annotations.media.Schema;


import java.util.*;
import javax.annotation.Generated;

/**
 * ApiV2OrdersStickersPost200Response
 */

@JsonTypeName("_api_v2_orders_stickers_post_200_response")
@Generated(value = "org.openapitools.codegen.languages.SpringCodegen", date = "2022-10-16T20:47:05.606+03:00[Europe/Moscow]")
public class ApiV2OrdersStickersPost200Response {

  @JsonProperty("additionalErrors")
  private JsonNullable<Object> additionalErrors = JsonNullable.undefined();

  @JsonProperty("error")
  private Boolean error;

  @JsonProperty("errorText")
  private String errorText;

  @JsonProperty("data")
  @Valid
  private List<ApiV2OrdersStickersPost200ResponseDataInner> data = null;

  public ApiV2OrdersStickersPost200Response additionalErrors(Object additionalErrors) {
    this.additionalErrors = JsonNullable.of(additionalErrors);
    return this;
  }

  /**
   * Дополнительные ошибки.
   * @return additionalErrors
  */
  
  @Schema(name = "additionalErrors", description = "Дополнительные ошибки.", required = false)
  public JsonNullable<Object> getAdditionalErrors() {
    return additionalErrors;
  }

  public void setAdditionalErrors(JsonNullable<Object> additionalErrors) {
    this.additionalErrors = additionalErrors;
  }

  public ApiV2OrdersStickersPost200Response error(Boolean error) {
    this.error = error;
    return this;
  }

  /**
   * Флаг ошибки.
   * @return error
  */
  
  @Schema(name = "error", example = "true", description = "Флаг ошибки.", required = false)
  public Boolean getError() {
    return error;
  }

  public void setError(Boolean error) {
    this.error = error;
  }

  public ApiV2OrdersStickersPost200Response errorText(String errorText) {
    this.errorText = errorText;
    return this;
  }

  /**
   * Описание ошибки.
   * @return errorText
  */
  
  @Schema(name = "errorText", example = "string", description = "Описание ошибки.", required = false)
  public String getErrorText() {
    return errorText;
  }

  public void setErrorText(String errorText) {
    this.errorText = errorText;
  }

  public ApiV2OrdersStickersPost200Response data(List<ApiV2OrdersStickersPost200ResponseDataInner> data) {
    this.data = data;
    return this;
  }

  public ApiV2OrdersStickersPost200Response addDataItem(ApiV2OrdersStickersPost200ResponseDataInner dataItem) {
    if (this.data == null) {
      this.data = new ArrayList<>();
    }
    this.data.add(dataItem);
    return this;
  }

  /**
   * Get data
   * @return data
  */
  @Valid 
  @Schema(name = "data", required = false)
  public List<ApiV2OrdersStickersPost200ResponseDataInner> getData() {
    return data;
  }

  public void setData(List<ApiV2OrdersStickersPost200ResponseDataInner> data) {
    this.data = data;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    ApiV2OrdersStickersPost200Response apiV2OrdersStickersPost200Response = (ApiV2OrdersStickersPost200Response) o;
    return Objects.equals(this.additionalErrors, apiV2OrdersStickersPost200Response.additionalErrors) &&
        Objects.equals(this.error, apiV2OrdersStickersPost200Response.error) &&
        Objects.equals(this.errorText, apiV2OrdersStickersPost200Response.errorText) &&
        Objects.equals(this.data, apiV2OrdersStickersPost200Response.data);
  }

  private static <T> boolean equalsNullable(JsonNullable<T> a, JsonNullable<T> b) {
    return a == b || (a != null && b != null && a.isPresent() && b.isPresent() && Objects.deepEquals(a.get(), b.get()));
  }

  @Override
  public int hashCode() {
    return Objects.hash(additionalErrors, error, errorText, data);
  }

  private static <T> int hashCodeNullable(JsonNullable<T> a) {
    if (a == null) {
      return 1;
    }
    return a.isPresent() ? Arrays.deepHashCode(new Object[]{a.get()}) : 31;
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class ApiV2OrdersStickersPost200Response {\n");
    sb.append("    additionalErrors: ").append(toIndentedString(additionalErrors)).append("\n");
    sb.append("    error: ").append(toIndentedString(error)).append("\n");
    sb.append("    errorText: ").append(toIndentedString(errorText)).append("\n");
    sb.append("    data: ").append(toIndentedString(data)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

