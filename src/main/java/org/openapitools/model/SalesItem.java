package org.openapitools.model;

import java.net.URI;
import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import java.math.BigDecimal;
import java.time.OffsetDateTime;
import org.springframework.format.annotation.DateTimeFormat;
import org.openapitools.jackson.nullable.JsonNullable;
import java.time.OffsetDateTime;
import javax.validation.Valid;
import javax.validation.constraints.*;
import io.swagger.v3.oas.annotations.media.Schema;


import java.util.*;
import javax.annotation.Generated;

/**
 * SalesItem
 */

@Generated(value = "org.openapitools.codegen.languages.SpringCodegen", date = "2022-10-16T20:47:05.606+03:00[Europe/Moscow]")
public class SalesItem {

  @JsonProperty("gNumber")
  private String gNumber;

  @JsonProperty("date")
  @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
  private OffsetDateTime date;

  @JsonProperty("lastChangeDate")
  @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
  private OffsetDateTime lastChangeDate;

  @JsonProperty("supplierArticle")
  private String supplierArticle;

  @JsonProperty("techSize")
  private String techSize;

  @JsonProperty("barcode")
  private String barcode;

  @JsonProperty("totalPrice")
  private BigDecimal totalPrice;

  @JsonProperty("discountPercent")
  private Integer discountPercent;

  @JsonProperty("isSupply")
  private Boolean isSupply;

  @JsonProperty("isRealization")
  private Boolean isRealization;

  @JsonProperty("promoCodeDiscount")
  private BigDecimal promoCodeDiscount;

  @JsonProperty("warehouseName")
  private String warehouseName;

  @JsonProperty("countryName")
  private String countryName;

  @JsonProperty("oblastOkrugName")
  private String oblastOkrugName;

  @JsonProperty("regionName")
  private String regionName;

  @JsonProperty("incomeID")
  private Integer incomeID;

  @JsonProperty("saleID")
  private String saleID;

  @JsonProperty("odid")
  private Integer odid;

  @JsonProperty("spp")
  private BigDecimal spp;

  @JsonProperty("forPay")
  private BigDecimal forPay;

  @JsonProperty("finishedPrice")
  private BigDecimal finishedPrice;

  @JsonProperty("priceWithDisc")
  private BigDecimal priceWithDisc;

  @JsonProperty("nmId")
  private Integer nmId;

  @JsonProperty("subject")
  private String subject;

  @JsonProperty("category")
  private String category;

  @JsonProperty("brand")
  private String brand;

  @JsonProperty("isStorno")
  private Integer isStorno;

  @JsonProperty("sticker")
  private String sticker;

  @JsonProperty("srid")
  private String srid;

  public SalesItem gNumber(String gNumber) {
    this.gNumber = gNumber;
    return this;
  }

  /**
   * Номер заказа. Объединяет все позиции одного заказа.
   * @return gNumber
  */
  @Size(max = 50) 
  @Schema(name = "gNumber", description = "Номер заказа. Объединяет все позиции одного заказа.", required = false)
  public String getgNumber() {
    return gNumber;
  }

  public void setgNumber(String gNumber) {
    this.gNumber = gNumber;
  }

  public SalesItem date(OffsetDateTime date) {
    this.date = date;
    return this;
  }

  /**
   * Дата и время продажи. Это поле соответствует параметру `dateFrom` в запросе, если параметр `flag=1`. Если часовой пояс не указан, то берется Московское время UTC+3.
   * @return date
  */
  @Valid 
  @Schema(name = "date", description = "Дата и время продажи. Это поле соответствует параметру `dateFrom` в запросе, если параметр `flag=1`. Если часовой пояс не указан, то берется Московское время UTC+3.", required = false)
  public OffsetDateTime getDate() {
    return date;
  }

  public void setDate(OffsetDateTime date) {
    this.date = date;
  }

  public SalesItem lastChangeDate(OffsetDateTime lastChangeDate) {
    this.lastChangeDate = lastChangeDate;
    return this;
  }

  /**
   * Дата и время обновления информации в сервисе. Это поле соответствует параметру `dateFrom` в запросе, если параметр `flag=0` или не указан. Если часовой пояс не указан, то берется Московское время UTC+3.
   * @return lastChangeDate
  */
  @Valid 
  @Schema(name = "lastChangeDate", description = "Дата и время обновления информации в сервисе. Это поле соответствует параметру `dateFrom` в запросе, если параметр `flag=0` или не указан. Если часовой пояс не указан, то берется Московское время UTC+3.", required = false)
  public OffsetDateTime getLastChangeDate() {
    return lastChangeDate;
  }

  public void setLastChangeDate(OffsetDateTime lastChangeDate) {
    this.lastChangeDate = lastChangeDate;
  }

  public SalesItem supplierArticle(String supplierArticle) {
    this.supplierArticle = supplierArticle;
    return this;
  }

  /**
   * Артикул поставщика
   * @return supplierArticle
  */
  @Size(max = 75) 
  @Schema(name = "supplierArticle", description = "Артикул поставщика", required = false)
  public String getSupplierArticle() {
    return supplierArticle;
  }

  public void setSupplierArticle(String supplierArticle) {
    this.supplierArticle = supplierArticle;
  }

  public SalesItem techSize(String techSize) {
    this.techSize = techSize;
    return this;
  }

  /**
   * Размер
   * @return techSize
  */
  @Size(max = 30) 
  @Schema(name = "techSize", description = "Размер", required = false)
  public String getTechSize() {
    return techSize;
  }

  public void setTechSize(String techSize) {
    this.techSize = techSize;
  }

  public SalesItem barcode(String barcode) {
    this.barcode = barcode;
    return this;
  }

  /**
   * Бар-код
   * @return barcode
  */
  @Size(max = 30) 
  @Schema(name = "barcode", description = "Бар-код", required = false)
  public String getBarcode() {
    return barcode;
  }

  public void setBarcode(String barcode) {
    this.barcode = barcode;
  }

  public SalesItem totalPrice(BigDecimal totalPrice) {
    this.totalPrice = totalPrice;
    return this;
  }

  /**
   * Цена до согласованной скидки/промо/спп. Для получения цены со скидкой можно воспользоваться формулой `priceWithDiscount = totalPrice * (1 - discountPercent/100)`
   * @return totalPrice
  */
  @Valid 
  @Schema(name = "totalPrice", description = "Цена до согласованной скидки/промо/спп. Для получения цены со скидкой можно воспользоваться формулой `priceWithDiscount = totalPrice * (1 - discountPercent/100)`", required = false)
  public BigDecimal getTotalPrice() {
    return totalPrice;
  }

  public void setTotalPrice(BigDecimal totalPrice) {
    this.totalPrice = totalPrice;
  }

  public SalesItem discountPercent(Integer discountPercent) {
    this.discountPercent = discountPercent;
    return this;
  }

  /**
   * Согласованный итоговый дисконт
   * @return discountPercent
  */
  
  @Schema(name = "discountPercent", description = "Согласованный итоговый дисконт", required = false)
  public Integer getDiscountPercent() {
    return discountPercent;
  }

  public void setDiscountPercent(Integer discountPercent) {
    this.discountPercent = discountPercent;
  }

  public SalesItem isSupply(Boolean isSupply) {
    this.isSupply = isSupply;
    return this;
  }

  /**
   * Договор поставки
   * @return isSupply
  */
  
  @Schema(name = "isSupply", description = "Договор поставки", required = false)
  public Boolean getIsSupply() {
    return isSupply;
  }

  public void setIsSupply(Boolean isSupply) {
    this.isSupply = isSupply;
  }

  public SalesItem isRealization(Boolean isRealization) {
    this.isRealization = isRealization;
    return this;
  }

  /**
   * Договор реализации
   * @return isRealization
  */
  
  @Schema(name = "isRealization", description = "Договор реализации", required = false)
  public Boolean getIsRealization() {
    return isRealization;
  }

  public void setIsRealization(Boolean isRealization) {
    this.isRealization = isRealization;
  }

  public SalesItem promoCodeDiscount(BigDecimal promoCodeDiscount) {
    this.promoCodeDiscount = promoCodeDiscount;
    return this;
  }

  /**
   * Скидка по промокоду
   * @return promoCodeDiscount
  */
  @Valid 
  @Schema(name = "promoCodeDiscount", description = "Скидка по промокоду", required = false)
  public BigDecimal getPromoCodeDiscount() {
    return promoCodeDiscount;
  }

  public void setPromoCodeDiscount(BigDecimal promoCodeDiscount) {
    this.promoCodeDiscount = promoCodeDiscount;
  }

  public SalesItem warehouseName(String warehouseName) {
    this.warehouseName = warehouseName;
    return this;
  }

  /**
   * Название склада отгрузки
   * @return warehouseName
  */
  @Size(max = 50) 
  @Schema(name = "warehouseName", description = "Название склада отгрузки", required = false)
  public String getWarehouseName() {
    return warehouseName;
  }

  public void setWarehouseName(String warehouseName) {
    this.warehouseName = warehouseName;
  }

  public SalesItem countryName(String countryName) {
    this.countryName = countryName;
    return this;
  }

  /**
   * Страна
   * @return countryName
  */
  @Size(max = 200) 
  @Schema(name = "countryName", description = "Страна", required = false)
  public String getCountryName() {
    return countryName;
  }

  public void setCountryName(String countryName) {
    this.countryName = countryName;
  }

  public SalesItem oblastOkrugName(String oblastOkrugName) {
    this.oblastOkrugName = oblastOkrugName;
    return this;
  }

  /**
   * Округ
   * @return oblastOkrugName
  */
  @Size(max = 200) 
  @Schema(name = "oblastOkrugName", description = "Округ", required = false)
  public String getOblastOkrugName() {
    return oblastOkrugName;
  }

  public void setOblastOkrugName(String oblastOkrugName) {
    this.oblastOkrugName = oblastOkrugName;
  }

  public SalesItem regionName(String regionName) {
    this.regionName = regionName;
    return this;
  }

  /**
   * Регион
   * @return regionName
  */
  @Size(max = 200) 
  @Schema(name = "regionName", description = "Регион", required = false)
  public String getRegionName() {
    return regionName;
  }

  public void setRegionName(String regionName) {
    this.regionName = regionName;
  }

  public SalesItem incomeID(Integer incomeID) {
    this.incomeID = incomeID;
    return this;
  }

  /**
   * Номер поставки (от продавца на склад)
   * @return incomeID
  */
  
  @Schema(name = "incomeID", description = "Номер поставки (от продавца на склад)", required = false)
  public Integer getIncomeID() {
    return incomeID;
  }

  public void setIncomeID(Integer incomeID) {
    this.incomeID = incomeID;
  }

  public SalesItem saleID(String saleID) {
    this.saleID = saleID;
    return this;
  }

  /**
   * Уникальный идентификатор продажи/возврата. <ul>  <li> `SXXXXXXXXXX` — продажа  <li> `RXXXXXXXXXX` — возврат  <li> `DXXXXXXXXXXX` — доплата <li> `AXXXXXXXXX` – сторно продаж (все значения полей как у продажи, но поля с суммами и кол-вом с минусом как в возврате) <li> `BXXXXXXXXX` - сторно возврата (все значения полей как у возврата, но поля с суммами и кол-вом с плюсом, в противоположность возврату) </ul> 
   * @return saleID
  */
  @Size(max = 15) 
  @Schema(name = "saleID", description = "Уникальный идентификатор продажи/возврата. <ul>  <li> `SXXXXXXXXXX` — продажа  <li> `RXXXXXXXXXX` — возврат  <li> `DXXXXXXXXXXX` — доплата <li> `AXXXXXXXXX` – сторно продаж (все значения полей как у продажи, но поля с суммами и кол-вом с минусом как в возврате) <li> `BXXXXXXXXX` - сторно возврата (все значения полей как у возврата, но поля с суммами и кол-вом с плюсом, в противоположность возврату) </ul> ", required = false)
  public String getSaleID() {
    return saleID;
  }

  public void setSaleID(String saleID) {
    this.saleID = saleID;
  }

  public SalesItem odid(Integer odid) {
    this.odid = odid;
    return this;
  }

  /**
   * Уникальный идентификатор позиции заказа. Может использоваться для поиска соответствия между заказами и продажами.
   * @return odid
  */
  
  @Schema(name = "odid", description = "Уникальный идентификатор позиции заказа. Может использоваться для поиска соответствия между заказами и продажами.", required = false)
  public Integer getOdid() {
    return odid;
  }

  public void setOdid(Integer odid) {
    this.odid = odid;
  }

  public SalesItem spp(BigDecimal spp) {
    this.spp = spp;
    return this;
  }

  /**
   * Согласованная скидка постоянного покупателя
   * @return spp
  */
  @Valid 
  @Schema(name = "spp", description = "Согласованная скидка постоянного покупателя", required = false)
  public BigDecimal getSpp() {
    return spp;
  }

  public void setSpp(BigDecimal spp) {
    this.spp = spp;
  }

  public SalesItem forPay(BigDecimal forPay) {
    this.forPay = forPay;
    return this;
  }

  /**
   * К перечислению поставщику
   * @return forPay
  */
  @Valid 
  @Schema(name = "forPay", description = "К перечислению поставщику", required = false)
  public BigDecimal getForPay() {
    return forPay;
  }

  public void setForPay(BigDecimal forPay) {
    this.forPay = forPay;
  }

  public SalesItem finishedPrice(BigDecimal finishedPrice) {
    this.finishedPrice = finishedPrice;
    return this;
  }

  /**
   * Фактическая цена заказа с учетом всех скидок
   * @return finishedPrice
  */
  @Valid 
  @Schema(name = "finishedPrice", description = "Фактическая цена заказа с учетом всех скидок", required = false)
  public BigDecimal getFinishedPrice() {
    return finishedPrice;
  }

  public void setFinishedPrice(BigDecimal finishedPrice) {
    this.finishedPrice = finishedPrice;
  }

  public SalesItem priceWithDisc(BigDecimal priceWithDisc) {
    this.priceWithDisc = priceWithDisc;
    return this;
  }

  /**
   * Цена, от которой считается вознаграждение поставщика `forpay` (с учетом всех согласованных скидок)
   * @return priceWithDisc
  */
  @Valid 
  @Schema(name = "priceWithDisc", description = "Цена, от которой считается вознаграждение поставщика `forpay` (с учетом всех согласованных скидок)", required = false)
  public BigDecimal getPriceWithDisc() {
    return priceWithDisc;
  }

  public void setPriceWithDisc(BigDecimal priceWithDisc) {
    this.priceWithDisc = priceWithDisc;
  }

  public SalesItem nmId(Integer nmId) {
    this.nmId = nmId;
    return this;
  }

  /**
   * Код WB
   * @return nmId
  */
  
  @Schema(name = "nmId", description = "Код WB", required = false)
  public Integer getNmId() {
    return nmId;
  }

  public void setNmId(Integer nmId) {
    this.nmId = nmId;
  }

  public SalesItem subject(String subject) {
    this.subject = subject;
    return this;
  }

  /**
   * Предмет
   * @return subject
  */
  @Size(max = 50) 
  @Schema(name = "subject", description = "Предмет", required = false)
  public String getSubject() {
    return subject;
  }

  public void setSubject(String subject) {
    this.subject = subject;
  }

  public SalesItem category(String category) {
    this.category = category;
    return this;
  }

  /**
   * Категория
   * @return category
  */
  @Size(max = 50) 
  @Schema(name = "category", description = "Категория", required = false)
  public String getCategory() {
    return category;
  }

  public void setCategory(String category) {
    this.category = category;
  }

  public SalesItem brand(String brand) {
    this.brand = brand;
    return this;
  }

  /**
   * Бренд
   * @return brand
  */
  @Size(max = 50) 
  @Schema(name = "brand", description = "Бренд", required = false)
  public String getBrand() {
    return brand;
  }

  public void setBrand(String brand) {
    this.brand = brand;
  }

  public SalesItem isStorno(Integer isStorno) {
    this.isStorno = isStorno;
    return this;
  }

  /**
   * Для сторно-операций `1`, для остальных `0`
   * @return isStorno
  */
  
  @Schema(name = "isStorno", description = "Для сторно-операций `1`, для остальных `0`", required = false)
  public Integer getIsStorno() {
    return isStorno;
  }

  public void setIsStorno(Integer isStorno) {
    this.isStorno = isStorno;
  }

  public SalesItem sticker(String sticker) {
    this.sticker = sticker;
    return this;
  }

  /**
   * Цифровое значение стикера, который клеится на товар в процессе сборки заказа по системе Маркетплейс.
   * @return sticker
  */
  
  @Schema(name = "sticker", description = "Цифровое значение стикера, который клеится на товар в процессе сборки заказа по системе Маркетплейс.", required = false)
  public String getSticker() {
    return sticker;
  }

  public void setSticker(String sticker) {
    this.sticker = sticker;
  }

  public SalesItem srid(String srid) {
    this.srid = srid;
    return this;
  }

  /**
   * Уникальный идентификатор заказа, функционально аналогичный `odid`/`rid`.  Данный параметр введен в июле'22 и в течение переходного периода может быть заполнен не во всех ответах. Примечание для работающих по системе Маркетплейс: `srid` равен `rid` в ответе на метод `GET /api/v2/orders`. 
   * @return srid
  */
  
  @Schema(name = "srid", description = "Уникальный идентификатор заказа, функционально аналогичный `odid`/`rid`.  Данный параметр введен в июле'22 и в течение переходного периода может быть заполнен не во всех ответах. Примечание для работающих по системе Маркетплейс: `srid` равен `rid` в ответе на метод `GET /api/v2/orders`. ", required = false)
  public String getSrid() {
    return srid;
  }

  public void setSrid(String srid) {
    this.srid = srid;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    SalesItem salesItem = (SalesItem) o;
    return Objects.equals(this.gNumber, salesItem.gNumber) &&
        Objects.equals(this.date, salesItem.date) &&
        Objects.equals(this.lastChangeDate, salesItem.lastChangeDate) &&
        Objects.equals(this.supplierArticle, salesItem.supplierArticle) &&
        Objects.equals(this.techSize, salesItem.techSize) &&
        Objects.equals(this.barcode, salesItem.barcode) &&
        Objects.equals(this.totalPrice, salesItem.totalPrice) &&
        Objects.equals(this.discountPercent, salesItem.discountPercent) &&
        Objects.equals(this.isSupply, salesItem.isSupply) &&
        Objects.equals(this.isRealization, salesItem.isRealization) &&
        Objects.equals(this.promoCodeDiscount, salesItem.promoCodeDiscount) &&
        Objects.equals(this.warehouseName, salesItem.warehouseName) &&
        Objects.equals(this.countryName, salesItem.countryName) &&
        Objects.equals(this.oblastOkrugName, salesItem.oblastOkrugName) &&
        Objects.equals(this.regionName, salesItem.regionName) &&
        Objects.equals(this.incomeID, salesItem.incomeID) &&
        Objects.equals(this.saleID, salesItem.saleID) &&
        Objects.equals(this.odid, salesItem.odid) &&
        Objects.equals(this.spp, salesItem.spp) &&
        Objects.equals(this.forPay, salesItem.forPay) &&
        Objects.equals(this.finishedPrice, salesItem.finishedPrice) &&
        Objects.equals(this.priceWithDisc, salesItem.priceWithDisc) &&
        Objects.equals(this.nmId, salesItem.nmId) &&
        Objects.equals(this.subject, salesItem.subject) &&
        Objects.equals(this.category, salesItem.category) &&
        Objects.equals(this.brand, salesItem.brand) &&
        Objects.equals(this.isStorno, salesItem.isStorno) &&
        Objects.equals(this.sticker, salesItem.sticker) &&
        Objects.equals(this.srid, salesItem.srid);
  }

  @Override
  public int hashCode() {
    return Objects.hash(gNumber, date, lastChangeDate, supplierArticle, techSize, barcode, totalPrice, discountPercent, isSupply, isRealization, promoCodeDiscount, warehouseName, countryName, oblastOkrugName, regionName, incomeID, saleID, odid, spp, forPay, finishedPrice, priceWithDisc, nmId, subject, category, brand, isStorno, sticker, srid);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class SalesItem {\n");
    sb.append("    gNumber: ").append(toIndentedString(gNumber)).append("\n");
    sb.append("    date: ").append(toIndentedString(date)).append("\n");
    sb.append("    lastChangeDate: ").append(toIndentedString(lastChangeDate)).append("\n");
    sb.append("    supplierArticle: ").append(toIndentedString(supplierArticle)).append("\n");
    sb.append("    techSize: ").append(toIndentedString(techSize)).append("\n");
    sb.append("    barcode: ").append(toIndentedString(barcode)).append("\n");
    sb.append("    totalPrice: ").append(toIndentedString(totalPrice)).append("\n");
    sb.append("    discountPercent: ").append(toIndentedString(discountPercent)).append("\n");
    sb.append("    isSupply: ").append(toIndentedString(isSupply)).append("\n");
    sb.append("    isRealization: ").append(toIndentedString(isRealization)).append("\n");
    sb.append("    promoCodeDiscount: ").append(toIndentedString(promoCodeDiscount)).append("\n");
    sb.append("    warehouseName: ").append(toIndentedString(warehouseName)).append("\n");
    sb.append("    countryName: ").append(toIndentedString(countryName)).append("\n");
    sb.append("    oblastOkrugName: ").append(toIndentedString(oblastOkrugName)).append("\n");
    sb.append("    regionName: ").append(toIndentedString(regionName)).append("\n");
    sb.append("    incomeID: ").append(toIndentedString(incomeID)).append("\n");
    sb.append("    saleID: ").append(toIndentedString(saleID)).append("\n");
    sb.append("    odid: ").append(toIndentedString(odid)).append("\n");
    sb.append("    spp: ").append(toIndentedString(spp)).append("\n");
    sb.append("    forPay: ").append(toIndentedString(forPay)).append("\n");
    sb.append("    finishedPrice: ").append(toIndentedString(finishedPrice)).append("\n");
    sb.append("    priceWithDisc: ").append(toIndentedString(priceWithDisc)).append("\n");
    sb.append("    nmId: ").append(toIndentedString(nmId)).append("\n");
    sb.append("    subject: ").append(toIndentedString(subject)).append("\n");
    sb.append("    category: ").append(toIndentedString(category)).append("\n");
    sb.append("    brand: ").append(toIndentedString(brand)).append("\n");
    sb.append("    isStorno: ").append(toIndentedString(isStorno)).append("\n");
    sb.append("    sticker: ").append(toIndentedString(sticker)).append("\n");
    sb.append("    srid: ").append(toIndentedString(srid)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

