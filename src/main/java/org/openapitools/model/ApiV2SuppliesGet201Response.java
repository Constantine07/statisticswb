package org.openapitools.model;

import java.net.URI;
import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonTypeName;
import org.openapitools.jackson.nullable.JsonNullable;
import java.time.OffsetDateTime;
import javax.validation.Valid;
import javax.validation.constraints.*;
import io.swagger.v3.oas.annotations.media.Schema;


import java.util.*;
import javax.annotation.Generated;

/**
 * ApiV2SuppliesGet201Response
 */

@JsonTypeName("_api_v2_supplies_get_201_response")
@Generated(value = "org.openapitools.codegen.languages.SpringCodegen", date = "2022-10-16T20:47:05.606+03:00[Europe/Moscow]")
public class ApiV2SuppliesGet201Response {

  @JsonProperty("supplyId")
  private String supplyId;

  public ApiV2SuppliesGet201Response supplyId(String supplyId) {
    this.supplyId = supplyId;
    return this;
  }

  /**
   * Get supplyId
   * @return supplyId
  */
  
  @Schema(name = "supplyId", example = "WB-GI-1234567", required = false)
  public String getSupplyId() {
    return supplyId;
  }

  public void setSupplyId(String supplyId) {
    this.supplyId = supplyId;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    ApiV2SuppliesGet201Response apiV2SuppliesGet201Response = (ApiV2SuppliesGet201Response) o;
    return Objects.equals(this.supplyId, apiV2SuppliesGet201Response.supplyId);
  }

  @Override
  public int hashCode() {
    return Objects.hash(supplyId);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class ApiV2SuppliesGet201Response {\n");
    sb.append("    supplyId: ").append(toIndentedString(supplyId)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

