package org.openapitools.model;

import java.net.URI;
import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonTypeName;
import java.util.ArrayList;
import java.util.List;
import org.openapitools.model.ApiV2StocksDelete200Response1DataErrorInner;
import org.openapitools.jackson.nullable.JsonNullable;
import java.time.OffsetDateTime;
import javax.validation.Valid;
import javax.validation.constraints.*;
import io.swagger.v3.oas.annotations.media.Schema;


import java.util.*;
import javax.annotation.Generated;

/**
 * ApiV2StocksDelete200Response1Data
 */

@JsonTypeName("_api_v2_stocks_delete_200_response_1_data")
@Generated(value = "org.openapitools.codegen.languages.SpringCodegen", date = "2022-10-16T20:47:05.606+03:00[Europe/Moscow]")
public class ApiV2StocksDelete200Response1Data {

  @JsonProperty("error")
  @Valid
  private List<ApiV2StocksDelete200Response1DataErrorInner> error = null;

  public ApiV2StocksDelete200Response1Data error(List<ApiV2StocksDelete200Response1DataErrorInner> error) {
    this.error = error;
    return this;
  }

  public ApiV2StocksDelete200Response1Data addErrorItem(ApiV2StocksDelete200Response1DataErrorInner errorItem) {
    if (this.error == null) {
      this.error = new ArrayList<>();
    }
    this.error.add(errorItem);
    return this;
  }

  /**
   * Get error
   * @return error
  */
  @Valid 
  @Schema(name = "error", required = false)
  public List<ApiV2StocksDelete200Response1DataErrorInner> getError() {
    return error;
  }

  public void setError(List<ApiV2StocksDelete200Response1DataErrorInner> error) {
    this.error = error;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    ApiV2StocksDelete200Response1Data apiV2StocksDelete200Response1Data = (ApiV2StocksDelete200Response1Data) o;
    return Objects.equals(this.error, apiV2StocksDelete200Response1Data.error);
  }

  @Override
  public int hashCode() {
    return Objects.hash(error);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class ApiV2StocksDelete200Response1Data {\n");
    sb.append("    error: ").append(toIndentedString(error)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

