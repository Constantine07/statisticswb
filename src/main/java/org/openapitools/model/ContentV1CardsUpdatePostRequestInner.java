package org.openapitools.model;

import java.net.URI;
import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonTypeName;
import java.util.ArrayList;
import java.util.List;
import org.openapitools.model.ContentV1CardsUpdatePostRequestInnerSizesInner;
import org.openapitools.jackson.nullable.JsonNullable;
import java.time.OffsetDateTime;
import javax.validation.Valid;
import javax.validation.constraints.*;
import io.swagger.v3.oas.annotations.media.Schema;


import java.util.*;
import javax.annotation.Generated;

/**
 * ContentV1CardsUpdatePostRequestInner
 */

@JsonTypeName("_content_v1_cards_update_post_request_inner")
@Generated(value = "org.openapitools.codegen.languages.SpringCodegen", date = "2022-10-16T20:47:05.606+03:00[Europe/Moscow]")
public class ContentV1CardsUpdatePostRequestInner {

  @JsonProperty("imtID")
  private Integer imtID;

  @JsonProperty("nmID")
  private Integer nmID;

  @JsonProperty("vendorCode")
  private String vendorCode;

  @JsonProperty("sizes")
  @Valid
  private List<ContentV1CardsUpdatePostRequestInnerSizesInner> sizes = null;

  @JsonProperty("characteristics")
  @Valid
  private List<Object> characteristics = null;

  public ContentV1CardsUpdatePostRequestInner imtID(Integer imtID) {
    this.imtID = imtID;
    return this;
  }

  /**
   * Идентификатор карточки товара (нужен для группирования НМ в одну КТ)
   * @return imtID
  */
  
  @Schema(name = "imtID", example = "85792498", description = "Идентификатор карточки товара (нужен для группирования НМ в одну КТ)", required = false)
  public Integer getImtID() {
    return imtID;
  }

  public void setImtID(Integer imtID) {
    this.imtID = imtID;
  }

  public ContentV1CardsUpdatePostRequestInner nmID(Integer nmID) {
    this.nmID = nmID;
    return this;
  }

  /**
   * Числовой идентификатор номенклатуры Wildberries
   * @return nmID
  */
  
  @Schema(name = "nmID", example = "66964219", description = "Числовой идентификатор номенклатуры Wildberries", required = false)
  public Integer getNmID() {
    return nmID;
  }

  public void setNmID(Integer nmID) {
    this.nmID = nmID;
  }

  public ContentV1CardsUpdatePostRequestInner vendorCode(String vendorCode) {
    this.vendorCode = vendorCode;
    return this;
  }

  /**
   * Вендор код, текстовый идентификатор номенклатуры поставщика
   * @return vendorCode
  */
  
  @Schema(name = "vendorCode", example = "6000000001", description = "Вендор код, текстовый идентификатор номенклатуры поставщика", required = false)
  public String getVendorCode() {
    return vendorCode;
  }

  public void setVendorCode(String vendorCode) {
    this.vendorCode = vendorCode;
  }

  public ContentV1CardsUpdatePostRequestInner sizes(List<ContentV1CardsUpdatePostRequestInnerSizesInner> sizes) {
    this.sizes = sizes;
    return this;
  }

  public ContentV1CardsUpdatePostRequestInner addSizesItem(ContentV1CardsUpdatePostRequestInnerSizesInner sizesItem) {
    if (this.sizes == null) {
      this.sizes = new ArrayList<>();
    }
    this.sizes.add(sizesItem);
    return this;
  }

  /**
   * Массив размеров для номенклатуры (для безразмерного товара все равно нужно передавать данный массив с одним элементом и нулевым размером, но с ценой и баркодом)
   * @return sizes
  */
  @Valid 
  @Schema(name = "sizes", description = "Массив размеров для номенклатуры (для безразмерного товара все равно нужно передавать данный массив с одним элементом и нулевым размером, но с ценой и баркодом)", required = false)
  public List<ContentV1CardsUpdatePostRequestInnerSizesInner> getSizes() {
    return sizes;
  }

  public void setSizes(List<ContentV1CardsUpdatePostRequestInnerSizesInner> sizes) {
    this.sizes = sizes;
  }

  public ContentV1CardsUpdatePostRequestInner characteristics(List<Object> characteristics) {
    this.characteristics = characteristics;
    return this;
  }

  public ContentV1CardsUpdatePostRequestInner addCharacteristicsItem(Object characteristicsItem) {
    if (this.characteristics == null) {
      this.characteristics = new ArrayList<>();
    }
    this.characteristics.add(characteristicsItem);
    return this;
  }

  /**
   * Массив характеристик, индивидуальный для каждой категории
   * @return characteristics
  */
  
  @Schema(name = "characteristics", example = "[{\"ТНВЭД\":[\"6403993600\"]},{\"Пол\":[\"Мужской\"]},{\"Цвет\":[\"зеленый\"]},{\"Предмет\":[\"Блузки\"]}]", description = "Массив характеристик, индивидуальный для каждой категории", required = false)
  public List<Object> getCharacteristics() {
    return characteristics;
  }

  public void setCharacteristics(List<Object> characteristics) {
    this.characteristics = characteristics;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    ContentV1CardsUpdatePostRequestInner contentV1CardsUpdatePostRequestInner = (ContentV1CardsUpdatePostRequestInner) o;
    return Objects.equals(this.imtID, contentV1CardsUpdatePostRequestInner.imtID) &&
        Objects.equals(this.nmID, contentV1CardsUpdatePostRequestInner.nmID) &&
        Objects.equals(this.vendorCode, contentV1CardsUpdatePostRequestInner.vendorCode) &&
        Objects.equals(this.sizes, contentV1CardsUpdatePostRequestInner.sizes) &&
        Objects.equals(this.characteristics, contentV1CardsUpdatePostRequestInner.characteristics);
  }

  @Override
  public int hashCode() {
    return Objects.hash(imtID, nmID, vendorCode, sizes, characteristics);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class ContentV1CardsUpdatePostRequestInner {\n");
    sb.append("    imtID: ").append(toIndentedString(imtID)).append("\n");
    sb.append("    nmID: ").append(toIndentedString(nmID)).append("\n");
    sb.append("    vendorCode: ").append(toIndentedString(vendorCode)).append("\n");
    sb.append("    sizes: ").append(toIndentedString(sizes)).append("\n");
    sb.append("    characteristics: ").append(toIndentedString(characteristics)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

