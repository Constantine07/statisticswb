package org.openapitools.model;

import java.net.URI;
import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonTypeName;
import java.util.ArrayList;
import java.util.List;
import org.openapitools.model.ApiV2StocksDelete200Response1Data;
import org.openapitools.jackson.nullable.JsonNullable;
import java.time.OffsetDateTime;
import javax.validation.Valid;
import javax.validation.constraints.*;
import io.swagger.v3.oas.annotations.media.Schema;


import java.util.*;
import javax.annotation.Generated;

/**
 * ApiV2StocksDelete200Response1
 */

@JsonTypeName("_api_v2_stocks_delete_200_response_1")
@Generated(value = "org.openapitools.codegen.languages.SpringCodegen", date = "2022-10-16T20:47:05.606+03:00[Europe/Moscow]")
public class ApiV2StocksDelete200Response1 {

  @JsonProperty("error")
  private Boolean error;

  @JsonProperty("errorText")
  private String errorText;

  @JsonProperty("additionalErrors")
  @Valid
  private List<Object> additionalErrors = null;

  @JsonProperty("data")
  private ApiV2StocksDelete200Response1Data data;

  public ApiV2StocksDelete200Response1 error(Boolean error) {
    this.error = error;
    return this;
  }

  /**
   * Get error
   * @return error
  */
  
  @Schema(name = "error", example = "true", required = false)
  public Boolean getError() {
    return error;
  }

  public void setError(Boolean error) {
    this.error = error;
  }

  public ApiV2StocksDelete200Response1 errorText(String errorText) {
    this.errorText = errorText;
    return this;
  }

  /**
   * Get errorText
   * @return errorText
  */
  
  @Schema(name = "errorText", example = "string", required = false)
  public String getErrorText() {
    return errorText;
  }

  public void setErrorText(String errorText) {
    this.errorText = errorText;
  }

  public ApiV2StocksDelete200Response1 additionalErrors(List<Object> additionalErrors) {
    this.additionalErrors = additionalErrors;
    return this;
  }

  public ApiV2StocksDelete200Response1 addAdditionalErrorsItem(Object additionalErrorsItem) {
    if (this.additionalErrors == null) {
      this.additionalErrors = new ArrayList<>();
    }
    this.additionalErrors.add(additionalErrorsItem);
    return this;
  }

  /**
   * Get additionalErrors
   * @return additionalErrors
  */
  
  @Schema(name = "additionalErrors", required = false)
  public List<Object> getAdditionalErrors() {
    return additionalErrors;
  }

  public void setAdditionalErrors(List<Object> additionalErrors) {
    this.additionalErrors = additionalErrors;
  }

  public ApiV2StocksDelete200Response1 data(ApiV2StocksDelete200Response1Data data) {
    this.data = data;
    return this;
  }

  /**
   * Get data
   * @return data
  */
  @Valid 
  @Schema(name = "data", required = false)
  public ApiV2StocksDelete200Response1Data getData() {
    return data;
  }

  public void setData(ApiV2StocksDelete200Response1Data data) {
    this.data = data;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    ApiV2StocksDelete200Response1 apiV2StocksDelete200Response1 = (ApiV2StocksDelete200Response1) o;
    return Objects.equals(this.error, apiV2StocksDelete200Response1.error) &&
        Objects.equals(this.errorText, apiV2StocksDelete200Response1.errorText) &&
        Objects.equals(this.additionalErrors, apiV2StocksDelete200Response1.additionalErrors) &&
        Objects.equals(this.data, apiV2StocksDelete200Response1.data);
  }

  @Override
  public int hashCode() {
    return Objects.hash(error, errorText, additionalErrors, data);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class ApiV2StocksDelete200Response1 {\n");
    sb.append("    error: ").append(toIndentedString(error)).append("\n");
    sb.append("    errorText: ").append(toIndentedString(errorText)).append("\n");
    sb.append("    additionalErrors: ").append(toIndentedString(additionalErrors)).append("\n");
    sb.append("    data: ").append(toIndentedString(data)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

