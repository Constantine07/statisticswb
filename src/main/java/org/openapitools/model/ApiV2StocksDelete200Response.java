package org.openapitools.model;

import java.net.URI;
import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonTypeName;
import java.util.ArrayList;
import java.util.List;
import org.openapitools.model.ApiV2StocksDelete200ResponseStocksInner;
import org.openapitools.jackson.nullable.JsonNullable;
import java.time.OffsetDateTime;
import javax.validation.Valid;
import javax.validation.constraints.*;
import io.swagger.v3.oas.annotations.media.Schema;


import java.util.*;
import javax.annotation.Generated;

/**
 * ApiV2StocksDelete200Response
 */

@JsonTypeName("_api_v2_stocks_delete_200_response")
@Generated(value = "org.openapitools.codegen.languages.SpringCodegen", date = "2022-10-16T20:47:05.606+03:00[Europe/Moscow]")
public class ApiV2StocksDelete200Response {

  @JsonProperty("total")
  private Integer total;

  @JsonProperty("stocks")
  @Valid
  private List<ApiV2StocksDelete200ResponseStocksInner> stocks = null;

  public ApiV2StocksDelete200Response total(Integer total) {
    this.total = total;
    return this;
  }

  /**
   * Get total
   * @return total
  */
  
  @Schema(name = "total", example = "1", required = false)
  public Integer getTotal() {
    return total;
  }

  public void setTotal(Integer total) {
    this.total = total;
  }

  public ApiV2StocksDelete200Response stocks(List<ApiV2StocksDelete200ResponseStocksInner> stocks) {
    this.stocks = stocks;
    return this;
  }

  public ApiV2StocksDelete200Response addStocksItem(ApiV2StocksDelete200ResponseStocksInner stocksItem) {
    if (this.stocks == null) {
      this.stocks = new ArrayList<>();
    }
    this.stocks.add(stocksItem);
    return this;
  }

  /**
   * Описание остатка товара.
   * @return stocks
  */
  @Valid 
  @Schema(name = "stocks", description = "Описание остатка товара.", required = false)
  public List<ApiV2StocksDelete200ResponseStocksInner> getStocks() {
    return stocks;
  }

  public void setStocks(List<ApiV2StocksDelete200ResponseStocksInner> stocks) {
    this.stocks = stocks;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    ApiV2StocksDelete200Response apiV2StocksDelete200Response = (ApiV2StocksDelete200Response) o;
    return Objects.equals(this.total, apiV2StocksDelete200Response.total) &&
        Objects.equals(this.stocks, apiV2StocksDelete200Response.stocks);
  }

  @Override
  public int hashCode() {
    return Objects.hash(total, stocks);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class ApiV2StocksDelete200Response {\n");
    sb.append("    total: ").append(toIndentedString(total)).append("\n");
    sb.append("    stocks: ").append(toIndentedString(stocks)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

