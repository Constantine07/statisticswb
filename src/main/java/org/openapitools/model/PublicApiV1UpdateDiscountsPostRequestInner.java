package org.openapitools.model;

import java.net.URI;
import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonTypeName;
import org.openapitools.jackson.nullable.JsonNullable;
import java.time.OffsetDateTime;
import javax.validation.Valid;
import javax.validation.constraints.*;
import io.swagger.v3.oas.annotations.media.Schema;


import java.util.*;
import javax.annotation.Generated;

/**
 * PublicApiV1UpdateDiscountsPostRequestInner
 */

@JsonTypeName("_public_api_v1_updateDiscounts_post_request_inner")
@Generated(value = "org.openapitools.codegen.languages.SpringCodegen", date = "2022-10-16T20:47:05.606+03:00[Europe/Moscow]")
public class PublicApiV1UpdateDiscountsPostRequestInner {

  @JsonProperty("discount")
  private Integer discount;

  @JsonProperty("nm")
  private Integer nm;

  public PublicApiV1UpdateDiscountsPostRequestInner discount(Integer discount) {
    this.discount = discount;
    return this;
  }

  /**
   * Размер скидки
   * @return discount
  */
  
  @Schema(name = "discount", example = "15", description = "Размер скидки", required = false)
  public Integer getDiscount() {
    return discount;
  }

  public void setDiscount(Integer discount) {
    this.discount = discount;
  }

  public PublicApiV1UpdateDiscountsPostRequestInner nm(Integer nm) {
    this.nm = nm;
    return this;
  }

  /**
   * Номенклатура
   * @return nm
  */
  
  @Schema(name = "nm", example = "1234567", description = "Номенклатура", required = false)
  public Integer getNm() {
    return nm;
  }

  public void setNm(Integer nm) {
    this.nm = nm;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    PublicApiV1UpdateDiscountsPostRequestInner publicApiV1UpdateDiscountsPostRequestInner = (PublicApiV1UpdateDiscountsPostRequestInner) o;
    return Objects.equals(this.discount, publicApiV1UpdateDiscountsPostRequestInner.discount) &&
        Objects.equals(this.nm, publicApiV1UpdateDiscountsPostRequestInner.nm);
  }

  @Override
  public int hashCode() {
    return Objects.hash(discount, nm);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class PublicApiV1UpdateDiscountsPostRequestInner {\n");
    sb.append("    discount: ").append(toIndentedString(discount)).append("\n");
    sb.append("    nm: ").append(toIndentedString(nm)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

