package org.openapitools.model;

import java.net.URI;
import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonTypeName;
import java.util.ArrayList;
import java.util.List;
import org.openapitools.model.ContentV1CardsUploadPostRequestInnerInnerSizesInner;
import org.openapitools.jackson.nullable.JsonNullable;
import java.time.OffsetDateTime;
import javax.validation.Valid;
import javax.validation.constraints.*;
import io.swagger.v3.oas.annotations.media.Schema;


import java.util.*;
import javax.annotation.Generated;

/**
 * ContentV1CardsUploadPostRequestInnerInner
 */

@JsonTypeName("_content_v1_cards_upload_post_request_inner_inner")
@Generated(value = "org.openapitools.codegen.languages.SpringCodegen", date = "2022-10-16T20:47:05.606+03:00[Europe/Moscow]")
public class ContentV1CardsUploadPostRequestInnerInner {

  @JsonProperty("vendorCode")
  private String vendorCode;

  @JsonProperty("characteristics")
  @Valid
  private List<Object> characteristics = null;

  @JsonProperty("sizes")
  @Valid
  private List<ContentV1CardsUploadPostRequestInnerInnerSizesInner> sizes = null;

  public ContentV1CardsUploadPostRequestInnerInner vendorCode(String vendorCode) {
    this.vendorCode = vendorCode;
    return this;
  }

  /**
   * Артикул новой НМ которую хотим создать в КТ
   * @return vendorCode
  */
  
  @Schema(name = "vendorCode", example = "6000000002", description = "Артикул новой НМ которую хотим создать в КТ", required = false)
  public String getVendorCode() {
    return vendorCode;
  }

  public void setVendorCode(String vendorCode) {
    this.vendorCode = vendorCode;
  }

  public ContentV1CardsUploadPostRequestInnerInner characteristics(List<Object> characteristics) {
    this.characteristics = characteristics;
    return this;
  }

  public ContentV1CardsUploadPostRequestInnerInner addCharacteristicsItem(Object characteristicsItem) {
    if (this.characteristics == null) {
      this.characteristics = new ArrayList<>();
    }
    this.characteristics.add(characteristicsItem);
    return this;
  }

  /**
   * Массив характеристик, индивидуальный для каждой категории
   * @return characteristics
  */
  
  @Schema(name = "characteristics", example = "[{\"ТНВЭД\":[\"6403993600\"]},{\"Пол\":[\"Мужской\"]},{\"Стилистика\":[\"casual\"]},{\"Предмет\":\"Платья\"}]", description = "Массив характеристик, индивидуальный для каждой категории", required = false)
  public List<Object> getCharacteristics() {
    return characteristics;
  }

  public void setCharacteristics(List<Object> characteristics) {
    this.characteristics = characteristics;
  }

  public ContentV1CardsUploadPostRequestInnerInner sizes(List<ContentV1CardsUploadPostRequestInnerInnerSizesInner> sizes) {
    this.sizes = sizes;
    return this;
  }

  public ContentV1CardsUploadPostRequestInnerInner addSizesItem(ContentV1CardsUploadPostRequestInnerInnerSizesInner sizesItem) {
    if (this.sizes == null) {
      this.sizes = new ArrayList<>();
    }
    this.sizes.add(sizesItem);
    return this;
  }

  /**
   * Массив размеров для номенклатуры (для безразмерного товара все равно нужно передавать данный массив с одним элементом и пустым Рос. размером, но с ценой и баркодом)
   * @return sizes
  */
  @Valid 
  @Schema(name = "sizes", description = "Массив размеров для номенклатуры (для безразмерного товара все равно нужно передавать данный массив с одним элементом и пустым Рос. размером, но с ценой и баркодом)", required = false)
  public List<ContentV1CardsUploadPostRequestInnerInnerSizesInner> getSizes() {
    return sizes;
  }

  public void setSizes(List<ContentV1CardsUploadPostRequestInnerInnerSizesInner> sizes) {
    this.sizes = sizes;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    ContentV1CardsUploadPostRequestInnerInner contentV1CardsUploadPostRequestInnerInner = (ContentV1CardsUploadPostRequestInnerInner) o;
    return Objects.equals(this.vendorCode, contentV1CardsUploadPostRequestInnerInner.vendorCode) &&
        Objects.equals(this.characteristics, contentV1CardsUploadPostRequestInnerInner.characteristics) &&
        Objects.equals(this.sizes, contentV1CardsUploadPostRequestInnerInner.sizes);
  }

  @Override
  public int hashCode() {
    return Objects.hash(vendorCode, characteristics, sizes);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class ContentV1CardsUploadPostRequestInnerInner {\n");
    sb.append("    vendorCode: ").append(toIndentedString(vendorCode)).append("\n");
    sb.append("    characteristics: ").append(toIndentedString(characteristics)).append("\n");
    sb.append("    sizes: ").append(toIndentedString(sizes)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

