package org.openapitools.model;

import java.net.URI;
import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonTypeName;
import java.util.Arrays;
import org.openapitools.jackson.nullable.JsonNullable;
import org.openapitools.jackson.nullable.JsonNullable;
import java.time.OffsetDateTime;
import javax.validation.Valid;
import javax.validation.constraints.*;
import io.swagger.v3.oas.annotations.media.Schema;


import java.util.*;
import javax.annotation.Generated;

/**
 * SuppliesIdRespBody
 */

@JsonTypeName("suppliesIdRespBody")
@Generated(value = "org.openapitools.codegen.languages.SpringCodegen", date = "2022-10-16T20:47:05.606+03:00[Europe/Moscow]")
public class SuppliesIdRespBody {

  @JsonProperty("additionalErrors")
  private JsonNullable<Object> additionalErrors = JsonNullable.undefined();

  @JsonProperty("data")
  private JsonNullable<Object> data = JsonNullable.undefined();

  @JsonProperty("error")
  private Boolean error;

  @JsonProperty("errorText")
  private String errorText;

  public SuppliesIdRespBody additionalErrors(Object additionalErrors) {
    this.additionalErrors = JsonNullable.of(additionalErrors);
    return this;
  }

  /**
   * Дополнительные ошибки.
   * @return additionalErrors
  */
  
  @Schema(name = "additionalErrors", description = "Дополнительные ошибки.", required = false)
  public JsonNullable<Object> getAdditionalErrors() {
    return additionalErrors;
  }

  public void setAdditionalErrors(JsonNullable<Object> additionalErrors) {
    this.additionalErrors = additionalErrors;
  }

  public SuppliesIdRespBody data(Object data) {
    this.data = JsonNullable.of(data);
    return this;
  }

  /**
   * Get data
   * @return data
  */
  
  @Schema(name = "data", required = false)
  public JsonNullable<Object> getData() {
    return data;
  }

  public void setData(JsonNullable<Object> data) {
    this.data = data;
  }

  public SuppliesIdRespBody error(Boolean error) {
    this.error = error;
    return this;
  }

  /**
   * Флаг ошибки.
   * @return error
  */
  
  @Schema(name = "error", example = "true", description = "Флаг ошибки.", required = false)
  public Boolean getError() {
    return error;
  }

  public void setError(Boolean error) {
    this.error = error;
  }

  public SuppliesIdRespBody errorText(String errorText) {
    this.errorText = errorText;
    return this;
  }

  /**
   * Описание ошибки.
   * @return errorText
  */
  
  @Schema(name = "errorText", example = "Описание ошибки", description = "Описание ошибки.", required = false)
  public String getErrorText() {
    return errorText;
  }

  public void setErrorText(String errorText) {
    this.errorText = errorText;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    SuppliesIdRespBody suppliesIdRespBody = (SuppliesIdRespBody) o;
    return Objects.equals(this.additionalErrors, suppliesIdRespBody.additionalErrors) &&
        Objects.equals(this.data, suppliesIdRespBody.data) &&
        Objects.equals(this.error, suppliesIdRespBody.error) &&
        Objects.equals(this.errorText, suppliesIdRespBody.errorText);
  }

  private static <T> boolean equalsNullable(JsonNullable<T> a, JsonNullable<T> b) {
    return a == b || (a != null && b != null && a.isPresent() && b.isPresent() && Objects.deepEquals(a.get(), b.get()));
  }

  @Override
  public int hashCode() {
    return Objects.hash(additionalErrors, data, error, errorText);
  }

  private static <T> int hashCodeNullable(JsonNullable<T> a) {
    if (a == null) {
      return 1;
    }
    return a.isPresent() ? Arrays.deepHashCode(new Object[]{a.get()}) : 31;
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class SuppliesIdRespBody {\n");
    sb.append("    additionalErrors: ").append(toIndentedString(additionalErrors)).append("\n");
    sb.append("    data: ").append(toIndentedString(data)).append("\n");
    sb.append("    error: ").append(toIndentedString(error)).append("\n");
    sb.append("    errorText: ").append(toIndentedString(errorText)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

