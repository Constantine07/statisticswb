package org.openapitools.model;

import java.net.URI;
import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonTypeName;
import org.openapitools.model.ApiV2OrdersStickersPost200ResponseDataInnerSticker;
import org.openapitools.jackson.nullable.JsonNullable;
import java.time.OffsetDateTime;
import javax.validation.Valid;
import javax.validation.constraints.*;
import io.swagger.v3.oas.annotations.media.Schema;


import java.util.*;
import javax.annotation.Generated;

/**
 * ApiV2OrdersStickersPost200ResponseDataInner
 */

@JsonTypeName("_api_v2_orders_stickers_post_200_response_data_inner")
@Generated(value = "org.openapitools.codegen.languages.SpringCodegen", date = "2022-10-16T20:47:05.606+03:00[Europe/Moscow]")
public class ApiV2OrdersStickersPost200ResponseDataInner {

  @JsonProperty("orderId")
  private Integer orderId;

  @JsonProperty("sticker")
  private ApiV2OrdersStickersPost200ResponseDataInnerSticker sticker;

  public ApiV2OrdersStickersPost200ResponseDataInner orderId(Integer orderId) {
    this.orderId = orderId;
    return this;
  }

  /**
   * Идентификатор сборочного задания.
   * @return orderId
  */
  
  @Schema(name = "orderId", example = "8423848", description = "Идентификатор сборочного задания.", required = false)
  public Integer getOrderId() {
    return orderId;
  }

  public void setOrderId(Integer orderId) {
    this.orderId = orderId;
  }

  public ApiV2OrdersStickersPost200ResponseDataInner sticker(ApiV2OrdersStickersPost200ResponseDataInnerSticker sticker) {
    this.sticker = sticker;
    return this;
  }

  /**
   * Get sticker
   * @return sticker
  */
  @Valid 
  @Schema(name = "sticker", required = false)
  public ApiV2OrdersStickersPost200ResponseDataInnerSticker getSticker() {
    return sticker;
  }

  public void setSticker(ApiV2OrdersStickersPost200ResponseDataInnerSticker sticker) {
    this.sticker = sticker;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    ApiV2OrdersStickersPost200ResponseDataInner apiV2OrdersStickersPost200ResponseDataInner = (ApiV2OrdersStickersPost200ResponseDataInner) o;
    return Objects.equals(this.orderId, apiV2OrdersStickersPost200ResponseDataInner.orderId) &&
        Objects.equals(this.sticker, apiV2OrdersStickersPost200ResponseDataInner.sticker);
  }

  @Override
  public int hashCode() {
    return Objects.hash(orderId, sticker);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class ApiV2OrdersStickersPost200ResponseDataInner {\n");
    sb.append("    orderId: ").append(toIndentedString(orderId)).append("\n");
    sb.append("    sticker: ").append(toIndentedString(sticker)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

