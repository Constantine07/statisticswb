package org.openapitools.model;

import java.net.URI;
import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonTypeName;
import org.openapitools.jackson.nullable.JsonNullable;
import java.time.OffsetDateTime;
import javax.validation.Valid;
import javax.validation.constraints.*;
import io.swagger.v3.oas.annotations.media.Schema;


import java.util.*;
import javax.annotation.Generated;

/**
 * PublicApiV1RevokeDiscountsPost200Response
 */

@JsonTypeName("_public_api_v1_revokeDiscounts_post_200_response")
@Generated(value = "org.openapitools.codegen.languages.SpringCodegen", date = "2022-10-16T20:47:05.606+03:00[Europe/Moscow]")
public class PublicApiV1RevokeDiscountsPost200Response {

  @JsonProperty("uploadId")
  private Integer uploadId;

  @JsonProperty("alreadyExists")
  private Boolean alreadyExists;

  public PublicApiV1RevokeDiscountsPost200Response uploadId(Integer uploadId) {
    this.uploadId = uploadId;
    return this;
  }

  /**
   * Номер запроса на сброс скидки
   * @return uploadId
  */
  
  @Schema(name = "uploadId", description = "Номер запроса на сброс скидки", required = false)
  public Integer getUploadId() {
    return uploadId;
  }

  public void setUploadId(Integer uploadId) {
    this.uploadId = uploadId;
  }

  public PublicApiV1RevokeDiscountsPost200Response alreadyExists(Boolean alreadyExists) {
    this.alreadyExists = alreadyExists;
    return this;
  }

  /**
   * Данное поле обозначает что скидка уже была сброшена
   * @return alreadyExists
  */
  
  @Schema(name = "alreadyExists", description = "Данное поле обозначает что скидка уже была сброшена", required = false)
  public Boolean getAlreadyExists() {
    return alreadyExists;
  }

  public void setAlreadyExists(Boolean alreadyExists) {
    this.alreadyExists = alreadyExists;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    PublicApiV1RevokeDiscountsPost200Response publicApiV1RevokeDiscountsPost200Response = (PublicApiV1RevokeDiscountsPost200Response) o;
    return Objects.equals(this.uploadId, publicApiV1RevokeDiscountsPost200Response.uploadId) &&
        Objects.equals(this.alreadyExists, publicApiV1RevokeDiscountsPost200Response.alreadyExists);
  }

  @Override
  public int hashCode() {
    return Objects.hash(uploadId, alreadyExists);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class PublicApiV1RevokeDiscountsPost200Response {\n");
    sb.append("    uploadId: ").append(toIndentedString(uploadId)).append("\n");
    sb.append("    alreadyExists: ").append(toIndentedString(alreadyExists)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

