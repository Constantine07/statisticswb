package org.openapitools.model;

import java.net.URI;
import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonTypeName;
import java.math.BigDecimal;
import org.openapitools.jackson.nullable.JsonNullable;
import java.time.OffsetDateTime;
import javax.validation.Valid;
import javax.validation.constraints.*;
import io.swagger.v3.oas.annotations.media.Schema;


import java.util.*;
import javax.annotation.Generated;

/**
 * PublicApiV1InfoGet200Response
 */

@JsonTypeName("_public_api_v1_info_get_200_response")
@Generated(value = "org.openapitools.codegen.languages.SpringCodegen", date = "2022-10-16T20:47:05.606+03:00[Europe/Moscow]")
public class PublicApiV1InfoGet200Response {

  @JsonProperty("nmId")
  private Integer nmId;

  @JsonProperty("price")
  private BigDecimal price;

  @JsonProperty("discount")
  private Integer discount;

  @JsonProperty("promoCode")
  private BigDecimal promoCode;

  public PublicApiV1InfoGet200Response nmId(Integer nmId) {
    this.nmId = nmId;
    return this;
  }

  /**
   * Номенклатура
   * @return nmId
  */
  
  @Schema(name = "nmId", example = "1234567", description = "Номенклатура", required = false)
  public Integer getNmId() {
    return nmId;
  }

  public void setNmId(Integer nmId) {
    this.nmId = nmId;
  }

  public PublicApiV1InfoGet200Response price(BigDecimal price) {
    this.price = price;
    return this;
  }

  /**
   * Цена
   * @return price
  */
  @Valid 
  @Schema(name = "price", example = "1000", description = "Цена", required = false)
  public BigDecimal getPrice() {
    return price;
  }

  public void setPrice(BigDecimal price) {
    this.price = price;
  }

  public PublicApiV1InfoGet200Response discount(Integer discount) {
    this.discount = discount;
    return this;
  }

  /**
   * Скидка
   * @return discount
  */
  
  @Schema(name = "discount", example = "10", description = "Скидка", required = false)
  public Integer getDiscount() {
    return discount;
  }

  public void setDiscount(Integer discount) {
    this.discount = discount;
  }

  public PublicApiV1InfoGet200Response promoCode(BigDecimal promoCode) {
    this.promoCode = promoCode;
    return this;
  }

  /**
   * Промокод
   * @return promoCode
  */
  @Valid 
  @Schema(name = "promoCode", example = "5", description = "Промокод", required = false)
  public BigDecimal getPromoCode() {
    return promoCode;
  }

  public void setPromoCode(BigDecimal promoCode) {
    this.promoCode = promoCode;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    PublicApiV1InfoGet200Response publicApiV1InfoGet200Response = (PublicApiV1InfoGet200Response) o;
    return Objects.equals(this.nmId, publicApiV1InfoGet200Response.nmId) &&
        Objects.equals(this.price, publicApiV1InfoGet200Response.price) &&
        Objects.equals(this.discount, publicApiV1InfoGet200Response.discount) &&
        Objects.equals(this.promoCode, publicApiV1InfoGet200Response.promoCode);
  }

  @Override
  public int hashCode() {
    return Objects.hash(nmId, price, discount, promoCode);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class PublicApiV1InfoGet200Response {\n");
    sb.append("    nmId: ").append(toIndentedString(nmId)).append("\n");
    sb.append("    price: ").append(toIndentedString(price)).append("\n");
    sb.append("    discount: ").append(toIndentedString(discount)).append("\n");
    sb.append("    promoCode: ").append(toIndentedString(promoCode)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

