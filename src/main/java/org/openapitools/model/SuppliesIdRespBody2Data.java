package org.openapitools.model;

import java.net.URI;
import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonTypeName;
import java.util.ArrayList;
import java.util.List;
import org.openapitools.jackson.nullable.JsonNullable;
import java.time.OffsetDateTime;
import javax.validation.Valid;
import javax.validation.constraints.*;
import io.swagger.v3.oas.annotations.media.Schema;


import java.util.*;
import javax.annotation.Generated;

/**
 * SuppliesIdRespBody2Data
 */

@JsonTypeName("suppliesIdRespBody2_data")
@Generated(value = "org.openapitools.codegen.languages.SpringCodegen", date = "2022-10-16T20:47:05.606+03:00[Europe/Moscow]")
public class SuppliesIdRespBody2Data {

  @JsonProperty("failedOrders")
  @Valid
  private List<String> failedOrders = null;

  public SuppliesIdRespBody2Data failedOrders(List<String> failedOrders) {
    this.failedOrders = failedOrders;
    return this;
  }

  public SuppliesIdRespBody2Data addFailedOrdersItem(String failedOrdersItem) {
    if (this.failedOrders == null) {
      this.failedOrders = new ArrayList<>();
    }
    this.failedOrders.add(failedOrdersItem);
    return this;
  }

  /**
   * Список заказов, которые не удалось обработать.
   * @return failedOrders
  */
  
  @Schema(name = "failedOrders", description = "Список заказов, которые не удалось обработать.", required = false)
  public List<String> getFailedOrders() {
    return failedOrders;
  }

  public void setFailedOrders(List<String> failedOrders) {
    this.failedOrders = failedOrders;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    SuppliesIdRespBody2Data suppliesIdRespBody2Data = (SuppliesIdRespBody2Data) o;
    return Objects.equals(this.failedOrders, suppliesIdRespBody2Data.failedOrders);
  }

  @Override
  public int hashCode() {
    return Objects.hash(failedOrders);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class SuppliesIdRespBody2Data {\n");
    sb.append("    failedOrders: ").append(toIndentedString(failedOrders)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

