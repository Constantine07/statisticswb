package org.openapitools.model;

import java.net.URI;
import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonTypeName;
import java.util.ArrayList;
import java.util.List;
import org.openapitools.model.ApiV2OrdersGet200ResponseOrdersInner;
import org.openapitools.jackson.nullable.JsonNullable;
import java.time.OffsetDateTime;
import javax.validation.Valid;
import javax.validation.constraints.*;
import io.swagger.v3.oas.annotations.media.Schema;


import java.util.*;
import javax.annotation.Generated;

/**
 * ApiV2OrdersGet200Response
 */

@JsonTypeName("_api_v2_orders_get_200_response")
@Generated(value = "org.openapitools.codegen.languages.SpringCodegen", date = "2022-10-16T20:47:05.606+03:00[Europe/Moscow]")
public class ApiV2OrdersGet200Response {

  @JsonProperty("total")
  private Integer total;

  @JsonProperty("orders")
  @Valid
  private List<ApiV2OrdersGet200ResponseOrdersInner> orders = null;

  public ApiV2OrdersGet200Response total(Integer total) {
    this.total = total;
    return this;
  }

  /**
   * Общее количество заказов по заданным параметрам (за указанный промежуток времени).
   * @return total
  */
  
  @Schema(name = "total", example = "1", description = "Общее количество заказов по заданным параметрам (за указанный промежуток времени).", required = false)
  public Integer getTotal() {
    return total;
  }

  public void setTotal(Integer total) {
    this.total = total;
  }

  public ApiV2OrdersGet200Response orders(List<ApiV2OrdersGet200ResponseOrdersInner> orders) {
    this.orders = orders;
    return this;
  }

  public ApiV2OrdersGet200Response addOrdersItem(ApiV2OrdersGet200ResponseOrdersInner ordersItem) {
    if (this.orders == null) {
      this.orders = new ArrayList<>();
    }
    this.orders.add(ordersItem);
    return this;
  }

  /**
   * Список заказов.
   * @return orders
  */
  @Valid 
  @Schema(name = "orders", description = "Список заказов.", required = false)
  public List<ApiV2OrdersGet200ResponseOrdersInner> getOrders() {
    return orders;
  }

  public void setOrders(List<ApiV2OrdersGet200ResponseOrdersInner> orders) {
    this.orders = orders;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    ApiV2OrdersGet200Response apiV2OrdersGet200Response = (ApiV2OrdersGet200Response) o;
    return Objects.equals(this.total, apiV2OrdersGet200Response.total) &&
        Objects.equals(this.orders, apiV2OrdersGet200Response.orders);
  }

  @Override
  public int hashCode() {
    return Objects.hash(total, orders);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class ApiV2OrdersGet200Response {\n");
    sb.append("    total: ").append(toIndentedString(total)).append("\n");
    sb.append("    orders: ").append(toIndentedString(orders)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

