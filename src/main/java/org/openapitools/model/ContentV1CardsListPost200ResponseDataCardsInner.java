package org.openapitools.model;

import java.net.URI;
import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonTypeName;
import java.util.ArrayList;
import java.util.List;
import org.openapitools.model.ContentV1CardsListPost200ResponseDataCardsInnerSizesInner;
import org.openapitools.jackson.nullable.JsonNullable;
import java.time.OffsetDateTime;
import javax.validation.Valid;
import javax.validation.constraints.*;
import io.swagger.v3.oas.annotations.media.Schema;


import java.util.*;
import javax.annotation.Generated;

/**
 * ContentV1CardsListPost200ResponseDataCardsInner
 */

@JsonTypeName("_content_v1_cards_list_post_200_response_data_cards_inner")
@Generated(value = "org.openapitools.codegen.languages.SpringCodegen", date = "2022-10-16T20:47:05.606+03:00[Europe/Moscow]")
public class ContentV1CardsListPost200ResponseDataCardsInner {

  @JsonProperty("sizes")
  @Valid
  private List<ContentV1CardsListPost200ResponseDataCardsInnerSizesInner> sizes = null;

  @JsonProperty("mediaFiles")
  @Valid
  private List<String> mediaFiles = null;

  @JsonProperty("colors")
  @Valid
  private List<String> colors = null;

  @JsonProperty("updateAt")
  private String updateAt;

  @JsonProperty("vendorCode")
  private String vendorCode;

  @JsonProperty("brand")
  private String brand;

  @JsonProperty("object")
  private String _object;

  @JsonProperty("nmID")
  private Integer nmID;

  public ContentV1CardsListPost200ResponseDataCardsInner sizes(List<ContentV1CardsListPost200ResponseDataCardsInnerSizesInner> sizes) {
    this.sizes = sizes;
    return this;
  }

  public ContentV1CardsListPost200ResponseDataCardsInner addSizesItem(ContentV1CardsListPost200ResponseDataCardsInnerSizesInner sizesItem) {
    if (this.sizes == null) {
      this.sizes = new ArrayList<>();
    }
    this.sizes.add(sizesItem);
    return this;
  }

  /**
   * Массив размеров для номенклатуры (для безразмерного товара все равно нужно передавать данный массив с одним элементом и  пустым Рос. размером, но с ценой и баркодом)
   * @return sizes
  */
  @Valid 
  @Schema(name = "sizes", description = "Массив размеров для номенклатуры (для безразмерного товара все равно нужно передавать данный массив с одним элементом и  пустым Рос. размером, но с ценой и баркодом)", required = false)
  public List<ContentV1CardsListPost200ResponseDataCardsInnerSizesInner> getSizes() {
    return sizes;
  }

  public void setSizes(List<ContentV1CardsListPost200ResponseDataCardsInnerSizesInner> sizes) {
    this.sizes = sizes;
  }

  public ContentV1CardsListPost200ResponseDataCardsInner mediaFiles(List<String> mediaFiles) {
    this.mediaFiles = mediaFiles;
    return this;
  }

  public ContentV1CardsListPost200ResponseDataCardsInner addMediaFilesItem(String mediaFilesItem) {
    if (this.mediaFiles == null) {
      this.mediaFiles = new ArrayList<>();
    }
    this.mediaFiles.add(mediaFilesItem);
    return this;
  }

  /**
   * Медиафайлы номенклатуры
   * @return mediaFiles
  */
  
  @Schema(name = "mediaFiles", description = "Медиафайлы номенклатуры", required = false)
  public List<String> getMediaFiles() {
    return mediaFiles;
  }

  public void setMediaFiles(List<String> mediaFiles) {
    this.mediaFiles = mediaFiles;
  }

  public ContentV1CardsListPost200ResponseDataCardsInner colors(List<String> colors) {
    this.colors = colors;
    return this;
  }

  public ContentV1CardsListPost200ResponseDataCardsInner addColorsItem(String colorsItem) {
    if (this.colors == null) {
      this.colors = new ArrayList<>();
    }
    this.colors.add(colorsItem);
    return this;
  }

  /**
   * Цвета номенклатуры
   * @return colors
  */
  
  @Schema(name = "colors", description = "Цвета номенклатуры", required = false)
  public List<String> getColors() {
    return colors;
  }

  public void setColors(List<String> colors) {
    this.colors = colors;
  }

  public ContentV1CardsListPost200ResponseDataCardsInner updateAt(String updateAt) {
    this.updateAt = updateAt;
    return this;
  }

  /**
   * Дата обновления
   * @return updateAt
  */
  
  @Schema(name = "updateAt", example = "2022-08-10T10:16:52Z", description = "Дата обновления", required = false)
  public String getUpdateAt() {
    return updateAt;
  }

  public void setUpdateAt(String updateAt) {
    this.updateAt = updateAt;
  }

  public ContentV1CardsListPost200ResponseDataCardsInner vendorCode(String vendorCode) {
    this.vendorCode = vendorCode;
    return this;
  }

  /**
   * Текстовый идентификатор НМ поставщика
   * @return vendorCode
  */
  
  @Schema(name = "vendorCode", example = "6000000001", description = "Текстовый идентификатор НМ поставщика", required = false)
  public String getVendorCode() {
    return vendorCode;
  }

  public void setVendorCode(String vendorCode) {
    this.vendorCode = vendorCode;
  }

  public ContentV1CardsListPost200ResponseDataCardsInner brand(String brand) {
    this.brand = brand;
    return this;
  }

  /**
   * Брэнд
   * @return brand
  */
  
  @Schema(name = "brand", example = "Шанель", description = "Брэнд", required = false)
  public String getBrand() {
    return brand;
  }

  public void setBrand(String brand) {
    this.brand = brand;
  }

  public ContentV1CardsListPost200ResponseDataCardsInner _object(String _object) {
    this._object = _object;
    return this;
  }

  /**
   * Категория для который создавалось КТ с данной НМ
   * @return _object
  */
  
  @Schema(name = "object", example = "Туалетная вода", description = "Категория для который создавалось КТ с данной НМ", required = false)
  public String getObject() {
    return _object;
  }

  public void setObject(String _object) {
    this._object = _object;
  }

  public ContentV1CardsListPost200ResponseDataCardsInner nmID(Integer nmID) {
    this.nmID = nmID;
    return this;
  }

  /**
   * Числовой идентификатор номенклатуры Wildberries
   * @return nmID
  */
  
  @Schema(name = "nmID", example = "66964167", description = "Числовой идентификатор номенклатуры Wildberries", required = false)
  public Integer getNmID() {
    return nmID;
  }

  public void setNmID(Integer nmID) {
    this.nmID = nmID;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    ContentV1CardsListPost200ResponseDataCardsInner contentV1CardsListPost200ResponseDataCardsInner = (ContentV1CardsListPost200ResponseDataCardsInner) o;
    return Objects.equals(this.sizes, contentV1CardsListPost200ResponseDataCardsInner.sizes) &&
        Objects.equals(this.mediaFiles, contentV1CardsListPost200ResponseDataCardsInner.mediaFiles) &&
        Objects.equals(this.colors, contentV1CardsListPost200ResponseDataCardsInner.colors) &&
        Objects.equals(this.updateAt, contentV1CardsListPost200ResponseDataCardsInner.updateAt) &&
        Objects.equals(this.vendorCode, contentV1CardsListPost200ResponseDataCardsInner.vendorCode) &&
        Objects.equals(this.brand, contentV1CardsListPost200ResponseDataCardsInner.brand) &&
        Objects.equals(this._object, contentV1CardsListPost200ResponseDataCardsInner._object) &&
        Objects.equals(this.nmID, contentV1CardsListPost200ResponseDataCardsInner.nmID);
  }

  @Override
  public int hashCode() {
    return Objects.hash(sizes, mediaFiles, colors, updateAt, vendorCode, brand, _object, nmID);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class ContentV1CardsListPost200ResponseDataCardsInner {\n");
    sb.append("    sizes: ").append(toIndentedString(sizes)).append("\n");
    sb.append("    mediaFiles: ").append(toIndentedString(mediaFiles)).append("\n");
    sb.append("    colors: ").append(toIndentedString(colors)).append("\n");
    sb.append("    updateAt: ").append(toIndentedString(updateAt)).append("\n");
    sb.append("    vendorCode: ").append(toIndentedString(vendorCode)).append("\n");
    sb.append("    brand: ").append(toIndentedString(brand)).append("\n");
    sb.append("    _object: ").append(toIndentedString(_object)).append("\n");
    sb.append("    nmID: ").append(toIndentedString(nmID)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

