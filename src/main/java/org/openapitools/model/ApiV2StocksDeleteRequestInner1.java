package org.openapitools.model;

import java.net.URI;
import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonTypeName;
import org.openapitools.jackson.nullable.JsonNullable;
import java.time.OffsetDateTime;
import javax.validation.Valid;
import javax.validation.constraints.*;
import io.swagger.v3.oas.annotations.media.Schema;


import java.util.*;
import javax.annotation.Generated;

/**
 * ApiV2StocksDeleteRequestInner1
 */

@JsonTypeName("_api_v2_stocks_delete_request_inner_1")
@Generated(value = "org.openapitools.codegen.languages.SpringCodegen", date = "2022-10-16T20:47:05.606+03:00[Europe/Moscow]")
public class ApiV2StocksDeleteRequestInner1 {

  @JsonProperty("barcode")
  private String barcode;

  @JsonProperty("warehouseId")
  private Integer warehouseId;

  public ApiV2StocksDeleteRequestInner1 barcode(String barcode) {
    this.barcode = barcode;
    return this;
  }

  /**
   * Баркод.
   * @return barcode
  */
  
  @Schema(name = "barcode", example = "656335639", description = "Баркод.", required = false)
  public String getBarcode() {
    return barcode;
  }

  public void setBarcode(String barcode) {
    this.barcode = barcode;
  }

  public ApiV2StocksDeleteRequestInner1 warehouseId(Integer warehouseId) {
    this.warehouseId = warehouseId;
    return this;
  }

  /**
   * ID склада.
   * @return warehouseId
  */
  
  @Schema(name = "warehouseId", example = "7543", description = "ID склада.", required = false)
  public Integer getWarehouseId() {
    return warehouseId;
  }

  public void setWarehouseId(Integer warehouseId) {
    this.warehouseId = warehouseId;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    ApiV2StocksDeleteRequestInner1 apiV2StocksDeleteRequestInner1 = (ApiV2StocksDeleteRequestInner1) o;
    return Objects.equals(this.barcode, apiV2StocksDeleteRequestInner1.barcode) &&
        Objects.equals(this.warehouseId, apiV2StocksDeleteRequestInner1.warehouseId);
  }

  @Override
  public int hashCode() {
    return Objects.hash(barcode, warehouseId);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class ApiV2StocksDeleteRequestInner1 {\n");
    sb.append("    barcode: ").append(toIndentedString(barcode)).append("\n");
    sb.append("    warehouseId: ").append(toIndentedString(warehouseId)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

