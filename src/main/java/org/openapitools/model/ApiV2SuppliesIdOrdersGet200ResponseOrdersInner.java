package org.openapitools.model;

import java.net.URI;
import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonTypeName;
import com.fasterxml.jackson.annotation.JsonValue;
import java.util.ArrayList;
import java.util.List;
import org.openapitools.jackson.nullable.JsonNullable;
import java.time.OffsetDateTime;
import javax.validation.Valid;
import javax.validation.constraints.*;
import io.swagger.v3.oas.annotations.media.Schema;


import java.util.*;
import javax.annotation.Generated;

/**
 * ApiV2SuppliesIdOrdersGet200ResponseOrdersInner
 */

@JsonTypeName("_api_v2_supplies__id__orders_get_200_response_orders_inner")
@Generated(value = "org.openapitools.codegen.languages.SpringCodegen", date = "2022-10-16T20:47:05.606+03:00[Europe/Moscow]")
public class ApiV2SuppliesIdOrdersGet200ResponseOrdersInner {

  @JsonProperty("orderId")
  private String orderId;

  @JsonProperty("dateCreated")
  private String dateCreated;

  @JsonProperty("storeId")
  private Integer storeId;

  @JsonProperty("wbWhId")
  private Integer wbWhId;

  @JsonProperty("pid")
  private Integer pid;

  @JsonProperty("officeAddress")
  private String officeAddress;

  @JsonProperty("chrtId")
  private Integer chrtId;

  @JsonProperty("barcodes")
  @Valid
  private List<String> barcodes = null;

  /**
   * Статус выставляемый продавцами. <br>`0` - Новый заказ. <br>`1` - В работе. <br>`2` - Сборочное задание завершено. <br>`3` - Сборочное задание отклонено. <br>`5` - На доставке курьером. <br>`6` - Курьер довез и клиент принял товар. <br>`7` - Клиент не принял товар. 
   */
  public enum StatusEnum {
    NUMBER_0(0),
    
    NUMBER_1(1),
    
    NUMBER_2(2),
    
    NUMBER_3(3),
    
    NUMBER_5(5),
    
    NUMBER_6(6),
    
    NUMBER_7(7);

    private Integer value;

    StatusEnum(Integer value) {
      this.value = value;
    }

    @JsonValue
    public Integer getValue() {
      return value;
    }

    @Override
    public String toString() {
      return String.valueOf(value);
    }

    @JsonCreator
    public static StatusEnum fromValue(Integer value) {
      for (StatusEnum b : StatusEnum.values()) {
        if (b.value.equals(value)) {
          return b;
        }
      }
      throw new IllegalArgumentException("Unexpected value '" + value + "'");
    }
  }

  @JsonProperty("status")
  private StatusEnum status;

  /**
   * Статус выставляемый Wildberries. <br>`0` - Новый заказ. <br>`1` - Отмена клиента. <br>`2` - Доставлен. <br>`3` - Возврат. <br>`4` - Ожидает. <br>`5` - Брак. 
   */
  public enum UserStatusEnum {
    NUMBER_0(0),
    
    NUMBER_1(1),
    
    NUMBER_2(2),
    
    NUMBER_3(3),
    
    NUMBER_4(4),
    
    NUMBER_5(5);

    private Integer value;

    UserStatusEnum(Integer value) {
      this.value = value;
    }

    @JsonValue
    public Integer getValue() {
      return value;
    }

    @Override
    public String toString() {
      return String.valueOf(value);
    }

    @JsonCreator
    public static UserStatusEnum fromValue(Integer value) {
      for (UserStatusEnum b : UserStatusEnum.values()) {
        if (b.value.equals(value)) {
          return b;
        }
      }
      throw new IllegalArgumentException("Unexpected value '" + value + "'");
    }
  }

  @JsonProperty("userStatus")
  private UserStatusEnum userStatus;

  @JsonProperty("rid")
  private String rid;

  @JsonProperty("totalPrice")
  private Integer totalPrice;

  @JsonProperty("currencyCode")
  private Integer currencyCode;

  @JsonProperty("orderUID")
  private String orderUID;

  /**
   * Тип доставки:   <ul>     <li>`1` - Обычная доставка.     <li>`2` - Доставка силами поставщика.   </ul> 
   */
  public enum DeliveryTypeEnum {
    NUMBER_1(1),
    
    NUMBER_2(2);

    private Integer value;

    DeliveryTypeEnum(Integer value) {
      this.value = value;
    }

    @JsonValue
    public Integer getValue() {
      return value;
    }

    @Override
    public String toString() {
      return String.valueOf(value);
    }

    @JsonCreator
    public static DeliveryTypeEnum fromValue(Integer value) {
      for (DeliveryTypeEnum b : DeliveryTypeEnum.values()) {
        if (b.value.equals(value)) {
          return b;
        }
      }
      throw new IllegalArgumentException("Unexpected value '" + value + "'");
    }
  }

  @JsonProperty("deliveryType")
  private DeliveryTypeEnum deliveryType;

  public ApiV2SuppliesIdOrdersGet200ResponseOrdersInner orderId(String orderId) {
    this.orderId = orderId;
    return this;
  }

  /**
   * Идентификатор заказа.
   * @return orderId
  */
  
  @Schema(name = "orderId", example = "13833711", description = "Идентификатор заказа.", required = false)
  public String getOrderId() {
    return orderId;
  }

  public void setOrderId(String orderId) {
    this.orderId = orderId;
  }

  public ApiV2SuppliesIdOrdersGet200ResponseOrdersInner dateCreated(String dateCreated) {
    this.dateCreated = dateCreated;
    return this;
  }

  /**
   * Дата создания заказа `(RFC3339)`.
   * @return dateCreated
  */
  
  @Schema(name = "dateCreated", example = "2021-02-20T16:50:33.365+03:00", description = "Дата создания заказа `(RFC3339)`.", required = false)
  public String getDateCreated() {
    return dateCreated;
  }

  public void setDateCreated(String dateCreated) {
    this.dateCreated = dateCreated;
  }

  public ApiV2SuppliesIdOrdersGet200ResponseOrdersInner storeId(Integer storeId) {
    this.storeId = storeId;
    return this;
  }

  /**
   * Идентификатор склада поставщика, на который пришел заказ.
   * @return storeId
  */
  
  @Schema(name = "storeId", example = "658434", description = "Идентификатор склада поставщика, на который пришел заказ.", required = false)
  public Integer getStoreId() {
    return storeId;
  }

  public void setStoreId(Integer storeId) {
    this.storeId = storeId;
  }

  public ApiV2SuppliesIdOrdersGet200ResponseOrdersInner wbWhId(Integer wbWhId) {
    this.wbWhId = wbWhId;
    return this;
  }

  /**
   * Идентификатор склада WB, на которой заказ должен быть доставлен.
   * @return wbWhId
  */
  
  @Schema(name = "wbWhId", example = "119408", description = "Идентификатор склада WB, на которой заказ должен быть доставлен.", required = false)
  public Integer getWbWhId() {
    return wbWhId;
  }

  public void setWbWhId(Integer wbWhId) {
    this.wbWhId = wbWhId;
  }

  public ApiV2SuppliesIdOrdersGet200ResponseOrdersInner pid(Integer pid) {
    this.pid = pid;
    return this;
  }

  /**
   * Идентификатор ПВЗ/магазина, куда необходимо доставить заказ (если применимо).
   * @return pid
  */
  
  @Schema(name = "pid", example = "0", description = "Идентификатор ПВЗ/магазина, куда необходимо доставить заказ (если применимо).", required = false)
  public Integer getPid() {
    return pid;
  }

  public void setPid(Integer pid) {
    this.pid = pid;
  }

  public ApiV2SuppliesIdOrdersGet200ResponseOrdersInner officeAddress(String officeAddress) {
    this.officeAddress = officeAddress;
    return this;
  }

  /**
   * Адрес ПВЗ/магазина, куда необходимо доставить заказ (если применимо).
   * @return officeAddress
  */
  
  @Schema(name = "officeAddress", example = "г Ставрополь (Ставропольский край), Ленина 482/1", description = "Адрес ПВЗ/магазина, куда необходимо доставить заказ (если применимо).", required = false)
  public String getOfficeAddress() {
    return officeAddress;
  }

  public void setOfficeAddress(String officeAddress) {
    this.officeAddress = officeAddress;
  }

  public ApiV2SuppliesIdOrdersGet200ResponseOrdersInner chrtId(Integer chrtId) {
    this.chrtId = chrtId;
    return this;
  }

  /**
   * Идентификатор артикула.
   * @return chrtId
  */
  
  @Schema(name = "chrtId", example = "11111111", description = "Идентификатор артикула.", required = false)
  public Integer getChrtId() {
    return chrtId;
  }

  public void setChrtId(Integer chrtId) {
    this.chrtId = chrtId;
  }

  public ApiV2SuppliesIdOrdersGet200ResponseOrdersInner barcodes(List<String> barcodes) {
    this.barcodes = barcodes;
    return this;
  }

  public ApiV2SuppliesIdOrdersGet200ResponseOrdersInner addBarcodesItem(String barcodesItem) {
    if (this.barcodes == null) {
      this.barcodes = new ArrayList<>();
    }
    this.barcodes.add(barcodesItem);
    return this;
  }

  /**
   * Массив штрихкодов товара.
   * @return barcodes
  */
  
  @Schema(name = "barcodes", description = "Массив штрихкодов товара.", required = false)
  public List<String> getBarcodes() {
    return barcodes;
  }

  public void setBarcodes(List<String> barcodes) {
    this.barcodes = barcodes;
  }

  public ApiV2SuppliesIdOrdersGet200ResponseOrdersInner status(StatusEnum status) {
    this.status = status;
    return this;
  }

  /**
   * Статус выставляемый продавцами. <br>`0` - Новый заказ. <br>`1` - В работе. <br>`2` - Сборочное задание завершено. <br>`3` - Сборочное задание отклонено. <br>`5` - На доставке курьером. <br>`6` - Курьер довез и клиент принял товар. <br>`7` - Клиент не принял товар. 
   * @return status
  */
  
  @Schema(name = "status", description = "Статус выставляемый продавцами. <br>`0` - Новый заказ. <br>`1` - В работе. <br>`2` - Сборочное задание завершено. <br>`3` - Сборочное задание отклонено. <br>`5` - На доставке курьером. <br>`6` - Курьер довез и клиент принял товар. <br>`7` - Клиент не принял товар. ", required = false)
  public StatusEnum getStatus() {
    return status;
  }

  public void setStatus(StatusEnum status) {
    this.status = status;
  }

  public ApiV2SuppliesIdOrdersGet200ResponseOrdersInner userStatus(UserStatusEnum userStatus) {
    this.userStatus = userStatus;
    return this;
  }

  /**
   * Статус выставляемый Wildberries. <br>`0` - Новый заказ. <br>`1` - Отмена клиента. <br>`2` - Доставлен. <br>`3` - Возврат. <br>`4` - Ожидает. <br>`5` - Брак. 
   * @return userStatus
  */
  
  @Schema(name = "userStatus", description = "Статус выставляемый Wildberries. <br>`0` - Новый заказ. <br>`1` - Отмена клиента. <br>`2` - Доставлен. <br>`3` - Возврат. <br>`4` - Ожидает. <br>`5` - Брак. ", required = false)
  public UserStatusEnum getUserStatus() {
    return userStatus;
  }

  public void setUserStatus(UserStatusEnum userStatus) {
    this.userStatus = userStatus;
  }

  public ApiV2SuppliesIdOrdersGet200ResponseOrdersInner rid(String rid) {
    this.rid = rid;
    return this;
  }

  /**
   * Уникальный идентификатор вещи, `разный у одинаковых chrt_id`.
   * @return rid
  */
  
  @Schema(name = "rid", example = "100321840623", description = "Уникальный идентификатор вещи, `разный у одинаковых chrt_id`.", required = false)
  public String getRid() {
    return rid;
  }

  public void setRid(String rid) {
    this.rid = rid;
  }

  public ApiV2SuppliesIdOrdersGet200ResponseOrdersInner totalPrice(Integer totalPrice) {
    this.totalPrice = totalPrice;
    return this;
  }

  /**
   * Цена в валюте продажи с учетом скидок в копейках. `Код валюты продажи в поле currencyCode`.
   * @return totalPrice
  */
  
  @Schema(name = "totalPrice", example = "5600", description = "Цена в валюте продажи с учетом скидок в копейках. `Код валюты продажи в поле currencyCode`.", required = false)
  public Integer getTotalPrice() {
    return totalPrice;
  }

  public void setTotalPrice(Integer totalPrice) {
    this.totalPrice = totalPrice;
  }

  public ApiV2SuppliesIdOrdersGet200ResponseOrdersInner currencyCode(Integer currencyCode) {
    this.currencyCode = currencyCode;
    return this;
  }

  /**
   * Код валюты по ISO 4217. <br>Пример: <ul>   <li>`51` - Армянский драм.   <li>`398` - Казахский тенге.   <li>`417` - Киргизский сом.   <li>`643` - Российский рубль.   <li>`840` - Доллар США.   <li>`933` - Белорусский рубль.   <li>`974` - Белорусский рубль. </ul> 
   * @return currencyCode
  */
  
  @Schema(name = "currencyCode", example = "643", description = "Код валюты по ISO 4217. <br>Пример: <ul>   <li>`51` - Армянский драм.   <li>`398` - Казахский тенге.   <li>`417` - Киргизский сом.   <li>`643` - Российский рубль.   <li>`840` - Доллар США.   <li>`933` - Белорусский рубль.   <li>`974` - Белорусский рубль. </ul> ", required = false)
  public Integer getCurrencyCode() {
    return currencyCode;
  }

  public void setCurrencyCode(Integer currencyCode) {
    this.currencyCode = currencyCode;
  }

  public ApiV2SuppliesIdOrdersGet200ResponseOrdersInner orderUID(String orderUID) {
    this.orderUID = orderUID;
    return this;
  }

  /**
   * Идентификатор транзакции (для группировки заказов).
   * @return orderUID
  */
  
  @Schema(name = "orderUID", example = "string", description = "Идентификатор транзакции (для группировки заказов).", required = false)
  public String getOrderUID() {
    return orderUID;
  }

  public void setOrderUID(String orderUID) {
    this.orderUID = orderUID;
  }

  public ApiV2SuppliesIdOrdersGet200ResponseOrdersInner deliveryType(DeliveryTypeEnum deliveryType) {
    this.deliveryType = deliveryType;
    return this;
  }

  /**
   * Тип доставки:   <ul>     <li>`1` - Обычная доставка.     <li>`2` - Доставка силами поставщика.   </ul> 
   * @return deliveryType
  */
  
  @Schema(name = "deliveryType", description = "Тип доставки:   <ul>     <li>`1` - Обычная доставка.     <li>`2` - Доставка силами поставщика.   </ul> ", required = false)
  public DeliveryTypeEnum getDeliveryType() {
    return deliveryType;
  }

  public void setDeliveryType(DeliveryTypeEnum deliveryType) {
    this.deliveryType = deliveryType;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    ApiV2SuppliesIdOrdersGet200ResponseOrdersInner apiV2SuppliesIdOrdersGet200ResponseOrdersInner = (ApiV2SuppliesIdOrdersGet200ResponseOrdersInner) o;
    return Objects.equals(this.orderId, apiV2SuppliesIdOrdersGet200ResponseOrdersInner.orderId) &&
        Objects.equals(this.dateCreated, apiV2SuppliesIdOrdersGet200ResponseOrdersInner.dateCreated) &&
        Objects.equals(this.storeId, apiV2SuppliesIdOrdersGet200ResponseOrdersInner.storeId) &&
        Objects.equals(this.wbWhId, apiV2SuppliesIdOrdersGet200ResponseOrdersInner.wbWhId) &&
        Objects.equals(this.pid, apiV2SuppliesIdOrdersGet200ResponseOrdersInner.pid) &&
        Objects.equals(this.officeAddress, apiV2SuppliesIdOrdersGet200ResponseOrdersInner.officeAddress) &&
        Objects.equals(this.chrtId, apiV2SuppliesIdOrdersGet200ResponseOrdersInner.chrtId) &&
        Objects.equals(this.barcodes, apiV2SuppliesIdOrdersGet200ResponseOrdersInner.barcodes) &&
        Objects.equals(this.status, apiV2SuppliesIdOrdersGet200ResponseOrdersInner.status) &&
        Objects.equals(this.userStatus, apiV2SuppliesIdOrdersGet200ResponseOrdersInner.userStatus) &&
        Objects.equals(this.rid, apiV2SuppliesIdOrdersGet200ResponseOrdersInner.rid) &&
        Objects.equals(this.totalPrice, apiV2SuppliesIdOrdersGet200ResponseOrdersInner.totalPrice) &&
        Objects.equals(this.currencyCode, apiV2SuppliesIdOrdersGet200ResponseOrdersInner.currencyCode) &&
        Objects.equals(this.orderUID, apiV2SuppliesIdOrdersGet200ResponseOrdersInner.orderUID) &&
        Objects.equals(this.deliveryType, apiV2SuppliesIdOrdersGet200ResponseOrdersInner.deliveryType);
  }

  @Override
  public int hashCode() {
    return Objects.hash(orderId, dateCreated, storeId, wbWhId, pid, officeAddress, chrtId, barcodes, status, userStatus, rid, totalPrice, currencyCode, orderUID, deliveryType);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class ApiV2SuppliesIdOrdersGet200ResponseOrdersInner {\n");
    sb.append("    orderId: ").append(toIndentedString(orderId)).append("\n");
    sb.append("    dateCreated: ").append(toIndentedString(dateCreated)).append("\n");
    sb.append("    storeId: ").append(toIndentedString(storeId)).append("\n");
    sb.append("    wbWhId: ").append(toIndentedString(wbWhId)).append("\n");
    sb.append("    pid: ").append(toIndentedString(pid)).append("\n");
    sb.append("    officeAddress: ").append(toIndentedString(officeAddress)).append("\n");
    sb.append("    chrtId: ").append(toIndentedString(chrtId)).append("\n");
    sb.append("    barcodes: ").append(toIndentedString(barcodes)).append("\n");
    sb.append("    status: ").append(toIndentedString(status)).append("\n");
    sb.append("    userStatus: ").append(toIndentedString(userStatus)).append("\n");
    sb.append("    rid: ").append(toIndentedString(rid)).append("\n");
    sb.append("    totalPrice: ").append(toIndentedString(totalPrice)).append("\n");
    sb.append("    currencyCode: ").append(toIndentedString(currencyCode)).append("\n");
    sb.append("    orderUID: ").append(toIndentedString(orderUID)).append("\n");
    sb.append("    deliveryType: ").append(toIndentedString(deliveryType)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

