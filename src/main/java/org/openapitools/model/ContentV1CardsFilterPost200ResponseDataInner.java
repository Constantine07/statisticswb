package org.openapitools.model;

import java.net.URI;
import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonTypeName;
import java.util.ArrayList;
import java.util.List;
import org.openapitools.model.ContentV1CardsFilterPost200ResponseDataInnerSizesInner;
import org.openapitools.jackson.nullable.JsonNullable;
import java.time.OffsetDateTime;
import javax.validation.Valid;
import javax.validation.constraints.*;
import io.swagger.v3.oas.annotations.media.Schema;


import java.util.*;
import javax.annotation.Generated;

/**
 * ContentV1CardsFilterPost200ResponseDataInner
 */

@JsonTypeName("_content_v1_cards_filter_post_200_response_data_inner")
@Generated(value = "org.openapitools.codegen.languages.SpringCodegen", date = "2022-10-16T20:47:05.606+03:00[Europe/Moscow]")
public class ContentV1CardsFilterPost200ResponseDataInner {

  @JsonProperty("imtID")
  private Integer imtID;

  @JsonProperty("nmID")
  private Integer nmID;

  @JsonProperty("vendorCode")
  private String vendorCode;

  @JsonProperty("mediaFiles")
  @Valid
  private List<String> mediaFiles = null;

  @JsonProperty("sizes")
  @Valid
  private List<ContentV1CardsFilterPost200ResponseDataInnerSizesInner> sizes = null;

  @JsonProperty("characteristics")
  @Valid
  private List<Object> characteristics = null;

  public ContentV1CardsFilterPost200ResponseDataInner imtID(Integer imtID) {
    this.imtID = imtID;
    return this;
  }

  /**
   * Идентификатор карточки товара (нужен для группирования НМ в одно КТ)
   * @return imtID
  */
  
  @Schema(name = "imtID", example = "50777914", description = "Идентификатор карточки товара (нужен для группирования НМ в одно КТ)", required = false)
  public Integer getImtID() {
    return imtID;
  }

  public void setImtID(Integer imtID) {
    this.imtID = imtID;
  }

  public ContentV1CardsFilterPost200ResponseDataInner nmID(Integer nmID) {
    this.nmID = nmID;
    return this;
  }

  /**
   * Числовой идентификатор номенклатуры Wildberries
   * @return nmID
  */
  
  @Schema(name = "nmID", example = "66964219", description = "Числовой идентификатор номенклатуры Wildberries", required = false)
  public Integer getNmID() {
    return nmID;
  }

  public void setNmID(Integer nmID) {
    this.nmID = nmID;
  }

  public ContentV1CardsFilterPost200ResponseDataInner vendorCode(String vendorCode) {
    this.vendorCode = vendorCode;
    return this;
  }

  /**
   * Вендор код, текстовый идентификатор номенклатуры поставщика
   * @return vendorCode
  */
  
  @Schema(name = "vendorCode", example = "6000000001", description = "Вендор код, текстовый идентификатор номенклатуры поставщика", required = false)
  public String getVendorCode() {
    return vendorCode;
  }

  public void setVendorCode(String vendorCode) {
    this.vendorCode = vendorCode;
  }

  public ContentV1CardsFilterPost200ResponseDataInner mediaFiles(List<String> mediaFiles) {
    this.mediaFiles = mediaFiles;
    return this;
  }

  public ContentV1CardsFilterPost200ResponseDataInner addMediaFilesItem(String mediaFilesItem) {
    if (this.mediaFiles == null) {
      this.mediaFiles = new ArrayList<>();
    }
    this.mediaFiles.add(mediaFilesItem);
    return this;
  }

  /**
   * Get mediaFiles
   * @return mediaFiles
  */
  
  @Schema(name = "mediaFiles", required = false)
  public List<String> getMediaFiles() {
    return mediaFiles;
  }

  public void setMediaFiles(List<String> mediaFiles) {
    this.mediaFiles = mediaFiles;
  }

  public ContentV1CardsFilterPost200ResponseDataInner sizes(List<ContentV1CardsFilterPost200ResponseDataInnerSizesInner> sizes) {
    this.sizes = sizes;
    return this;
  }

  public ContentV1CardsFilterPost200ResponseDataInner addSizesItem(ContentV1CardsFilterPost200ResponseDataInnerSizesInner sizesItem) {
    if (this.sizes == null) {
      this.sizes = new ArrayList<>();
    }
    this.sizes.add(sizesItem);
    return this;
  }

  /**
   * Get sizes
   * @return sizes
  */
  @Valid 
  @Schema(name = "sizes", required = false)
  public List<ContentV1CardsFilterPost200ResponseDataInnerSizesInner> getSizes() {
    return sizes;
  }

  public void setSizes(List<ContentV1CardsFilterPost200ResponseDataInnerSizesInner> sizes) {
    this.sizes = sizes;
  }

  public ContentV1CardsFilterPost200ResponseDataInner characteristics(List<Object> characteristics) {
    this.characteristics = characteristics;
    return this;
  }

  public ContentV1CardsFilterPost200ResponseDataInner addCharacteristicsItem(Object characteristicsItem) {
    if (this.characteristics == null) {
      this.characteristics = new ArrayList<>();
    }
    this.characteristics.add(characteristicsItem);
    return this;
  }

  /**
   * Массив характеристик, индивидуальный для каждой категории
   * @return characteristics
  */
  
  @Schema(name = "characteristics", example = "[{\"Бренд\":\"GlisH\"},{\"Комплектация\":[\"Шлепанцы женские на не скользящей подошве - 1 пара\"]}]", description = "Массив характеристик, индивидуальный для каждой категории", required = false)
  public List<Object> getCharacteristics() {
    return characteristics;
  }

  public void setCharacteristics(List<Object> characteristics) {
    this.characteristics = characteristics;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    ContentV1CardsFilterPost200ResponseDataInner contentV1CardsFilterPost200ResponseDataInner = (ContentV1CardsFilterPost200ResponseDataInner) o;
    return Objects.equals(this.imtID, contentV1CardsFilterPost200ResponseDataInner.imtID) &&
        Objects.equals(this.nmID, contentV1CardsFilterPost200ResponseDataInner.nmID) &&
        Objects.equals(this.vendorCode, contentV1CardsFilterPost200ResponseDataInner.vendorCode) &&
        Objects.equals(this.mediaFiles, contentV1CardsFilterPost200ResponseDataInner.mediaFiles) &&
        Objects.equals(this.sizes, contentV1CardsFilterPost200ResponseDataInner.sizes) &&
        Objects.equals(this.characteristics, contentV1CardsFilterPost200ResponseDataInner.characteristics);
  }

  @Override
  public int hashCode() {
    return Objects.hash(imtID, nmID, vendorCode, mediaFiles, sizes, characteristics);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class ContentV1CardsFilterPost200ResponseDataInner {\n");
    sb.append("    imtID: ").append(toIndentedString(imtID)).append("\n");
    sb.append("    nmID: ").append(toIndentedString(nmID)).append("\n");
    sb.append("    vendorCode: ").append(toIndentedString(vendorCode)).append("\n");
    sb.append("    mediaFiles: ").append(toIndentedString(mediaFiles)).append("\n");
    sb.append("    sizes: ").append(toIndentedString(sizes)).append("\n");
    sb.append("    characteristics: ").append(toIndentedString(characteristics)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

