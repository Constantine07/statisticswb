package org.openapitools.model;

import java.net.URI;
import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonTypeName;
import org.openapitools.jackson.nullable.JsonNullable;
import java.time.OffsetDateTime;
import javax.validation.Valid;
import javax.validation.constraints.*;
import io.swagger.v3.oas.annotations.media.Schema;


import java.util.*;
import javax.annotation.Generated;

/**
 * ContentV1ObjectParentAllGet200ResponseDataInner
 */

@JsonTypeName("_content_v1_object_parent_all_get_200_response_data_inner")
@Generated(value = "org.openapitools.codegen.languages.SpringCodegen", date = "2022-10-16T20:47:05.606+03:00[Europe/Moscow]")
public class ContentV1ObjectParentAllGet200ResponseDataInner {

  @JsonProperty("name")
  private String name;

  @JsonProperty("isVisible")
  private Boolean isVisible;

  public ContentV1ObjectParentAllGet200ResponseDataInner name(String name) {
    this.name = name;
    return this;
  }

  /**
   * Название категории
   * @return name
  */
  
  @Schema(name = "name", example = "Электроника", description = "Название категории", required = false)
  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public ContentV1ObjectParentAllGet200ResponseDataInner isVisible(Boolean isVisible) {
    this.isVisible = isVisible;
    return this;
  }

  /**
   * Виден на сайте
   * @return isVisible
  */
  
  @Schema(name = "isVisible", example = "true", description = "Виден на сайте", required = false)
  public Boolean getIsVisible() {
    return isVisible;
  }

  public void setIsVisible(Boolean isVisible) {
    this.isVisible = isVisible;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    ContentV1ObjectParentAllGet200ResponseDataInner contentV1ObjectParentAllGet200ResponseDataInner = (ContentV1ObjectParentAllGet200ResponseDataInner) o;
    return Objects.equals(this.name, contentV1ObjectParentAllGet200ResponseDataInner.name) &&
        Objects.equals(this.isVisible, contentV1ObjectParentAllGet200ResponseDataInner.isVisible);
  }

  @Override
  public int hashCode() {
    return Objects.hash(name, isVisible);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class ContentV1ObjectParentAllGet200ResponseDataInner {\n");
    sb.append("    name: ").append(toIndentedString(name)).append("\n");
    sb.append("    isVisible: ").append(toIndentedString(isVisible)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

