package org.openapitools.model;

import java.net.URI;
import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonTypeName;
import com.fasterxml.jackson.annotation.JsonValue;
import java.util.ArrayList;
import java.util.List;
import org.openapitools.model.ApiV2OrdersGetRequestInnerSgtinInner;
import org.openapitools.jackson.nullable.JsonNullable;
import java.time.OffsetDateTime;
import javax.validation.Valid;
import javax.validation.constraints.*;
import io.swagger.v3.oas.annotations.media.Schema;


import java.util.*;
import javax.annotation.Generated;

/**
 * ApiV2OrdersGetRequestInner
 */

@JsonTypeName("_api_v2_orders_get_request_inner")
@Generated(value = "org.openapitools.codegen.languages.SpringCodegen", date = "2022-10-16T20:47:05.606+03:00[Europe/Moscow]")
public class ApiV2OrdersGetRequestInner {

  @JsonProperty("orderId")
  private String orderId;

  /**
   * Статус. `0` - Новый заказ. `1` - В работе. `2` - Сборочное задание завершено. `3` - Сборочное задание отклонено. `5` - На доставке курьером. `6` - Клиент получил товар (курьерская доставка и самовывоз). `7` - Клиент не принял товар (курьерская доставка и самовывоз). `8` - Товар для самовывоза из магазина принят к работе. `9` - Товар для самовывоза из магазина готов к выдаче. 
   */
  public enum StatusEnum {
    NUMBER_0(0),
    
    NUMBER_1(1),
    
    NUMBER_2(2),
    
    NUMBER_3(3),
    
    NUMBER_5(5),
    
    NUMBER_6(6),
    
    NUMBER_7(7),
    
    NUMBER_8(8),
    
    NUMBER_9(9);

    private Integer value;

    StatusEnum(Integer value) {
      this.value = value;
    }

    @JsonValue
    public Integer getValue() {
      return value;
    }

    @Override
    public String toString() {
      return String.valueOf(value);
    }

    @JsonCreator
    public static StatusEnum fromValue(Integer value) {
      for (StatusEnum b : StatusEnum.values()) {
        if (b.value.equals(value)) {
          return b;
        }
      }
      throw new IllegalArgumentException("Unexpected value '" + value + "'");
    }
  }

  @JsonProperty("status")
  private StatusEnum status;

  @JsonProperty("sgtin")
  @Valid
  private List<ApiV2OrdersGetRequestInnerSgtinInner> sgtin = null;

  public ApiV2OrdersGetRequestInner orderId(String orderId) {
    this.orderId = orderId;
    return this;
  }

  /**
   * Идентификатор заказа.
   * @return orderId
  */
  
  @Schema(name = "orderId", example = "13833711", description = "Идентификатор заказа.", required = false)
  public String getOrderId() {
    return orderId;
  }

  public void setOrderId(String orderId) {
    this.orderId = orderId;
  }

  public ApiV2OrdersGetRequestInner status(StatusEnum status) {
    this.status = status;
    return this;
  }

  /**
   * Статус. `0` - Новый заказ. `1` - В работе. `2` - Сборочное задание завершено. `3` - Сборочное задание отклонено. `5` - На доставке курьером. `6` - Клиент получил товар (курьерская доставка и самовывоз). `7` - Клиент не принял товар (курьерская доставка и самовывоз). `8` - Товар для самовывоза из магазина принят к работе. `9` - Товар для самовывоза из магазина готов к выдаче. 
   * @return status
  */
  
  @Schema(name = "status", description = "Статус. `0` - Новый заказ. `1` - В работе. `2` - Сборочное задание завершено. `3` - Сборочное задание отклонено. `5` - На доставке курьером. `6` - Клиент получил товар (курьерская доставка и самовывоз). `7` - Клиент не принял товар (курьерская доставка и самовывоз). `8` - Товар для самовывоза из магазина принят к работе. `9` - Товар для самовывоза из магазина готов к выдаче. ", required = false)
  public StatusEnum getStatus() {
    return status;
  }

  public void setStatus(StatusEnum status) {
    this.status = status;
  }

  public ApiV2OrdersGetRequestInner sgtin(List<ApiV2OrdersGetRequestInnerSgtinInner> sgtin) {
    this.sgtin = sgtin;
    return this;
  }

  public ApiV2OrdersGetRequestInner addSgtinItem(ApiV2OrdersGetRequestInnerSgtinInner sgtinItem) {
    if (this.sgtin == null) {
      this.sgtin = new ArrayList<>();
    }
    this.sgtin.add(sgtinItem);
    return this;
  }

  /**
   * Массив КИЗов. Требуется только для фармацевтической продукции при переводе её в статус `6` (\"Клиент получил товар\"). 
   * @return sgtin
  */
  @Valid 
  @Schema(name = "sgtin", description = "Массив КИЗов. Требуется только для фармацевтической продукции при переводе её в статус `6` (\"Клиент получил товар\"). ", required = false)
  public List<ApiV2OrdersGetRequestInnerSgtinInner> getSgtin() {
    return sgtin;
  }

  public void setSgtin(List<ApiV2OrdersGetRequestInnerSgtinInner> sgtin) {
    this.sgtin = sgtin;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    ApiV2OrdersGetRequestInner apiV2OrdersGetRequestInner = (ApiV2OrdersGetRequestInner) o;
    return Objects.equals(this.orderId, apiV2OrdersGetRequestInner.orderId) &&
        Objects.equals(this.status, apiV2OrdersGetRequestInner.status) &&
        Objects.equals(this.sgtin, apiV2OrdersGetRequestInner.sgtin);
  }

  @Override
  public int hashCode() {
    return Objects.hash(orderId, status, sgtin);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class ApiV2OrdersGetRequestInner {\n");
    sb.append("    orderId: ").append(toIndentedString(orderId)).append("\n");
    sb.append("    status: ").append(toIndentedString(status)).append("\n");
    sb.append("    sgtin: ").append(toIndentedString(sgtin)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

