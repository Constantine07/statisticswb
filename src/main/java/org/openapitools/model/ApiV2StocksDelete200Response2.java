package org.openapitools.model;

import java.net.URI;
import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonTypeName;
import java.util.ArrayList;
import java.util.List;
import org.openapitools.model.ApiV2StocksDelete200Response2DataInner;
import org.openapitools.jackson.nullable.JsonNullable;
import java.time.OffsetDateTime;
import javax.validation.Valid;
import javax.validation.constraints.*;
import io.swagger.v3.oas.annotations.media.Schema;


import java.util.*;
import javax.annotation.Generated;

/**
 * ApiV2StocksDelete200Response2
 */

@JsonTypeName("_api_v2_stocks_delete_200_response_2")
@Generated(value = "org.openapitools.codegen.languages.SpringCodegen", date = "2022-10-16T20:47:05.606+03:00[Europe/Moscow]")
public class ApiV2StocksDelete200Response2 {

  @JsonProperty("error")
  private Boolean error;

  @JsonProperty("errorText")
  private String errorText;

  @JsonProperty("additionalErrors")
  private Object additionalErrors;

  @JsonProperty("data")
  @Valid
  private List<ApiV2StocksDelete200Response2DataInner> data = null;

  public ApiV2StocksDelete200Response2 error(Boolean error) {
    this.error = error;
    return this;
  }

  /**
   * Флаг ошибки.
   * @return error
  */
  
  @Schema(name = "error", example = "true", description = "Флаг ошибки.", required = false)
  public Boolean getError() {
    return error;
  }

  public void setError(Boolean error) {
    this.error = error;
  }

  public ApiV2StocksDelete200Response2 errorText(String errorText) {
    this.errorText = errorText;
    return this;
  }

  /**
   * Описание ошибки.
   * @return errorText
  */
  
  @Schema(name = "errorText", example = "string", description = "Описание ошибки.", required = false)
  public String getErrorText() {
    return errorText;
  }

  public void setErrorText(String errorText) {
    this.errorText = errorText;
  }

  public ApiV2StocksDelete200Response2 additionalErrors(Object additionalErrors) {
    this.additionalErrors = additionalErrors;
    return this;
  }

  /**
   * Дополнительные ошибки.
   * @return additionalErrors
  */
  
  @Schema(name = "additionalErrors", description = "Дополнительные ошибки.", required = false)
  public Object getAdditionalErrors() {
    return additionalErrors;
  }

  public void setAdditionalErrors(Object additionalErrors) {
    this.additionalErrors = additionalErrors;
  }

  public ApiV2StocksDelete200Response2 data(List<ApiV2StocksDelete200Response2DataInner> data) {
    this.data = data;
    return this;
  }

  public ApiV2StocksDelete200Response2 addDataItem(ApiV2StocksDelete200Response2DataInner dataItem) {
    if (this.data == null) {
      this.data = new ArrayList<>();
    }
    this.data.add(dataItem);
    return this;
  }

  /**
   * Get data
   * @return data
  */
  @Valid 
  @Schema(name = "data", required = false)
  public List<ApiV2StocksDelete200Response2DataInner> getData() {
    return data;
  }

  public void setData(List<ApiV2StocksDelete200Response2DataInner> data) {
    this.data = data;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    ApiV2StocksDelete200Response2 apiV2StocksDelete200Response2 = (ApiV2StocksDelete200Response2) o;
    return Objects.equals(this.error, apiV2StocksDelete200Response2.error) &&
        Objects.equals(this.errorText, apiV2StocksDelete200Response2.errorText) &&
        Objects.equals(this.additionalErrors, apiV2StocksDelete200Response2.additionalErrors) &&
        Objects.equals(this.data, apiV2StocksDelete200Response2.data);
  }

  @Override
  public int hashCode() {
    return Objects.hash(error, errorText, additionalErrors, data);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class ApiV2StocksDelete200Response2 {\n");
    sb.append("    error: ").append(toIndentedString(error)).append("\n");
    sb.append("    errorText: ").append(toIndentedString(errorText)).append("\n");
    sb.append("    additionalErrors: ").append(toIndentedString(additionalErrors)).append("\n");
    sb.append("    data: ").append(toIndentedString(data)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

