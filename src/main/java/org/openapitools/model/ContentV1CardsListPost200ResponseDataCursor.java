package org.openapitools.model;

import java.net.URI;
import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonTypeName;
import org.openapitools.jackson.nullable.JsonNullable;
import java.time.OffsetDateTime;
import javax.validation.Valid;
import javax.validation.constraints.*;
import io.swagger.v3.oas.annotations.media.Schema;


import java.util.*;
import javax.annotation.Generated;

/**
 * ContentV1CardsListPost200ResponseDataCursor
 */

@JsonTypeName("_content_v1_cards_list_post_200_response_data_cursor")
@Generated(value = "org.openapitools.codegen.languages.SpringCodegen", date = "2022-10-16T20:47:05.606+03:00[Europe/Moscow]")
public class ContentV1CardsListPost200ResponseDataCursor {

  @JsonProperty("offset")
  private Integer offset;

  @JsonProperty("limit")
  private Integer limit;

  @JsonProperty("total")
  private Integer total;

  public ContentV1CardsListPost200ResponseDataCursor offset(Integer offset) {
    this.offset = offset;
    return this;
  }

  /**
   * Смещение от начала списка с указанными критериями поиска и сортировки.
   * @return offset
  */
  
  @Schema(name = "offset", example = "0", description = "Смещение от начала списка с указанными критериями поиска и сортировки.", required = false)
  public Integer getOffset() {
    return offset;
  }

  public void setOffset(Integer offset) {
    this.offset = offset;
  }

  public ContentV1CardsListPost200ResponseDataCursor limit(Integer limit) {
    this.limit = limit;
    return this;
  }

  /**
   * Ограничение по количеству НМ в ответе
   * @return limit
  */
  
  @Schema(name = "limit", example = "1", description = "Ограничение по количеству НМ в ответе", required = false)
  public Integer getLimit() {
    return limit;
  }

  public void setLimit(Integer limit) {
    this.limit = limit;
  }

  public ContentV1CardsListPost200ResponseDataCursor total(Integer total) {
    this.total = total;
    return this;
  }

  /**
   * Общее количество НМ. (Временно отключено)
   * @return total
  */
  
  @Schema(name = "total", example = "66964167", description = "Общее количество НМ. (Временно отключено)", required = false)
  public Integer getTotal() {
    return total;
  }

  public void setTotal(Integer total) {
    this.total = total;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    ContentV1CardsListPost200ResponseDataCursor contentV1CardsListPost200ResponseDataCursor = (ContentV1CardsListPost200ResponseDataCursor) o;
    return Objects.equals(this.offset, contentV1CardsListPost200ResponseDataCursor.offset) &&
        Objects.equals(this.limit, contentV1CardsListPost200ResponseDataCursor.limit) &&
        Objects.equals(this.total, contentV1CardsListPost200ResponseDataCursor.total);
  }

  @Override
  public int hashCode() {
    return Objects.hash(offset, limit, total);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class ContentV1CardsListPost200ResponseDataCursor {\n");
    sb.append("    offset: ").append(toIndentedString(offset)).append("\n");
    sb.append("    limit: ").append(toIndentedString(limit)).append("\n");
    sb.append("    total: ").append(toIndentedString(total)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

