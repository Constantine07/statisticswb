package org.openapitools.model;

import java.net.URI;
import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonTypeName;
import org.openapitools.jackson.nullable.JsonNullable;
import java.time.OffsetDateTime;
import javax.validation.Valid;
import javax.validation.constraints.*;
import io.swagger.v3.oas.annotations.media.Schema;


import java.util.*;
import javax.annotation.Generated;

/**
 * ApiV2StocksDelete200Response1DataErrorInner
 */

@JsonTypeName("_api_v2_stocks_delete_200_response_1_data_error_inner")
@Generated(value = "org.openapitools.codegen.languages.SpringCodegen", date = "2022-10-16T20:47:05.606+03:00[Europe/Moscow]")
public class ApiV2StocksDelete200Response1DataErrorInner {

  @JsonProperty("barcode")
  private String barcode;

  @JsonProperty("err")
  private String err;

  public ApiV2StocksDelete200Response1DataErrorInner barcode(String barcode) {
    this.barcode = barcode;
    return this;
  }

  /**
   * Баркод.
   * @return barcode
  */
  
  @Schema(name = "barcode", example = "123456789", description = "Баркод.", required = false)
  public String getBarcode() {
    return barcode;
  }

  public void setBarcode(String barcode) {
    this.barcode = barcode;
  }

  public ApiV2StocksDelete200Response1DataErrorInner err(String err) {
    this.err = err;
    return this;
  }

  /**
   * Ошибка, случившаяся при загрудке остатка с данным баркодом.
   * @return err
  */
  
  @Schema(name = "err", example = "указан отрицательный остаток", description = "Ошибка, случившаяся при загрудке остатка с данным баркодом.", required = false)
  public String getErr() {
    return err;
  }

  public void setErr(String err) {
    this.err = err;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    ApiV2StocksDelete200Response1DataErrorInner apiV2StocksDelete200Response1DataErrorInner = (ApiV2StocksDelete200Response1DataErrorInner) o;
    return Objects.equals(this.barcode, apiV2StocksDelete200Response1DataErrorInner.barcode) &&
        Objects.equals(this.err, apiV2StocksDelete200Response1DataErrorInner.err);
  }

  @Override
  public int hashCode() {
    return Objects.hash(barcode, err);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class ApiV2StocksDelete200Response1DataErrorInner {\n");
    sb.append("    barcode: ").append(toIndentedString(barcode)).append("\n");
    sb.append("    err: ").append(toIndentedString(err)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

