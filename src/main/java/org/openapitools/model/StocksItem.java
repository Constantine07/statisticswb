package org.openapitools.model;

import java.net.URI;
import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import java.math.BigDecimal;
import java.time.OffsetDateTime;
import org.springframework.format.annotation.DateTimeFormat;
import org.openapitools.jackson.nullable.JsonNullable;
import java.time.OffsetDateTime;
import javax.validation.Valid;
import javax.validation.constraints.*;
import io.swagger.v3.oas.annotations.media.Schema;


import java.util.*;
import javax.annotation.Generated;

/**
 * StocksItem
 */

@Generated(value = "org.openapitools.codegen.languages.SpringCodegen", date = "2022-10-16T20:47:05.606+03:00[Europe/Moscow]")
public class StocksItem {

  @JsonProperty("lastChangeDate")
  @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
  private OffsetDateTime lastChangeDate;

  @JsonProperty("supplierArticle")
  private String supplierArticle;

  @JsonProperty("techSize")
  private String techSize;

  @JsonProperty("barcode")
  private String barcode;

  @JsonProperty("quantity")
  private Integer quantity;

  @JsonProperty("isSupply")
  private Boolean isSupply;

  @JsonProperty("isRealization")
  private Boolean isRealization;

  @JsonProperty("quantityFull")
  private Integer quantityFull;

  @JsonProperty("quantityNotInOrders")
  private Integer quantityNotInOrders;

  @JsonProperty("warehouse")
  private Integer warehouse;

  @JsonProperty("warehouseName")
  private String warehouseName;

  @JsonProperty("inWayToClient")
  private Integer inWayToClient;

  @JsonProperty("inWayFromClient")
  private Integer inWayFromClient;

  @JsonProperty("nmId")
  private Integer nmId;

  @JsonProperty("subject")
  private String subject;

  @JsonProperty("category")
  private String category;

  @JsonProperty("daysOnSite")
  private Integer daysOnSite;

  @JsonProperty("brand")
  private String brand;

  @JsonProperty("SCCode")
  private String scCode;

  @JsonProperty("Price")
  private BigDecimal price;

  @JsonProperty("Discount")
  private BigDecimal discount;

  public StocksItem lastChangeDate(OffsetDateTime lastChangeDate) {
    this.lastChangeDate = lastChangeDate;
    return this;
  }

  /**
   * Дата и время обновления информации в сервисе. Это поле соответствует параметру `dateFrom` в запросе. Если часовой пояс не указан, то берется Московское время UTC+3.
   * @return lastChangeDate
  */
  @Valid 
  @Schema(name = "lastChangeDate", description = "Дата и время обновления информации в сервисе. Это поле соответствует параметру `dateFrom` в запросе. Если часовой пояс не указан, то берется Московское время UTC+3.", required = false)
  public OffsetDateTime getLastChangeDate() {
    return lastChangeDate;
  }

  public void setLastChangeDate(OffsetDateTime lastChangeDate) {
    this.lastChangeDate = lastChangeDate;
  }

  public StocksItem supplierArticle(String supplierArticle) {
    this.supplierArticle = supplierArticle;
    return this;
  }

  /**
   * Артикул поставщика
   * @return supplierArticle
  */
  @Size(max = 75) 
  @Schema(name = "supplierArticle", description = "Артикул поставщика", required = false)
  public String getSupplierArticle() {
    return supplierArticle;
  }

  public void setSupplierArticle(String supplierArticle) {
    this.supplierArticle = supplierArticle;
  }

  public StocksItem techSize(String techSize) {
    this.techSize = techSize;
    return this;
  }

  /**
   * Размер
   * @return techSize
  */
  @Size(max = 30) 
  @Schema(name = "techSize", description = "Размер", required = false)
  public String getTechSize() {
    return techSize;
  }

  public void setTechSize(String techSize) {
    this.techSize = techSize;
  }

  public StocksItem barcode(String barcode) {
    this.barcode = barcode;
    return this;
  }

  /**
   * Бар-код
   * @return barcode
  */
  @Size(max = 30) 
  @Schema(name = "barcode", description = "Бар-код", required = false)
  public String getBarcode() {
    return barcode;
  }

  public void setBarcode(String barcode) {
    this.barcode = barcode;
  }

  public StocksItem quantity(Integer quantity) {
    this.quantity = quantity;
    return this;
  }

  /**
   * Количество, доступное для продажи (сколько можно добавить в корзину)
   * @return quantity
  */
  
  @Schema(name = "quantity", description = "Количество, доступное для продажи (сколько можно добавить в корзину)", required = false)
  public Integer getQuantity() {
    return quantity;
  }

  public void setQuantity(Integer quantity) {
    this.quantity = quantity;
  }

  public StocksItem isSupply(Boolean isSupply) {
    this.isSupply = isSupply;
    return this;
  }

  /**
   * Договор поставки
   * @return isSupply
  */
  
  @Schema(name = "isSupply", description = "Договор поставки", required = false)
  public Boolean getIsSupply() {
    return isSupply;
  }

  public void setIsSupply(Boolean isSupply) {
    this.isSupply = isSupply;
  }

  public StocksItem isRealization(Boolean isRealization) {
    this.isRealization = isRealization;
    return this;
  }

  /**
   * Договор реализации
   * @return isRealization
  */
  
  @Schema(name = "isRealization", description = "Договор реализации", required = false)
  public Boolean getIsRealization() {
    return isRealization;
  }

  public void setIsRealization(Boolean isRealization) {
    this.isRealization = isRealization;
  }

  public StocksItem quantityFull(Integer quantityFull) {
    this.quantityFull = quantityFull;
    return this;
  }

  /**
   * Полное (непроданное) количество, которое числится на складе
   * @return quantityFull
  */
  
  @Schema(name = "quantityFull", description = "Полное (непроданное) количество, которое числится на складе", required = false)
  public Integer getQuantityFull() {
    return quantityFull;
  }

  public void setQuantityFull(Integer quantityFull) {
    this.quantityFull = quantityFull;
  }

  public StocksItem quantityNotInOrders(Integer quantityNotInOrders) {
    this.quantityNotInOrders = quantityNotInOrders;
    return this;
  }

  /**
   * Количество не в заказе. Числится на складе, но при этом не числится в незавершенном заказе.
   * @return quantityNotInOrders
  */
  
  @Schema(name = "quantityNotInOrders", description = "Количество не в заказе. Числится на складе, но при этом не числится в незавершенном заказе.", required = false)
  public Integer getQuantityNotInOrders() {
    return quantityNotInOrders;
  }

  public void setQuantityNotInOrders(Integer quantityNotInOrders) {
    this.quantityNotInOrders = quantityNotInOrders;
  }

  public StocksItem warehouse(Integer warehouse) {
    this.warehouse = warehouse;
    return this;
  }

  /**
   * Уникальный идентификатор склада
   * @return warehouse
  */
  
  @Schema(name = "warehouse", description = "Уникальный идентификатор склада", required = false)
  public Integer getWarehouse() {
    return warehouse;
  }

  public void setWarehouse(Integer warehouse) {
    this.warehouse = warehouse;
  }

  public StocksItem warehouseName(String warehouseName) {
    this.warehouseName = warehouseName;
    return this;
  }

  /**
   * Название склада
   * @return warehouseName
  */
  @Size(max = 50) 
  @Schema(name = "warehouseName", description = "Название склада", required = false)
  public String getWarehouseName() {
    return warehouseName;
  }

  public void setWarehouseName(String warehouseName) {
    this.warehouseName = warehouseName;
  }

  public StocksItem inWayToClient(Integer inWayToClient) {
    this.inWayToClient = inWayToClient;
    return this;
  }

  /**
   * В пути к клиенту (штук)
   * @return inWayToClient
  */
  
  @Schema(name = "inWayToClient", description = "В пути к клиенту (штук)", required = false)
  public Integer getInWayToClient() {
    return inWayToClient;
  }

  public void setInWayToClient(Integer inWayToClient) {
    this.inWayToClient = inWayToClient;
  }

  public StocksItem inWayFromClient(Integer inWayFromClient) {
    this.inWayFromClient = inWayFromClient;
    return this;
  }

  /**
   * В пути от клиента (штук)
   * @return inWayFromClient
  */
  
  @Schema(name = "inWayFromClient", description = "В пути от клиента (штук)", required = false)
  public Integer getInWayFromClient() {
    return inWayFromClient;
  }

  public void setInWayFromClient(Integer inWayFromClient) {
    this.inWayFromClient = inWayFromClient;
  }

  public StocksItem nmId(Integer nmId) {
    this.nmId = nmId;
    return this;
  }

  /**
   * Код WB
   * @return nmId
  */
  
  @Schema(name = "nmId", description = "Код WB", required = false)
  public Integer getNmId() {
    return nmId;
  }

  public void setNmId(Integer nmId) {
    this.nmId = nmId;
  }

  public StocksItem subject(String subject) {
    this.subject = subject;
    return this;
  }

  /**
   * Предмет
   * @return subject
  */
  @Size(max = 50) 
  @Schema(name = "subject", description = "Предмет", required = false)
  public String getSubject() {
    return subject;
  }

  public void setSubject(String subject) {
    this.subject = subject;
  }

  public StocksItem category(String category) {
    this.category = category;
    return this;
  }

  /**
   * Категория
   * @return category
  */
  @Size(max = 50) 
  @Schema(name = "category", description = "Категория", required = false)
  public String getCategory() {
    return category;
  }

  public void setCategory(String category) {
    this.category = category;
  }

  public StocksItem daysOnSite(Integer daysOnSite) {
    this.daysOnSite = daysOnSite;
    return this;
  }

  /**
   * Количество дней на сайте
   * @return daysOnSite
  */
  
  @Schema(name = "daysOnSite", description = "Количество дней на сайте", required = false)
  public Integer getDaysOnSite() {
    return daysOnSite;
  }

  public void setDaysOnSite(Integer daysOnSite) {
    this.daysOnSite = daysOnSite;
  }

  public StocksItem brand(String brand) {
    this.brand = brand;
    return this;
  }

  /**
   * Бренд
   * @return brand
  */
  @Size(max = 50) 
  @Schema(name = "brand", description = "Бренд", required = false)
  public String getBrand() {
    return brand;
  }

  public void setBrand(String brand) {
    this.brand = brand;
  }

  public StocksItem scCode(String scCode) {
    this.scCode = scCode;
    return this;
  }

  /**
   * Код контракта
   * @return scCode
  */
  @Size(max = 50) 
  @Schema(name = "SCCode", description = "Код контракта", required = false)
  public String getScCode() {
    return scCode;
  }

  public void setScCode(String scCode) {
    this.scCode = scCode;
  }

  public StocksItem price(BigDecimal price) {
    this.price = price;
    return this;
  }

  /**
   * Цена
   * @return price
  */
  @Valid 
  @Schema(name = "Price", description = "Цена", required = false)
  public BigDecimal getPrice() {
    return price;
  }

  public void setPrice(BigDecimal price) {
    this.price = price;
  }

  public StocksItem discount(BigDecimal discount) {
    this.discount = discount;
    return this;
  }

  /**
   * Скидка
   * @return discount
  */
  @Valid 
  @Schema(name = "Discount", description = "Скидка", required = false)
  public BigDecimal getDiscount() {
    return discount;
  }

  public void setDiscount(BigDecimal discount) {
    this.discount = discount;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    StocksItem stocksItem = (StocksItem) o;
    return Objects.equals(this.lastChangeDate, stocksItem.lastChangeDate) &&
        Objects.equals(this.supplierArticle, stocksItem.supplierArticle) &&
        Objects.equals(this.techSize, stocksItem.techSize) &&
        Objects.equals(this.barcode, stocksItem.barcode) &&
        Objects.equals(this.quantity, stocksItem.quantity) &&
        Objects.equals(this.isSupply, stocksItem.isSupply) &&
        Objects.equals(this.isRealization, stocksItem.isRealization) &&
        Objects.equals(this.quantityFull, stocksItem.quantityFull) &&
        Objects.equals(this.quantityNotInOrders, stocksItem.quantityNotInOrders) &&
        Objects.equals(this.warehouse, stocksItem.warehouse) &&
        Objects.equals(this.warehouseName, stocksItem.warehouseName) &&
        Objects.equals(this.inWayToClient, stocksItem.inWayToClient) &&
        Objects.equals(this.inWayFromClient, stocksItem.inWayFromClient) &&
        Objects.equals(this.nmId, stocksItem.nmId) &&
        Objects.equals(this.subject, stocksItem.subject) &&
        Objects.equals(this.category, stocksItem.category) &&
        Objects.equals(this.daysOnSite, stocksItem.daysOnSite) &&
        Objects.equals(this.brand, stocksItem.brand) &&
        Objects.equals(this.scCode, stocksItem.scCode) &&
        Objects.equals(this.price, stocksItem.price) &&
        Objects.equals(this.discount, stocksItem.discount);
  }

  @Override
  public int hashCode() {
    return Objects.hash(lastChangeDate, supplierArticle, techSize, barcode, quantity, isSupply, isRealization, quantityFull, quantityNotInOrders, warehouse, warehouseName, inWayToClient, inWayFromClient, nmId, subject, category, daysOnSite, brand, scCode, price, discount);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class StocksItem {\n");
    sb.append("    lastChangeDate: ").append(toIndentedString(lastChangeDate)).append("\n");
    sb.append("    supplierArticle: ").append(toIndentedString(supplierArticle)).append("\n");
    sb.append("    techSize: ").append(toIndentedString(techSize)).append("\n");
    sb.append("    barcode: ").append(toIndentedString(barcode)).append("\n");
    sb.append("    quantity: ").append(toIndentedString(quantity)).append("\n");
    sb.append("    isSupply: ").append(toIndentedString(isSupply)).append("\n");
    sb.append("    isRealization: ").append(toIndentedString(isRealization)).append("\n");
    sb.append("    quantityFull: ").append(toIndentedString(quantityFull)).append("\n");
    sb.append("    quantityNotInOrders: ").append(toIndentedString(quantityNotInOrders)).append("\n");
    sb.append("    warehouse: ").append(toIndentedString(warehouse)).append("\n");
    sb.append("    warehouseName: ").append(toIndentedString(warehouseName)).append("\n");
    sb.append("    inWayToClient: ").append(toIndentedString(inWayToClient)).append("\n");
    sb.append("    inWayFromClient: ").append(toIndentedString(inWayFromClient)).append("\n");
    sb.append("    nmId: ").append(toIndentedString(nmId)).append("\n");
    sb.append("    subject: ").append(toIndentedString(subject)).append("\n");
    sb.append("    category: ").append(toIndentedString(category)).append("\n");
    sb.append("    daysOnSite: ").append(toIndentedString(daysOnSite)).append("\n");
    sb.append("    brand: ").append(toIndentedString(brand)).append("\n");
    sb.append("    scCode: ").append(toIndentedString(scCode)).append("\n");
    sb.append("    price: ").append(toIndentedString(price)).append("\n");
    sb.append("    discount: ").append(toIndentedString(discount)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

