package org.openapitools.model;

import java.net.URI;
import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonTypeName;
import java.util.ArrayList;
import java.util.List;
import org.openapitools.model.ApiV2SuppliesGet200ResponseSuppliesInner;
import org.openapitools.jackson.nullable.JsonNullable;
import java.time.OffsetDateTime;
import javax.validation.Valid;
import javax.validation.constraints.*;
import io.swagger.v3.oas.annotations.media.Schema;


import java.util.*;
import javax.annotation.Generated;

/**
 * ApiV2SuppliesGet200Response
 */

@JsonTypeName("_api_v2_supplies_get_200_response")
@Generated(value = "org.openapitools.codegen.languages.SpringCodegen", date = "2022-10-16T20:47:05.606+03:00[Europe/Moscow]")
public class ApiV2SuppliesGet200Response {

  @JsonProperty("supplies")
  @Valid
  private List<ApiV2SuppliesGet200ResponseSuppliesInner> supplies = null;

  public ApiV2SuppliesGet200Response supplies(List<ApiV2SuppliesGet200ResponseSuppliesInner> supplies) {
    this.supplies = supplies;
    return this;
  }

  public ApiV2SuppliesGet200Response addSuppliesItem(ApiV2SuppliesGet200ResponseSuppliesInner suppliesItem) {
    if (this.supplies == null) {
      this.supplies = new ArrayList<>();
    }
    this.supplies.add(suppliesItem);
    return this;
  }

  /**
   * Get supplies
   * @return supplies
  */
  @Valid 
  @Schema(name = "supplies", required = false)
  public List<ApiV2SuppliesGet200ResponseSuppliesInner> getSupplies() {
    return supplies;
  }

  public void setSupplies(List<ApiV2SuppliesGet200ResponseSuppliesInner> supplies) {
    this.supplies = supplies;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    ApiV2SuppliesGet200Response apiV2SuppliesGet200Response = (ApiV2SuppliesGet200Response) o;
    return Objects.equals(this.supplies, apiV2SuppliesGet200Response.supplies);
  }

  @Override
  public int hashCode() {
    return Objects.hash(supplies);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class ApiV2SuppliesGet200Response {\n");
    sb.append("    supplies: ").append(toIndentedString(supplies)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

