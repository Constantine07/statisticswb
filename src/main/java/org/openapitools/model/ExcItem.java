package org.openapitools.model;

import java.net.URI;
import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import java.math.BigDecimal;
import java.time.OffsetDateTime;
import org.springframework.format.annotation.DateTimeFormat;
import org.openapitools.jackson.nullable.JsonNullable;
import java.time.OffsetDateTime;
import javax.validation.Valid;
import javax.validation.constraints.*;
import io.swagger.v3.oas.annotations.media.Schema;


import java.util.*;
import javax.annotation.Generated;

/**
 * ExcItem
 */

@Generated(value = "org.openapitools.codegen.languages.SpringCodegen", date = "2022-10-16T20:47:05.606+03:00[Europe/Moscow]")
public class ExcItem {

  @JsonProperty("id")
  private Integer id;

  @JsonProperty("finishedPrice")
  private BigDecimal finishedPrice;

  @JsonProperty("operationTypeId")
  private Integer operationTypeId;

  @JsonProperty("fiscalDt")
  @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
  private OffsetDateTime fiscalDt;

  @JsonProperty("docNumber")
  private Integer docNumber;

  @JsonProperty("fnNumber")
  private String fnNumber;

  @JsonProperty("regNumber")
  private String regNumber;

  @JsonProperty("excise")
  private String excise;

  @JsonProperty("date")
  @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
  private OffsetDateTime date;

  public ExcItem id(Integer id) {
    this.id = id;
    return this;
  }

  /**
   * Внутренний код операции
   * @return id
  */
  
  @Schema(name = "id", description = "Внутренний код операции", required = false)
  public Integer getId() {
    return id;
  }

  public void setId(Integer id) {
    this.id = id;
  }

  public ExcItem finishedPrice(BigDecimal finishedPrice) {
    this.finishedPrice = finishedPrice;
    return this;
  }

  /**
   * Цена товара с учетом НДС
   * @return finishedPrice
  */
  @Valid 
  @Schema(name = "finishedPrice", description = "Цена товара с учетом НДС", required = false)
  public BigDecimal getFinishedPrice() {
    return finishedPrice;
  }

  public void setFinishedPrice(BigDecimal finishedPrice) {
    this.finishedPrice = finishedPrice;
  }

  public ExcItem operationTypeId(Integer operationTypeId) {
    this.operationTypeId = operationTypeId;
    return this;
  }

  /**
   * Тип операции (`1` - продажа, `2` - возврат)
   * @return operationTypeId
  */
  
  @Schema(name = "operationTypeId", description = "Тип операции (`1` - продажа, `2` - возврат)", required = false)
  public Integer getOperationTypeId() {
    return operationTypeId;
  }

  public void setOperationTypeId(Integer operationTypeId) {
    this.operationTypeId = operationTypeId;
  }

  public ExcItem fiscalDt(OffsetDateTime fiscalDt) {
    this.fiscalDt = fiscalDt;
    return this;
  }

  /**
   * Время фискализации. Передается с указанием часового пояса.
   * @return fiscalDt
  */
  @Valid 
  @Schema(name = "fiscalDt", description = "Время фискализации. Передается с указанием часового пояса.", required = false)
  public OffsetDateTime getFiscalDt() {
    return fiscalDt;
  }

  public void setFiscalDt(OffsetDateTime fiscalDt) {
    this.fiscalDt = fiscalDt;
  }

  public ExcItem docNumber(Integer docNumber) {
    this.docNumber = docNumber;
    return this;
  }

  /**
   * Номер фискального документа
   * @return docNumber
  */
  
  @Schema(name = "docNumber", description = "Номер фискального документа", required = false)
  public Integer getDocNumber() {
    return docNumber;
  }

  public void setDocNumber(Integer docNumber) {
    this.docNumber = docNumber;
  }

  public ExcItem fnNumber(String fnNumber) {
    this.fnNumber = fnNumber;
    return this;
  }

  /**
   * Номер фискального накопителя
   * @return fnNumber
  */
  
  @Schema(name = "fnNumber", description = "Номер фискального накопителя", required = false)
  public String getFnNumber() {
    return fnNumber;
  }

  public void setFnNumber(String fnNumber) {
    this.fnNumber = fnNumber;
  }

  public ExcItem regNumber(String regNumber) {
    this.regNumber = regNumber;
    return this;
  }

  /**
   * Регистрационный номер ККТ
   * @return regNumber
  */
  
  @Schema(name = "regNumber", description = "Регистрационный номер ККТ", required = false)
  public String getRegNumber() {
    return regNumber;
  }

  public void setRegNumber(String regNumber) {
    this.regNumber = regNumber;
  }

  public ExcItem excise(String excise) {
    this.excise = excise;
    return this;
  }

  /**
   * Акциз (он же киз)
   * @return excise
  */
  
  @Schema(name = "excise", description = "Акциз (он же киз)", required = false)
  public String getExcise() {
    return excise;
  }

  public void setExcise(String excise) {
    this.excise = excise;
  }

  public ExcItem date(OffsetDateTime date) {
    this.date = date;
    return this;
  }

  /**
   * Дата появления данных в системе. Передается с указанием часового пояса.
   * @return date
  */
  @Valid 
  @Schema(name = "date", description = "Дата появления данных в системе. Передается с указанием часового пояса.", required = false)
  public OffsetDateTime getDate() {
    return date;
  }

  public void setDate(OffsetDateTime date) {
    this.date = date;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    ExcItem excItem = (ExcItem) o;
    return Objects.equals(this.id, excItem.id) &&
        Objects.equals(this.finishedPrice, excItem.finishedPrice) &&
        Objects.equals(this.operationTypeId, excItem.operationTypeId) &&
        Objects.equals(this.fiscalDt, excItem.fiscalDt) &&
        Objects.equals(this.docNumber, excItem.docNumber) &&
        Objects.equals(this.fnNumber, excItem.fnNumber) &&
        Objects.equals(this.regNumber, excItem.regNumber) &&
        Objects.equals(this.excise, excItem.excise) &&
        Objects.equals(this.date, excItem.date);
  }

  @Override
  public int hashCode() {
    return Objects.hash(id, finishedPrice, operationTypeId, fiscalDt, docNumber, fnNumber, regNumber, excise, date);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class ExcItem {\n");
    sb.append("    id: ").append(toIndentedString(id)).append("\n");
    sb.append("    finishedPrice: ").append(toIndentedString(finishedPrice)).append("\n");
    sb.append("    operationTypeId: ").append(toIndentedString(operationTypeId)).append("\n");
    sb.append("    fiscalDt: ").append(toIndentedString(fiscalDt)).append("\n");
    sb.append("    docNumber: ").append(toIndentedString(docNumber)).append("\n");
    sb.append("    fnNumber: ").append(toIndentedString(fnNumber)).append("\n");
    sb.append("    regNumber: ").append(toIndentedString(regNumber)).append("\n");
    sb.append("    excise: ").append(toIndentedString(excise)).append("\n");
    sb.append("    date: ").append(toIndentedString(date)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

