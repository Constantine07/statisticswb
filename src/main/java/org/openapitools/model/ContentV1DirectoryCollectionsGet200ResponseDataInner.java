package org.openapitools.model;

import java.net.URI;
import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonTypeName;
import org.openapitools.jackson.nullable.JsonNullable;
import java.time.OffsetDateTime;
import javax.validation.Valid;
import javax.validation.constraints.*;
import io.swagger.v3.oas.annotations.media.Schema;


import java.util.*;
import javax.annotation.Generated;

/**
 * ContentV1DirectoryCollectionsGet200ResponseDataInner
 */

@JsonTypeName("_content_v1_directory_collections_get_200_response_data_inner")
@Generated(value = "org.openapitools.codegen.languages.SpringCodegen", date = "2022-10-16T20:47:05.606+03:00[Europe/Moscow]")
public class ContentV1DirectoryCollectionsGet200ResponseDataInner {

  @JsonProperty("id")
  private Integer id;

  @JsonProperty("name")
  private String name;

  public ContentV1DirectoryCollectionsGet200ResponseDataInner id(Integer id) {
    this.id = id;
    return this;
  }

  /**
   * Идентификатор значения характеристики
   * @return id
  */
  
  @Schema(name = "id", example = "26764163", description = "Идентификатор значения характеристики", required = false)
  public Integer getId() {
    return id;
  }

  public void setId(Integer id) {
    this.id = id;
  }

  public ContentV1DirectoryCollectionsGet200ResponseDataInner name(String name) {
    this.name = name;
    return this;
  }

  /**
   * Значение характеристики
   * @return name
  */
  
  @Schema(name = "name", example = "2022-2023 зима осень", description = "Значение характеристики", required = false)
  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    ContentV1DirectoryCollectionsGet200ResponseDataInner contentV1DirectoryCollectionsGet200ResponseDataInner = (ContentV1DirectoryCollectionsGet200ResponseDataInner) o;
    return Objects.equals(this.id, contentV1DirectoryCollectionsGet200ResponseDataInner.id) &&
        Objects.equals(this.name, contentV1DirectoryCollectionsGet200ResponseDataInner.name);
  }

  @Override
  public int hashCode() {
    return Objects.hash(id, name);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class ContentV1DirectoryCollectionsGet200ResponseDataInner {\n");
    sb.append("    id: ").append(toIndentedString(id)).append("\n");
    sb.append("    name: ").append(toIndentedString(name)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

