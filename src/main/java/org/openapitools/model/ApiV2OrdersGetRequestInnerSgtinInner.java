package org.openapitools.model;

import java.net.URI;
import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonTypeName;
import org.openapitools.jackson.nullable.JsonNullable;
import java.time.OffsetDateTime;
import javax.validation.Valid;
import javax.validation.constraints.*;
import io.swagger.v3.oas.annotations.media.Schema;


import java.util.*;
import javax.annotation.Generated;

/**
 * ApiV2OrdersGetRequestInnerSgtinInner
 */

@JsonTypeName("_api_v2_orders_get_request_inner_sgtin_inner")
@Generated(value = "org.openapitools.codegen.languages.SpringCodegen", date = "2022-10-16T20:47:05.606+03:00[Europe/Moscow]")
public class ApiV2OrdersGetRequestInnerSgtinInner {

  @JsonProperty("code")
  private String code;

  @JsonProperty("numerator")
  private Integer numerator;

  @JsonProperty("denominator")
  private Integer denominator;

  @JsonProperty("sid")
  private Integer sid;

  public ApiV2OrdersGetRequestInnerSgtinInner code(String code) {
    this.code = code;
    return this;
  }

  /**
   * Код маркировки на упаковке. 
   * @return code
  */
  
  @Schema(name = "code", example = "...", description = "Код маркировки на упаковке. ", required = false)
  public String getCode() {
    return code;
  }

  public void setCode(String code) {
    this.code = code;
  }

  public ApiV2OrdersGetRequestInnerSgtinInner numerator(Integer numerator) {
    this.numerator = numerator;
    return this;
  }

  /**
   * Числитель, количество единиц товара из упаковки (1, если продается целая упаковка). 
   * @return numerator
  */
  
  @Schema(name = "numerator", example = "1", description = "Числитель, количество единиц товара из упаковки (1, если продается целая упаковка). ", required = false)
  public Integer getNumerator() {
    return numerator;
  }

  public void setNumerator(Integer numerator) {
    this.numerator = numerator;
  }

  public ApiV2OrdersGetRequestInnerSgtinInner denominator(Integer denominator) {
    this.denominator = denominator;
    return this;
  }

  /**
   * Знаменатель, количество единиц товара в упаковке (1, если продается целая упаковка, т.е. для полной упаковки числитель = знаменатель = 1) 
   * @return denominator
  */
  
  @Schema(name = "denominator", example = "1", description = "Знаменатель, количество единиц товара в упаковке (1, если продается целая упаковка, т.е. для полной упаковки числитель = знаменатель = 1) ", required = false)
  public Integer getDenominator() {
    return denominator;
  }

  public void setDenominator(Integer denominator) {
    this.denominator = denominator;
  }

  public ApiV2OrdersGetRequestInnerSgtinInner sid(Integer sid) {
    this.sid = sid;
    return this;
  }

  /**
   * Идентификатор места деятельности субъекта обращения в ИС МДЛП. 
   * @return sid
  */
  
  @Schema(name = "sid", example = "123", description = "Идентификатор места деятельности субъекта обращения в ИС МДЛП. ", required = false)
  public Integer getSid() {
    return sid;
  }

  public void setSid(Integer sid) {
    this.sid = sid;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    ApiV2OrdersGetRequestInnerSgtinInner apiV2OrdersGetRequestInnerSgtinInner = (ApiV2OrdersGetRequestInnerSgtinInner) o;
    return Objects.equals(this.code, apiV2OrdersGetRequestInnerSgtinInner.code) &&
        Objects.equals(this.numerator, apiV2OrdersGetRequestInnerSgtinInner.numerator) &&
        Objects.equals(this.denominator, apiV2OrdersGetRequestInnerSgtinInner.denominator) &&
        Objects.equals(this.sid, apiV2OrdersGetRequestInnerSgtinInner.sid);
  }

  @Override
  public int hashCode() {
    return Objects.hash(code, numerator, denominator, sid);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class ApiV2OrdersGetRequestInnerSgtinInner {\n");
    sb.append("    code: ").append(toIndentedString(code)).append("\n");
    sb.append("    numerator: ").append(toIndentedString(numerator)).append("\n");
    sb.append("    denominator: ").append(toIndentedString(denominator)).append("\n");
    sb.append("    sid: ").append(toIndentedString(sid)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

