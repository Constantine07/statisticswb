package org.openapitools.model;

import java.net.URI;
import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonTypeName;
import org.openapitools.jackson.nullable.JsonNullable;
import java.time.OffsetDateTime;
import javax.validation.Valid;
import javax.validation.constraints.*;
import io.swagger.v3.oas.annotations.media.Schema;


import java.util.*;
import javax.annotation.Generated;

/**
 * ContentV1CardsListPostRequestSort
 */

@JsonTypeName("_content_v1_cards_list_post_request_sort")
@Generated(value = "org.openapitools.codegen.languages.SpringCodegen", date = "2022-10-16T20:47:05.606+03:00[Europe/Moscow]")
public class ContentV1CardsListPostRequestSort {

  @JsonProperty("limit")
  private Integer limit;

  @JsonProperty("offset")
  private Integer offset;

  @JsonProperty("searchValue")
  private String searchValue;

  @JsonProperty("sortColumn")
  private String sortColumn;

  @JsonProperty("ascending")
  private Boolean ascending;

  public ContentV1CardsListPostRequestSort limit(Integer limit) {
    this.limit = limit;
    return this;
  }

  /**
   * Максимальное кол-во карточек, которое необходимо вывести. (Максимальное значение - `1000`, по умолчанию `10`)
   * @return limit
  */
  
  @Schema(name = "limit", example = "1", description = "Максимальное кол-во карточек, которое необходимо вывести. (Максимальное значение - `1000`, по умолчанию `10`)", required = false)
  public Integer getLimit() {
    return limit;
  }

  public void setLimit(Integer limit) {
    this.limit = limit;
  }

  public ContentV1CardsListPostRequestSort offset(Integer offset) {
    this.offset = offset;
    return this;
  }

  /**
   * Смещение от начала списка с указанными критериями поиска и сортировки.
   * @return offset
  */
  
  @Schema(name = "offset", example = "50", description = "Смещение от начала списка с указанными критериями поиска и сортировки.", required = false)
  public Integer getOffset() {
    return offset;
  }

  public void setOffset(Integer offset) {
    this.offset = offset;
  }

  public ContentV1CardsListPostRequestSort searchValue(String searchValue) {
    this.searchValue = searchValue;
    return this;
  }

  /**
   * Значение для поиска. Всегда строка. Поиск работает по номенклатурам и артикулам.
   * @return searchValue
  */
  
  @Schema(name = "searchValue", example = "", description = "Значение для поиска. Всегда строка. Поиск работает по номенклатурам и артикулам.", required = false)
  public String getSearchValue() {
    return searchValue;
  }

  public void setSearchValue(String searchValue) {
    this.searchValue = searchValue;
  }

  public ContentV1CardsListPostRequestSort sortColumn(String sortColumn) {
    this.sortColumn = sortColumn;
    return this;
  }

  /**
   * Выбор параметра для сортировки. По умолчанию `updateAt desc` - дата обновления по убыванию. Доступен так же параметр сортировки - `hasPhoto`.
   * @return sortColumn
  */
  
  @Schema(name = "sortColumn", example = "updateAt", description = "Выбор параметра для сортировки. По умолчанию `updateAt desc` - дата обновления по убыванию. Доступен так же параметр сортировки - `hasPhoto`.", required = false)
  public String getSortColumn() {
    return sortColumn;
  }

  public void setSortColumn(String sortColumn) {
    this.sortColumn = sortColumn;
  }

  public ContentV1CardsListPostRequestSort ascending(Boolean ascending) {
    this.ascending = ascending;
    return this;
  }

  /**
   * Направление сортировки. `true` - по возрастанию, `false` - по убыванию.
   * @return ascending
  */
  
  @Schema(name = "ascending", example = "false", description = "Направление сортировки. `true` - по возрастанию, `false` - по убыванию.", required = false)
  public Boolean getAscending() {
    return ascending;
  }

  public void setAscending(Boolean ascending) {
    this.ascending = ascending;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    ContentV1CardsListPostRequestSort contentV1CardsListPostRequestSort = (ContentV1CardsListPostRequestSort) o;
    return Objects.equals(this.limit, contentV1CardsListPostRequestSort.limit) &&
        Objects.equals(this.offset, contentV1CardsListPostRequestSort.offset) &&
        Objects.equals(this.searchValue, contentV1CardsListPostRequestSort.searchValue) &&
        Objects.equals(this.sortColumn, contentV1CardsListPostRequestSort.sortColumn) &&
        Objects.equals(this.ascending, contentV1CardsListPostRequestSort.ascending);
  }

  @Override
  public int hashCode() {
    return Objects.hash(limit, offset, searchValue, sortColumn, ascending);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class ContentV1CardsListPostRequestSort {\n");
    sb.append("    limit: ").append(toIndentedString(limit)).append("\n");
    sb.append("    offset: ").append(toIndentedString(offset)).append("\n");
    sb.append("    searchValue: ").append(toIndentedString(searchValue)).append("\n");
    sb.append("    sortColumn: ").append(toIndentedString(sortColumn)).append("\n");
    sb.append("    ascending: ").append(toIndentedString(ascending)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

