package org.openapitools.model;

import java.net.URI;
import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonTypeName;
import com.fasterxml.jackson.annotation.JsonValue;
import java.util.Arrays;
import org.openapitools.jackson.nullable.JsonNullable;
import java.time.OffsetDateTime;
import javax.validation.Valid;
import javax.validation.constraints.*;
import io.swagger.v3.oas.annotations.media.Schema;


import java.util.*;
import javax.annotation.Generated;

/**
 * ApiV2SuppliesIdBarcodeGet200Response
 */

@JsonTypeName("_api_v2_supplies__id__barcode_get_200_response")
@Generated(value = "org.openapitools.codegen.languages.SpringCodegen", date = "2022-10-16T20:47:05.606+03:00[Europe/Moscow]")
public class ApiV2SuppliesIdBarcodeGet200Response {

  /**
   * Формат передаваемого файла.
   */
  public enum MimeTypeEnum {
    APPLICATION_PDF("application/pdf"),
    
    IMAGE_SVG_XML("image/svg+xml");

    private String value;

    MimeTypeEnum(String value) {
      this.value = value;
    }

    @JsonValue
    public String getValue() {
      return value;
    }

    @Override
    public String toString() {
      return String.valueOf(value);
    }

    @JsonCreator
    public static MimeTypeEnum fromValue(String value) {
      for (MimeTypeEnum b : MimeTypeEnum.values()) {
        if (b.value.equals(value)) {
          return b;
        }
      }
      throw new IllegalArgumentException("Unexpected value '" + value + "'");
    }
  }

  @JsonProperty("mimeType")
  private MimeTypeEnum mimeType;

  @JsonProperty("name")
  private String name;

  @JsonProperty("file")
  private byte[] file;

  public ApiV2SuppliesIdBarcodeGet200Response mimeType(MimeTypeEnum mimeType) {
    this.mimeType = mimeType;
    return this;
  }

  /**
   * Формат передаваемого файла.
   * @return mimeType
  */
  
  @Schema(name = "mimeType", description = "Формат передаваемого файла.", required = false)
  public MimeTypeEnum getMimeType() {
    return mimeType;
  }

  public void setMimeType(MimeTypeEnum mimeType) {
    this.mimeType = mimeType;
  }

  public ApiV2SuppliesIdBarcodeGet200Response name(String name) {
    this.name = name;
    return this;
  }

  /**
   * Идентификатор поставки
   * @return name
  */
  
  @Schema(name = "name", example = "WB-GI-1234567", description = "Идентификатор поставки", required = false)
  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public ApiV2SuppliesIdBarcodeGet200Response file(byte[] file) {
    this.file = file;
    return this;
  }

  /**
   * Штрихкод поставки генерируется в формате code-128. Сам файл приходит в массиве байтов, закодированном в base64.
   * @return file
  */
  
  @Schema(name = "file", example = "[B@4792f119", description = "Штрихкод поставки генерируется в формате code-128. Сам файл приходит в массиве байтов, закодированном в base64.", required = false)
  public byte[] getFile() {
    return file;
  }

  public void setFile(byte[] file) {
    this.file = file;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    ApiV2SuppliesIdBarcodeGet200Response apiV2SuppliesIdBarcodeGet200Response = (ApiV2SuppliesIdBarcodeGet200Response) o;
    return Objects.equals(this.mimeType, apiV2SuppliesIdBarcodeGet200Response.mimeType) &&
        Objects.equals(this.name, apiV2SuppliesIdBarcodeGet200Response.name) &&
        Arrays.equals(this.file, apiV2SuppliesIdBarcodeGet200Response.file);
  }

  @Override
  public int hashCode() {
    return Objects.hash(mimeType, name, Arrays.hashCode(file));
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class ApiV2SuppliesIdBarcodeGet200Response {\n");
    sb.append("    mimeType: ").append(toIndentedString(mimeType)).append("\n");
    sb.append("    name: ").append(toIndentedString(name)).append("\n");
    sb.append("    file: ").append(toIndentedString(file)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

