package org.openapitools.model;

import java.net.URI;
import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonTypeName;
import java.math.BigDecimal;
import org.openapitools.jackson.nullable.JsonNullable;
import java.time.OffsetDateTime;
import javax.validation.Valid;
import javax.validation.constraints.*;
import io.swagger.v3.oas.annotations.media.Schema;


import java.util.*;
import javax.annotation.Generated;

/**
 * Детализованный адрес клиента для доставки (если применимо). &lt;br&gt;Некоторые из полей могут прийти пустыми из-за специфики адреса. 
 */

@Schema(name = "deliveryAddressDetails", description = "Детализованный адрес клиента для доставки (если применимо). <br>Некоторые из полей могут прийти пустыми из-за специфики адреса. ")
@JsonTypeName("deliveryAddressDetails")
@Generated(value = "org.openapitools.codegen.languages.SpringCodegen", date = "2022-10-16T20:47:05.606+03:00[Europe/Moscow]")
public class DeliveryAddressDetails {

  @JsonProperty("province")
  private String province;

  @JsonProperty("area")
  private String area;

  @JsonProperty("city")
  private String city;

  @JsonProperty("street")
  private String street;

  @JsonProperty("home")
  private String home;

  @JsonProperty("flat")
  private String flat;

  @JsonProperty("entrance")
  private String entrance;

  @JsonProperty("longitude")
  private BigDecimal longitude;

  @JsonProperty("latitude")
  private BigDecimal latitude;

  public DeliveryAddressDetails province(String province) {
    this.province = province;
    return this;
  }

  /**
   * Область.
   * @return province
  */
  
  @Schema(name = "province", example = "Челябинская область", description = "Область.", required = false)
  public String getProvince() {
    return province;
  }

  public void setProvince(String province) {
    this.province = province;
  }

  public DeliveryAddressDetails area(String area) {
    this.area = area;
    return this;
  }

  /**
   * Район.
   * @return area
  */
  
  @Schema(name = "area", example = "Челябинск", description = "Район.", required = false)
  public String getArea() {
    return area;
  }

  public void setArea(String area) {
    this.area = area;
  }

  public DeliveryAddressDetails city(String city) {
    this.city = city;
    return this;
  }

  /**
   * Город.
   * @return city
  */
  
  @Schema(name = "city", example = "Челябинск", description = "Город.", required = false)
  public String getCity() {
    return city;
  }

  public void setCity(String city) {
    this.city = city;
  }

  public DeliveryAddressDetails street(String street) {
    this.street = street;
    return this;
  }

  /**
   * Улица.
   * @return street
  */
  
  @Schema(name = "street", example = "51-я улица Арабкира", description = "Улица.", required = false)
  public String getStreet() {
    return street;
  }

  public void setStreet(String street) {
    this.street = street;
  }

  public DeliveryAddressDetails home(String home) {
    this.home = home;
    return this;
  }

  /**
   * Номер дома.
   * @return home
  */
  
  @Schema(name = "home", example = "10А", description = "Номер дома.", required = false)
  public String getHome() {
    return home;
  }

  public void setHome(String home) {
    this.home = home;
  }

  public DeliveryAddressDetails flat(String flat) {
    this.flat = flat;
    return this;
  }

  /**
   * Номер квартиры.
   * @return flat
  */
  
  @Schema(name = "flat", example = "42", description = "Номер квартиры.", required = false)
  public String getFlat() {
    return flat;
  }

  public void setFlat(String flat) {
    this.flat = flat;
  }

  public DeliveryAddressDetails entrance(String entrance) {
    this.entrance = entrance;
    return this;
  }

  /**
   * Подъезд.
   * @return entrance
  */
  
  @Schema(name = "entrance", example = "3", description = "Подъезд.", required = false)
  public String getEntrance() {
    return entrance;
  }

  public void setEntrance(String entrance) {
    this.entrance = entrance;
  }

  public DeliveryAddressDetails longitude(BigDecimal longitude) {
    this.longitude = longitude;
    return this;
  }

  /**
   * Координата долготы.
   * @return longitude
  */
  @Valid 
  @Schema(name = "longitude", example = "44.519068", description = "Координата долготы.", required = false)
  public BigDecimal getLongitude() {
    return longitude;
  }

  public void setLongitude(BigDecimal longitude) {
    this.longitude = longitude;
  }

  public DeliveryAddressDetails latitude(BigDecimal latitude) {
    this.latitude = latitude;
    return this;
  }

  /**
   * Координата широты.
   * @return latitude
  */
  @Valid 
  @Schema(name = "latitude", example = "40.20192", description = "Координата широты.", required = false)
  public BigDecimal getLatitude() {
    return latitude;
  }

  public void setLatitude(BigDecimal latitude) {
    this.latitude = latitude;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    DeliveryAddressDetails deliveryAddressDetails = (DeliveryAddressDetails) o;
    return Objects.equals(this.province, deliveryAddressDetails.province) &&
        Objects.equals(this.area, deliveryAddressDetails.area) &&
        Objects.equals(this.city, deliveryAddressDetails.city) &&
        Objects.equals(this.street, deliveryAddressDetails.street) &&
        Objects.equals(this.home, deliveryAddressDetails.home) &&
        Objects.equals(this.flat, deliveryAddressDetails.flat) &&
        Objects.equals(this.entrance, deliveryAddressDetails.entrance) &&
        Objects.equals(this.longitude, deliveryAddressDetails.longitude) &&
        Objects.equals(this.latitude, deliveryAddressDetails.latitude);
  }

  @Override
  public int hashCode() {
    return Objects.hash(province, area, city, street, home, flat, entrance, longitude, latitude);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class DeliveryAddressDetails {\n");
    sb.append("    province: ").append(toIndentedString(province)).append("\n");
    sb.append("    area: ").append(toIndentedString(area)).append("\n");
    sb.append("    city: ").append(toIndentedString(city)).append("\n");
    sb.append("    street: ").append(toIndentedString(street)).append("\n");
    sb.append("    home: ").append(toIndentedString(home)).append("\n");
    sb.append("    flat: ").append(toIndentedString(flat)).append("\n");
    sb.append("    entrance: ").append(toIndentedString(entrance)).append("\n");
    sb.append("    longitude: ").append(toIndentedString(longitude)).append("\n");
    sb.append("    latitude: ").append(toIndentedString(latitude)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

