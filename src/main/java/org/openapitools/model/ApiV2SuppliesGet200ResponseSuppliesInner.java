package org.openapitools.model;

import java.net.URI;
import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonTypeName;
import org.openapitools.jackson.nullable.JsonNullable;
import java.time.OffsetDateTime;
import javax.validation.Valid;
import javax.validation.constraints.*;
import io.swagger.v3.oas.annotations.media.Schema;


import java.util.*;
import javax.annotation.Generated;

/**
 * ApiV2SuppliesGet200ResponseSuppliesInner
 */

@JsonTypeName("_api_v2_supplies_get_200_response_supplies_inner")
@Generated(value = "org.openapitools.codegen.languages.SpringCodegen", date = "2022-10-16T20:47:05.606+03:00[Europe/Moscow]")
public class ApiV2SuppliesGet200ResponseSuppliesInner {

  @JsonProperty("supplyId")
  private String supplyId;

  public ApiV2SuppliesGet200ResponseSuppliesInner supplyId(String supplyId) {
    this.supplyId = supplyId;
    return this;
  }

  /**
   * Идентификатор поставки
   * @return supplyId
  */
  
  @Schema(name = "supplyId", example = "WB-GI-1234567", description = "Идентификатор поставки", required = false)
  public String getSupplyId() {
    return supplyId;
  }

  public void setSupplyId(String supplyId) {
    this.supplyId = supplyId;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    ApiV2SuppliesGet200ResponseSuppliesInner apiV2SuppliesGet200ResponseSuppliesInner = (ApiV2SuppliesGet200ResponseSuppliesInner) o;
    return Objects.equals(this.supplyId, apiV2SuppliesGet200ResponseSuppliesInner.supplyId);
  }

  @Override
  public int hashCode() {
    return Objects.hash(supplyId);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class ApiV2SuppliesGet200ResponseSuppliesInner {\n");
    sb.append("    supplyId: ").append(toIndentedString(supplyId)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

