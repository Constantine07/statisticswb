package org.openapitools.model;

import java.net.URI;
import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonTypeName;
import org.openapitools.model.ApiV2OrdersStickersPost200ResponseDataInnerStickerWbStickerIdParts;
import org.openapitools.jackson.nullable.JsonNullable;
import java.time.OffsetDateTime;
import javax.validation.Valid;
import javax.validation.constraints.*;
import io.swagger.v3.oas.annotations.media.Schema;


import java.util.*;
import javax.annotation.Generated;

/**
 * ApiV2OrdersStickersPost200ResponseDataInnerSticker
 */

@JsonTypeName("_api_v2_orders_stickers_post_200_response_data_inner_sticker")
@Generated(value = "org.openapitools.codegen.languages.SpringCodegen", date = "2022-10-16T20:47:05.606+03:00[Europe/Moscow]")
public class ApiV2OrdersStickersPost200ResponseDataInnerSticker {

  @JsonProperty("wbStickerId")
  private Integer wbStickerId;

  @JsonProperty("wbStickerIdParts")
  private ApiV2OrdersStickersPost200ResponseDataInnerStickerWbStickerIdParts wbStickerIdParts;

  @JsonProperty("wbStickerEncoded")
  private String wbStickerEncoded;

  @JsonProperty("wbStickerSvgBase64")
  private String wbStickerSvgBase64;

  @JsonProperty("wbStickerZplV")
  private String wbStickerZplV;

  @JsonProperty("wbStickerZpl")
  private String wbStickerZpl;

  public ApiV2OrdersStickersPost200ResponseDataInnerSticker wbStickerId(Integer wbStickerId) {
    this.wbStickerId = wbStickerId;
    return this;
  }

  /**
   * Идентификатор этикетки.
   * @return wbStickerId
  */
  
  @Schema(name = "wbStickerId", example = "2316489753", description = "Идентификатор этикетки.", required = false)
  public Integer getWbStickerId() {
    return wbStickerId;
  }

  public void setWbStickerId(Integer wbStickerId) {
    this.wbStickerId = wbStickerId;
  }

  public ApiV2OrdersStickersPost200ResponseDataInnerSticker wbStickerIdParts(ApiV2OrdersStickersPost200ResponseDataInnerStickerWbStickerIdParts wbStickerIdParts) {
    this.wbStickerIdParts = wbStickerIdParts;
    return this;
  }

  /**
   * Get wbStickerIdParts
   * @return wbStickerIdParts
  */
  @Valid 
  @Schema(name = "wbStickerIdParts", required = false)
  public ApiV2OrdersStickersPost200ResponseDataInnerStickerWbStickerIdParts getWbStickerIdParts() {
    return wbStickerIdParts;
  }

  public void setWbStickerIdParts(ApiV2OrdersStickersPost200ResponseDataInnerStickerWbStickerIdParts wbStickerIdParts) {
    this.wbStickerIdParts = wbStickerIdParts;
  }

  public ApiV2OrdersStickersPost200ResponseDataInnerSticker wbStickerEncoded(String wbStickerEncoded) {
    this.wbStickerEncoded = wbStickerEncoded;
    return this;
  }

  /**
   * Закодированное значение стикера (представляется на этикетке в формате Code-128). 
   * @return wbStickerEncoded
  */
  
  @Schema(name = "wbStickerEncoded", example = "!uKEtQZVx", description = "Закодированное значение стикера (представляется на этикетке в формате Code-128). ", required = false)
  public String getWbStickerEncoded() {
    return wbStickerEncoded;
  }

  public void setWbStickerEncoded(String wbStickerEncoded) {
    this.wbStickerEncoded = wbStickerEncoded;
  }

  public ApiV2OrdersStickersPost200ResponseDataInnerSticker wbStickerSvgBase64(String wbStickerSvgBase64) {
    this.wbStickerSvgBase64 = wbStickerSvgBase64;
    return this;
  }

  /**
   * Полное представление этикетки в векторном формате. 
   * @return wbStickerSvgBase64
  */
  
  @Schema(name = "wbStickerSvgBase64", example = "PD94bWwgdmVyc2lvbj0iMS4wIj8+CjwhLS0gR2VuZXJhdGVkIGJ5IFNWR28gLS0+Cjxzdmcgd2lkdGg9IjQwMCIgaGVpZ2h0PSIzMDAiCiAgICAgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIgogICAgIHhtbG5zOnhsaW5rPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5L3hsaW5rIj4KPHJlY3QgeD0iMCIgeT0iMCIgd2lkdGg9IjQwMCIgaGVpZQiIGhlaWdodD0iMTcwIiBzdHlsZT0iZmlsbDpibGFjayIgLz4KPHJlY3QgeD0iMzE4IiB5PSIyMCIgd2lkdGg9IjYiIGhlaWdodD0iMTcwIiBzdHlsZT0iZmlsbDpibGFjayIgLz4KPHJlY3QgeD0iMzI2IiB5PSIyMCIgd2lkdGg9IjIiIGhlaWdodD0iMTcwIiBzdHlsZT0iZmlsbDpibGFjayIgLz4KPHJlY3QgeD0iMzMwIiB5PSIyMCIgd2lkdGg9IjQiIGhlaWdodD0iMTcwIiBzdHlsZT0iZmlsbDpibGFjayIgLz4KPHJlY3QgeD0iMjAiIHk9IjIwMCIgd2lkdGg9IjM1MCIgaGVpZ2h0PSI5MCIgc3R5bGU9ImZpbGw6YmxhY2siIC8+Cjx0ZXh0IHg9IjMwIiB5PSIyNDAiIHN0eWxlPSJmaWxsOndoaXRlO2ZvbnQtc2l6ZTozMHB0O3RleHQtYW5jaG9yOnN0YXJ0IiA+MjMxNjQ4PC90ZXh0Pgo8dGV4dCB4PSIzNTAiIHk9IjI3MCIgc3R5bGU9ImZpbGw6d2hpdGU7Zm9udC1zaXplOjUwcHQ7dGV4dC1hbmNob3I6ZW5kIiA+OTc1MzwvdGV4dD4KPC9zdmc+Cg==", description = "Полное представление этикетки в векторном формате. ", required = false)
  public String getWbStickerSvgBase64() {
    return wbStickerSvgBase64;
  }

  public void setWbStickerSvgBase64(String wbStickerSvgBase64) {
    this.wbStickerSvgBase64 = wbStickerSvgBase64;
  }

  public ApiV2OrdersStickersPost200ResponseDataInnerSticker wbStickerZplV(String wbStickerZplV) {
    this.wbStickerZplV = wbStickerZplV;
    return this;
  }

  /**
   * Полное представление этикетки в формате ZPL (вертикальное положение). 
   * @return wbStickerZplV
  */
  
  @Schema(name = "wbStickerZplV", example = "^XZ ^XA ^CI28 ^CF0 ^LS0 ^FT26,160^BY2^BCN,140,N,N^FD!uKEtQZVx^FS ^FO30,177^FB200,1,0,L^AZN,35,40^FD231648^FS ^FO160,173^FB110,1,0,R^AZN,68,50^FD9753^FS ^LRY^FO20,170^GB280,60,50^FS^LRN ^XZ", description = "Полное представление этикетки в формате ZPL (вертикальное положение). ", required = false)
  public String getWbStickerZplV() {
    return wbStickerZplV;
  }

  public void setWbStickerZplV(String wbStickerZplV) {
    this.wbStickerZplV = wbStickerZplV;
  }

  public ApiV2OrdersStickersPost200ResponseDataInnerSticker wbStickerZpl(String wbStickerZpl) {
    this.wbStickerZpl = wbStickerZpl;
    return this;
  }

  /**
   * Полное представление этикетки в формате ZPL (горизонтальное положение) 
   * @return wbStickerZpl
  */
  
  @Schema(name = "wbStickerZpl", example = "^XZ ^XA ^CI28 ^CF0 ^LS0 ^FO20,26^BY2^BCR,140,N,N^FD!uKEtQZVx^FS ^FO177,87^FB200,1,0,L^AZB,35,40^FD231648^FS ^FO173,50^FB110,1,0,R^AZB,68,50^FD9753^FS ^LRY^FO170,20^GB60,280,50^FS^LRN ^XZ", description = "Полное представление этикетки в формате ZPL (горизонтальное положение) ", required = false)
  public String getWbStickerZpl() {
    return wbStickerZpl;
  }

  public void setWbStickerZpl(String wbStickerZpl) {
    this.wbStickerZpl = wbStickerZpl;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    ApiV2OrdersStickersPost200ResponseDataInnerSticker apiV2OrdersStickersPost200ResponseDataInnerSticker = (ApiV2OrdersStickersPost200ResponseDataInnerSticker) o;
    return Objects.equals(this.wbStickerId, apiV2OrdersStickersPost200ResponseDataInnerSticker.wbStickerId) &&
        Objects.equals(this.wbStickerIdParts, apiV2OrdersStickersPost200ResponseDataInnerSticker.wbStickerIdParts) &&
        Objects.equals(this.wbStickerEncoded, apiV2OrdersStickersPost200ResponseDataInnerSticker.wbStickerEncoded) &&
        Objects.equals(this.wbStickerSvgBase64, apiV2OrdersStickersPost200ResponseDataInnerSticker.wbStickerSvgBase64) &&
        Objects.equals(this.wbStickerZplV, apiV2OrdersStickersPost200ResponseDataInnerSticker.wbStickerZplV) &&
        Objects.equals(this.wbStickerZpl, apiV2OrdersStickersPost200ResponseDataInnerSticker.wbStickerZpl);
  }

  @Override
  public int hashCode() {
    return Objects.hash(wbStickerId, wbStickerIdParts, wbStickerEncoded, wbStickerSvgBase64, wbStickerZplV, wbStickerZpl);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class ApiV2OrdersStickersPost200ResponseDataInnerSticker {\n");
    sb.append("    wbStickerId: ").append(toIndentedString(wbStickerId)).append("\n");
    sb.append("    wbStickerIdParts: ").append(toIndentedString(wbStickerIdParts)).append("\n");
    sb.append("    wbStickerEncoded: ").append(toIndentedString(wbStickerEncoded)).append("\n");
    sb.append("    wbStickerSvgBase64: ").append(toIndentedString(wbStickerSvgBase64)).append("\n");
    sb.append("    wbStickerZplV: ").append(toIndentedString(wbStickerZplV)).append("\n");
    sb.append("    wbStickerZpl: ").append(toIndentedString(wbStickerZpl)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

