package org.openapitools.model;

import java.net.URI;
import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonTypeName;
import org.openapitools.jackson.nullable.JsonNullable;
import java.time.OffsetDateTime;
import javax.validation.Valid;
import javax.validation.constraints.*;
import io.swagger.v3.oas.annotations.media.Schema;


import java.util.*;
import javax.annotation.Generated;

/**
 * ApiV2WarehousesGet200ResponseInner
 */

@JsonTypeName("_api_v2_warehouses_get_200_response_inner")
@Generated(value = "org.openapitools.codegen.languages.SpringCodegen", date = "2022-10-16T20:47:05.606+03:00[Europe/Moscow]")
public class ApiV2WarehousesGet200ResponseInner {

  @JsonProperty("id")
  private Integer id;

  @JsonProperty("name")
  private String name;

  public ApiV2WarehousesGet200ResponseInner id(Integer id) {
    this.id = id;
    return this;
  }

  /**
   * ID склада.
   * @return id
  */
  
  @Schema(name = "id", example = "5235", description = "ID склада.", required = false)
  public Integer getId() {
    return id;
  }

  public void setId(Integer id) {
    this.id = id;
  }

  public ApiV2WarehousesGet200ResponseInner name(String name) {
    this.name = name;
    return this;
  }

  /**
   * Наименование склада.
   * @return name
  */
  
  @Schema(name = "name", example = "string", description = "Наименование склада.", required = false)
  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    ApiV2WarehousesGet200ResponseInner apiV2WarehousesGet200ResponseInner = (ApiV2WarehousesGet200ResponseInner) o;
    return Objects.equals(this.id, apiV2WarehousesGet200ResponseInner.id) &&
        Objects.equals(this.name, apiV2WarehousesGet200ResponseInner.name);
  }

  @Override
  public int hashCode() {
    return Objects.hash(id, name);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class ApiV2WarehousesGet200ResponseInner {\n");
    sb.append("    id: ").append(toIndentedString(id)).append("\n");
    sb.append("    name: ").append(toIndentedString(name)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

