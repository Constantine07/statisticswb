package org.openapitools.model;

import java.net.URI;
import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonTypeName;
import java.util.ArrayList;
import java.util.List;
import org.openapitools.jackson.nullable.JsonNullable;
import java.time.OffsetDateTime;
import javax.validation.Valid;
import javax.validation.constraints.*;
import io.swagger.v3.oas.annotations.media.Schema;


import java.util.*;
import javax.annotation.Generated;

/**
 * ContentV1CardsUploadAddPostRequestCardsInnerSizesInner
 */

@JsonTypeName("_content_v1_cards_upload_add_post_request_cards_inner_sizes_inner")
@Generated(value = "org.openapitools.codegen.languages.SpringCodegen", date = "2022-10-16T20:47:05.606+03:00[Europe/Moscow]")
public class ContentV1CardsUploadAddPostRequestCardsInnerSizesInner {

  @JsonProperty("techSize")
  private String techSize;

  @JsonProperty("wbSize")
  private String wbSize;

  @JsonProperty("price")
  private Integer price;

  @JsonProperty("skus")
  @Valid
  private List<String> skus = null;

  public ContentV1CardsUploadAddPostRequestCardsInnerSizesInner techSize(String techSize) {
    this.techSize = techSize;
    return this;
  }

  /**
   * Размер поставщика (пример S, M, L, XL, 42, 42-43)
   * @return techSize
  */
  
  @Schema(name = "techSize", example = "40-41", description = "Размер поставщика (пример S, M, L, XL, 42, 42-43)", required = false)
  public String getTechSize() {
    return techSize;
  }

  public void setTechSize(String techSize) {
    this.techSize = techSize;
  }

  public ContentV1CardsUploadAddPostRequestCardsInnerSizesInner wbSize(String wbSize) {
    this.wbSize = wbSize;
    return this;
  }

  /**
   * Рос. размер
   * @return wbSize
  */
  
  @Schema(name = "wbSize", example = "40-41", description = "Рос. размер", required = false)
  public String getWbSize() {
    return wbSize;
  }

  public void setWbSize(String wbSize) {
    this.wbSize = wbSize;
  }

  public ContentV1CardsUploadAddPostRequestCardsInnerSizesInner price(Integer price) {
    this.price = price;
    return this;
  }

  /**
   * Цена
   * @return price
  */
  
  @Schema(name = "price", example = "3999", description = "Цена", required = false)
  public Integer getPrice() {
    return price;
  }

  public void setPrice(Integer price) {
    this.price = price;
  }

  public ContentV1CardsUploadAddPostRequestCardsInnerSizesInner skus(List<String> skus) {
    this.skus = skus;
    return this;
  }

  public ContentV1CardsUploadAddPostRequestCardsInnerSizesInner addSkusItem(String skusItem) {
    if (this.skus == null) {
      this.skus = new ArrayList<>();
    }
    this.skus.add(skusItem);
    return this;
  }

  /**
   * Массив баркодов, строковых идентификаторов размеров поставщика (их можно сгенерировать с помощью API, см. Viewer)
   * @return skus
  */
  
  @Schema(name = "skus", description = "Массив баркодов, строковых идентификаторов размеров поставщика (их можно сгенерировать с помощью API, см. Viewer)", required = false)
  public List<String> getSkus() {
    return skus;
  }

  public void setSkus(List<String> skus) {
    this.skus = skus;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    ContentV1CardsUploadAddPostRequestCardsInnerSizesInner contentV1CardsUploadAddPostRequestCardsInnerSizesInner = (ContentV1CardsUploadAddPostRequestCardsInnerSizesInner) o;
    return Objects.equals(this.techSize, contentV1CardsUploadAddPostRequestCardsInnerSizesInner.techSize) &&
        Objects.equals(this.wbSize, contentV1CardsUploadAddPostRequestCardsInnerSizesInner.wbSize) &&
        Objects.equals(this.price, contentV1CardsUploadAddPostRequestCardsInnerSizesInner.price) &&
        Objects.equals(this.skus, contentV1CardsUploadAddPostRequestCardsInnerSizesInner.skus);
  }

  @Override
  public int hashCode() {
    return Objects.hash(techSize, wbSize, price, skus);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class ContentV1CardsUploadAddPostRequestCardsInnerSizesInner {\n");
    sb.append("    techSize: ").append(toIndentedString(techSize)).append("\n");
    sb.append("    wbSize: ").append(toIndentedString(wbSize)).append("\n");
    sb.append("    price: ").append(toIndentedString(price)).append("\n");
    sb.append("    skus: ").append(toIndentedString(skus)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

