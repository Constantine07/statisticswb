package org.openapitools.model;

import java.net.URI;
import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.OffsetDateTime;
import org.springframework.format.annotation.DateTimeFormat;
import org.openapitools.jackson.nullable.JsonNullable;
import java.time.OffsetDateTime;
import javax.validation.Valid;
import javax.validation.constraints.*;
import io.swagger.v3.oas.annotations.media.Schema;


import java.util.*;
import javax.annotation.Generated;

/**
 * IncomesItem
 */

@Generated(value = "org.openapitools.codegen.languages.SpringCodegen", date = "2022-10-16T20:47:05.606+03:00[Europe/Moscow]")
public class IncomesItem {

  @JsonProperty("incomeId")
  private Integer incomeId;

  @JsonProperty("number")
  private String number;

  @JsonProperty("date")
  @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
  private LocalDate date;

  @JsonProperty("lastChangeDate")
  @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
  private OffsetDateTime lastChangeDate;

  @JsonProperty("supplierArticle")
  private String supplierArticle;

  @JsonProperty("techSize")
  private String techSize;

  @JsonProperty("barcode")
  private String barcode;

  @JsonProperty("quantity")
  private Integer quantity;

  @JsonProperty("totalPrice")
  private BigDecimal totalPrice;

  @JsonProperty("dateClose")
  @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
  private LocalDate dateClose;

  @JsonProperty("warehouseName")
  private String warehouseName;

  @JsonProperty("nmId")
  private Integer nmId;

  /**
   * Текущий статус поставки
   */
  public enum StatusEnum {
    _("Принято");

    private String value;

    StatusEnum(String value) {
      this.value = value;
    }

    @JsonValue
    public String getValue() {
      return value;
    }

    @Override
    public String toString() {
      return String.valueOf(value);
    }

    @JsonCreator
    public static StatusEnum fromValue(String value) {
      for (StatusEnum b : StatusEnum.values()) {
        if (b.value.equals(value)) {
          return b;
        }
      }
      throw new IllegalArgumentException("Unexpected value '" + value + "'");
    }
  }

  @JsonProperty("status")
  private StatusEnum status;

  public IncomesItem incomeId(Integer incomeId) {
    this.incomeId = incomeId;
    return this;
  }

  /**
   * Номер поставки
   * @return incomeId
  */
  
  @Schema(name = "incomeId", description = "Номер поставки", required = false)
  public Integer getIncomeId() {
    return incomeId;
  }

  public void setIncomeId(Integer incomeId) {
    this.incomeId = incomeId;
  }

  public IncomesItem number(String number) {
    this.number = number;
    return this;
  }

  /**
   * Номер УПД
   * @return number
  */
  @Size(max = 40) 
  @Schema(name = "number", description = "Номер УПД", required = false)
  public String getNumber() {
    return number;
  }

  public void setNumber(String number) {
    this.number = number;
  }

  public IncomesItem date(LocalDate date) {
    this.date = date;
    return this;
  }

  /**
   * Дата поступления. Если часовой пояс не указан, то берется Московское время UTC+3.
   * @return date
  */
  @Valid 
  @Schema(name = "date", description = "Дата поступления. Если часовой пояс не указан, то берется Московское время UTC+3.", required = false)
  public LocalDate getDate() {
    return date;
  }

  public void setDate(LocalDate date) {
    this.date = date;
  }

  public IncomesItem lastChangeDate(OffsetDateTime lastChangeDate) {
    this.lastChangeDate = lastChangeDate;
    return this;
  }

  /**
   * Дата и время обновления информации в сервисе. Это поле соответствует параметру `dateFrom` в запросе. Если часовой пояс не указан, то берется Московское время UTC+3.
   * @return lastChangeDate
  */
  @Valid 
  @Schema(name = "lastChangeDate", description = "Дата и время обновления информации в сервисе. Это поле соответствует параметру `dateFrom` в запросе. Если часовой пояс не указан, то берется Московское время UTC+3.", required = false)
  public OffsetDateTime getLastChangeDate() {
    return lastChangeDate;
  }

  public void setLastChangeDate(OffsetDateTime lastChangeDate) {
    this.lastChangeDate = lastChangeDate;
  }

  public IncomesItem supplierArticle(String supplierArticle) {
    this.supplierArticle = supplierArticle;
    return this;
  }

  /**
   * Артикул поставщика
   * @return supplierArticle
  */
  @Size(max = 75) 
  @Schema(name = "supplierArticle", description = "Артикул поставщика", required = false)
  public String getSupplierArticle() {
    return supplierArticle;
  }

  public void setSupplierArticle(String supplierArticle) {
    this.supplierArticle = supplierArticle;
  }

  public IncomesItem techSize(String techSize) {
    this.techSize = techSize;
    return this;
  }

  /**
   * Размер
   * @return techSize
  */
  @Size(max = 30) 
  @Schema(name = "techSize", description = "Размер", required = false)
  public String getTechSize() {
    return techSize;
  }

  public void setTechSize(String techSize) {
    this.techSize = techSize;
  }

  public IncomesItem barcode(String barcode) {
    this.barcode = barcode;
    return this;
  }

  /**
   * Бар-код
   * @return barcode
  */
  @Size(max = 30) 
  @Schema(name = "barcode", description = "Бар-код", required = false)
  public String getBarcode() {
    return barcode;
  }

  public void setBarcode(String barcode) {
    this.barcode = barcode;
  }

  public IncomesItem quantity(Integer quantity) {
    this.quantity = quantity;
    return this;
  }

  /**
   * Количество
   * @return quantity
  */
  
  @Schema(name = "quantity", description = "Количество", required = false)
  public Integer getQuantity() {
    return quantity;
  }

  public void setQuantity(Integer quantity) {
    this.quantity = quantity;
  }

  public IncomesItem totalPrice(BigDecimal totalPrice) {
    this.totalPrice = totalPrice;
    return this;
  }

  /**
   * Цена из УПД
   * @return totalPrice
  */
  @Valid 
  @Schema(name = "totalPrice", description = "Цена из УПД", required = false)
  public BigDecimal getTotalPrice() {
    return totalPrice;
  }

  public void setTotalPrice(BigDecimal totalPrice) {
    this.totalPrice = totalPrice;
  }

  public IncomesItem dateClose(LocalDate dateClose) {
    this.dateClose = dateClose;
    return this;
  }

  /**
   * Дата принятия (закрытия) в WB. Если часовой пояс не указан, то берется Московское время UTC+3.
   * @return dateClose
  */
  @Valid 
  @Schema(name = "dateClose", description = "Дата принятия (закрытия) в WB. Если часовой пояс не указан, то берется Московское время UTC+3.", required = false)
  public LocalDate getDateClose() {
    return dateClose;
  }

  public void setDateClose(LocalDate dateClose) {
    this.dateClose = dateClose;
  }

  public IncomesItem warehouseName(String warehouseName) {
    this.warehouseName = warehouseName;
    return this;
  }

  /**
   * Название склада
   * @return warehouseName
  */
  @Size(max = 50) 
  @Schema(name = "warehouseName", description = "Название склада", required = false)
  public String getWarehouseName() {
    return warehouseName;
  }

  public void setWarehouseName(String warehouseName) {
    this.warehouseName = warehouseName;
  }

  public IncomesItem nmId(Integer nmId) {
    this.nmId = nmId;
    return this;
  }

  /**
   * Код WB
   * @return nmId
  */
  
  @Schema(name = "nmId", description = "Код WB", required = false)
  public Integer getNmId() {
    return nmId;
  }

  public void setNmId(Integer nmId) {
    this.nmId = nmId;
  }

  public IncomesItem status(StatusEnum status) {
    this.status = status;
    return this;
  }

  /**
   * Текущий статус поставки
   * @return status
  */
  @Size(max = 50) 
  @Schema(name = "status", description = "Текущий статус поставки", required = false)
  public StatusEnum getStatus() {
    return status;
  }

  public void setStatus(StatusEnum status) {
    this.status = status;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    IncomesItem incomesItem = (IncomesItem) o;
    return Objects.equals(this.incomeId, incomesItem.incomeId) &&
        Objects.equals(this.number, incomesItem.number) &&
        Objects.equals(this.date, incomesItem.date) &&
        Objects.equals(this.lastChangeDate, incomesItem.lastChangeDate) &&
        Objects.equals(this.supplierArticle, incomesItem.supplierArticle) &&
        Objects.equals(this.techSize, incomesItem.techSize) &&
        Objects.equals(this.barcode, incomesItem.barcode) &&
        Objects.equals(this.quantity, incomesItem.quantity) &&
        Objects.equals(this.totalPrice, incomesItem.totalPrice) &&
        Objects.equals(this.dateClose, incomesItem.dateClose) &&
        Objects.equals(this.warehouseName, incomesItem.warehouseName) &&
        Objects.equals(this.nmId, incomesItem.nmId) &&
        Objects.equals(this.status, incomesItem.status);
  }

  @Override
  public int hashCode() {
    return Objects.hash(incomeId, number, date, lastChangeDate, supplierArticle, techSize, barcode, quantity, totalPrice, dateClose, warehouseName, nmId, status);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class IncomesItem {\n");
    sb.append("    incomeId: ").append(toIndentedString(incomeId)).append("\n");
    sb.append("    number: ").append(toIndentedString(number)).append("\n");
    sb.append("    date: ").append(toIndentedString(date)).append("\n");
    sb.append("    lastChangeDate: ").append(toIndentedString(lastChangeDate)).append("\n");
    sb.append("    supplierArticle: ").append(toIndentedString(supplierArticle)).append("\n");
    sb.append("    techSize: ").append(toIndentedString(techSize)).append("\n");
    sb.append("    barcode: ").append(toIndentedString(barcode)).append("\n");
    sb.append("    quantity: ").append(toIndentedString(quantity)).append("\n");
    sb.append("    totalPrice: ").append(toIndentedString(totalPrice)).append("\n");
    sb.append("    dateClose: ").append(toIndentedString(dateClose)).append("\n");
    sb.append("    warehouseName: ").append(toIndentedString(warehouseName)).append("\n");
    sb.append("    nmId: ").append(toIndentedString(nmId)).append("\n");
    sb.append("    status: ").append(toIndentedString(status)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

