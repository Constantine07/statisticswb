package org.openapitools.model;

import java.net.URI;
import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import java.math.BigDecimal;
import java.time.OffsetDateTime;
import org.springframework.format.annotation.DateTimeFormat;
import org.openapitools.jackson.nullable.JsonNullable;
import java.time.OffsetDateTime;
import javax.validation.Valid;
import javax.validation.constraints.*;
import io.swagger.v3.oas.annotations.media.Schema;


import java.util.*;
import javax.annotation.Generated;

/**
 * OrdersItem
 */

@Generated(value = "org.openapitools.codegen.languages.SpringCodegen", date = "2022-10-16T20:47:05.606+03:00[Europe/Moscow]")
public class OrdersItem {

  @JsonProperty("gNumber")
  private String gNumber;

  @JsonProperty("date")
  @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
  private OffsetDateTime date;

  @JsonProperty("lastChangeDate")
  @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
  private OffsetDateTime lastChangeDate;

  @JsonProperty("supplierArticle")
  private String supplierArticle;

  @JsonProperty("techSize")
  private String techSize;

  @JsonProperty("barcode")
  private String barcode;

  @JsonProperty("totalPrice")
  private BigDecimal totalPrice;

  @JsonProperty("discountPercent")
  private Integer discountPercent;

  @JsonProperty("warehouseName")
  private String warehouseName;

  @JsonProperty("oblast")
  private String oblast;

  @JsonProperty("incomeID")
  private Integer incomeID;

  @JsonProperty("odid")
  private Integer odid;

  @JsonProperty("nmId")
  private Integer nmId;

  @JsonProperty("subject")
  private String subject;

  @JsonProperty("category")
  private String category;

  @JsonProperty("brand")
  private String brand;

  @JsonProperty("isCancel")
  private Boolean isCancel;

  @JsonProperty("cancel_dt")
  @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
  private OffsetDateTime cancelDt;

  @JsonProperty("sticker")
  private String sticker;

  @JsonProperty("srid")
  private String srid;

  public OrdersItem gNumber(String gNumber) {
    this.gNumber = gNumber;
    return this;
  }

  /**
   * Номер заказа. Объединяет все позиции одного заказа.
   * @return gNumber
  */
  @Size(max = 50) 
  @Schema(name = "gNumber", description = "Номер заказа. Объединяет все позиции одного заказа.", required = false)
  public String getgNumber() {
    return gNumber;
  }

  public void setgNumber(String gNumber) {
    this.gNumber = gNumber;
  }

  public OrdersItem date(OffsetDateTime date) {
    this.date = date;
    return this;
  }

  /**
   * Дата и время заказа. Это поле соответствует параметру `dateFrom` в запросе, если параметр `flag=1`. Если часовой пояс не указан, то берется Московское время UTC+3.
   * @return date
  */
  @Valid 
  @Schema(name = "date", description = "Дата и время заказа. Это поле соответствует параметру `dateFrom` в запросе, если параметр `flag=1`. Если часовой пояс не указан, то берется Московское время UTC+3.", required = false)
  public OffsetDateTime getDate() {
    return date;
  }

  public void setDate(OffsetDateTime date) {
    this.date = date;
  }

  public OrdersItem lastChangeDate(OffsetDateTime lastChangeDate) {
    this.lastChangeDate = lastChangeDate;
    return this;
  }

  /**
   * Дата и время обновления информации в сервисе. Это поле соответствует параметру `dateFrom` в запросе, если параметр `flag=0` или не указан. Если часовой пояс не указан, то берется Московское время UTC+3.
   * @return lastChangeDate
  */
  @Valid 
  @Schema(name = "lastChangeDate", description = "Дата и время обновления информации в сервисе. Это поле соответствует параметру `dateFrom` в запросе, если параметр `flag=0` или не указан. Если часовой пояс не указан, то берется Московское время UTC+3.", required = false)
  public OffsetDateTime getLastChangeDate() {
    return lastChangeDate;
  }

  public void setLastChangeDate(OffsetDateTime lastChangeDate) {
    this.lastChangeDate = lastChangeDate;
  }

  public OrdersItem supplierArticle(String supplierArticle) {
    this.supplierArticle = supplierArticle;
    return this;
  }

  /**
   * Артикул поставщика
   * @return supplierArticle
  */
  @Size(max = 75) 
  @Schema(name = "supplierArticle", description = "Артикул поставщика", required = false)
  public String getSupplierArticle() {
    return supplierArticle;
  }

  public void setSupplierArticle(String supplierArticle) {
    this.supplierArticle = supplierArticle;
  }

  public OrdersItem techSize(String techSize) {
    this.techSize = techSize;
    return this;
  }

  /**
   * Размер
   * @return techSize
  */
  @Size(max = 30) 
  @Schema(name = "techSize", description = "Размер", required = false)
  public String getTechSize() {
    return techSize;
  }

  public void setTechSize(String techSize) {
    this.techSize = techSize;
  }

  public OrdersItem barcode(String barcode) {
    this.barcode = barcode;
    return this;
  }

  /**
   * Бар-код
   * @return barcode
  */
  @Size(max = 30) 
  @Schema(name = "barcode", description = "Бар-код", required = false)
  public String getBarcode() {
    return barcode;
  }

  public void setBarcode(String barcode) {
    this.barcode = barcode;
  }

  public OrdersItem totalPrice(BigDecimal totalPrice) {
    this.totalPrice = totalPrice;
    return this;
  }

  /**
   * Цена до согласованной итоговой скидки/промо/спп. Для получения цены со скидкой можно воспользоваться формулой `priceWithDiscount = totalPrice * (1 - discountPercent/100)`
   * @return totalPrice
  */
  @Valid 
  @Schema(name = "totalPrice", description = "Цена до согласованной итоговой скидки/промо/спп. Для получения цены со скидкой можно воспользоваться формулой `priceWithDiscount = totalPrice * (1 - discountPercent/100)`", required = false)
  public BigDecimal getTotalPrice() {
    return totalPrice;
  }

  public void setTotalPrice(BigDecimal totalPrice) {
    this.totalPrice = totalPrice;
  }

  public OrdersItem discountPercent(Integer discountPercent) {
    this.discountPercent = discountPercent;
    return this;
  }

  /**
   * Согласованный итоговый дисконт. Будучи примененным к `totalPrice`, даёт сумму к оплате.
   * @return discountPercent
  */
  
  @Schema(name = "discountPercent", description = "Согласованный итоговый дисконт. Будучи примененным к `totalPrice`, даёт сумму к оплате.", required = false)
  public Integer getDiscountPercent() {
    return discountPercent;
  }

  public void setDiscountPercent(Integer discountPercent) {
    this.discountPercent = discountPercent;
  }

  public OrdersItem warehouseName(String warehouseName) {
    this.warehouseName = warehouseName;
    return this;
  }

  /**
   * Название склада отгрузки
   * @return warehouseName
  */
  @Size(max = 50) 
  @Schema(name = "warehouseName", description = "Название склада отгрузки", required = false)
  public String getWarehouseName() {
    return warehouseName;
  }

  public void setWarehouseName(String warehouseName) {
    this.warehouseName = warehouseName;
  }

  public OrdersItem oblast(String oblast) {
    this.oblast = oblast;
    return this;
  }

  /**
   * Область
   * @return oblast
  */
  @Size(max = 200) 
  @Schema(name = "oblast", description = "Область", required = false)
  public String getOblast() {
    return oblast;
  }

  public void setOblast(String oblast) {
    this.oblast = oblast;
  }

  public OrdersItem incomeID(Integer incomeID) {
    this.incomeID = incomeID;
    return this;
  }

  /**
   * Номер поставки (от продавца на склад)
   * @return incomeID
  */
  
  @Schema(name = "incomeID", description = "Номер поставки (от продавца на склад)", required = false)
  public Integer getIncomeID() {
    return incomeID;
  }

  public void setIncomeID(Integer incomeID) {
    this.incomeID = incomeID;
  }

  public OrdersItem odid(Integer odid) {
    this.odid = odid;
    return this;
  }

  /**
   * Уникальный идентификатор позиции заказа. Может использоваться для поиска соответствия между заказами и продажами.
   * @return odid
  */
  
  @Schema(name = "odid", description = "Уникальный идентификатор позиции заказа. Может использоваться для поиска соответствия между заказами и продажами.", required = false)
  public Integer getOdid() {
    return odid;
  }

  public void setOdid(Integer odid) {
    this.odid = odid;
  }

  public OrdersItem nmId(Integer nmId) {
    this.nmId = nmId;
    return this;
  }

  /**
   * Код WB
   * @return nmId
  */
  
  @Schema(name = "nmId", description = "Код WB", required = false)
  public Integer getNmId() {
    return nmId;
  }

  public void setNmId(Integer nmId) {
    this.nmId = nmId;
  }

  public OrdersItem subject(String subject) {
    this.subject = subject;
    return this;
  }

  /**
   * Предмет
   * @return subject
  */
  @Size(max = 50) 
  @Schema(name = "subject", description = "Предмет", required = false)
  public String getSubject() {
    return subject;
  }

  public void setSubject(String subject) {
    this.subject = subject;
  }

  public OrdersItem category(String category) {
    this.category = category;
    return this;
  }

  /**
   * Категория
   * @return category
  */
  @Size(max = 50) 
  @Schema(name = "category", description = "Категория", required = false)
  public String getCategory() {
    return category;
  }

  public void setCategory(String category) {
    this.category = category;
  }

  public OrdersItem brand(String brand) {
    this.brand = brand;
    return this;
  }

  /**
   * Бренд
   * @return brand
  */
  @Size(max = 50) 
  @Schema(name = "brand", description = "Бренд", required = false)
  public String getBrand() {
    return brand;
  }

  public void setBrand(String brand) {
    this.brand = brand;
  }

  public OrdersItem isCancel(Boolean isCancel) {
    this.isCancel = isCancel;
    return this;
  }

  /**
   * Отмена заказа. true - заказ отменен до оплаты.
   * @return isCancel
  */
  
  @Schema(name = "isCancel", description = "Отмена заказа. true - заказ отменен до оплаты.", required = false)
  public Boolean getIsCancel() {
    return isCancel;
  }

  public void setIsCancel(Boolean isCancel) {
    this.isCancel = isCancel;
  }

  public OrdersItem cancelDt(OffsetDateTime cancelDt) {
    this.cancelDt = cancelDt;
    return this;
  }

  /**
   * Дата и время отмены заказа. Если заказ не был отменен, то `\"0001-01-01T00:00:00\"`. Если часовой пояс не указан, то берется Московское время UTC+3.
   * @return cancelDt
  */
  @Valid 
  @Schema(name = "cancel_dt", description = "Дата и время отмены заказа. Если заказ не был отменен, то `\"0001-01-01T00:00:00\"`. Если часовой пояс не указан, то берется Московское время UTC+3.", required = false)
  public OffsetDateTime getCancelDt() {
    return cancelDt;
  }

  public void setCancelDt(OffsetDateTime cancelDt) {
    this.cancelDt = cancelDt;
  }

  public OrdersItem sticker(String sticker) {
    this.sticker = sticker;
    return this;
  }

  /**
   * Цифровое значение стикера, который клеится на товар в процессе сборки заказа по системе Маркетплейс.
   * @return sticker
  */
  
  @Schema(name = "sticker", description = "Цифровое значение стикера, который клеится на товар в процессе сборки заказа по системе Маркетплейс.", required = false)
  public String getSticker() {
    return sticker;
  }

  public void setSticker(String sticker) {
    this.sticker = sticker;
  }

  public OrdersItem srid(String srid) {
    this.srid = srid;
    return this;
  }

  /**
   * Уникальный идентификатор заказа, функционально аналогичный `odid`/`rid`.  Данный параметр введен в июле'22 и в течение переходного периода может быть заполнен не во всех ответах. Примечание для работающих по системе Маркетплейс: `srid` равен `rid` в ответе на метод `GET /api/v2/orders`. 
   * @return srid
  */
  
  @Schema(name = "srid", description = "Уникальный идентификатор заказа, функционально аналогичный `odid`/`rid`.  Данный параметр введен в июле'22 и в течение переходного периода может быть заполнен не во всех ответах. Примечание для работающих по системе Маркетплейс: `srid` равен `rid` в ответе на метод `GET /api/v2/orders`. ", required = false)
  public String getSrid() {
    return srid;
  }

  public void setSrid(String srid) {
    this.srid = srid;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    OrdersItem ordersItem = (OrdersItem) o;
    return Objects.equals(this.gNumber, ordersItem.gNumber) &&
        Objects.equals(this.date, ordersItem.date) &&
        Objects.equals(this.lastChangeDate, ordersItem.lastChangeDate) &&
        Objects.equals(this.supplierArticle, ordersItem.supplierArticle) &&
        Objects.equals(this.techSize, ordersItem.techSize) &&
        Objects.equals(this.barcode, ordersItem.barcode) &&
        Objects.equals(this.totalPrice, ordersItem.totalPrice) &&
        Objects.equals(this.discountPercent, ordersItem.discountPercent) &&
        Objects.equals(this.warehouseName, ordersItem.warehouseName) &&
        Objects.equals(this.oblast, ordersItem.oblast) &&
        Objects.equals(this.incomeID, ordersItem.incomeID) &&
        Objects.equals(this.odid, ordersItem.odid) &&
        Objects.equals(this.nmId, ordersItem.nmId) &&
        Objects.equals(this.subject, ordersItem.subject) &&
        Objects.equals(this.category, ordersItem.category) &&
        Objects.equals(this.brand, ordersItem.brand) &&
        Objects.equals(this.isCancel, ordersItem.isCancel) &&
        Objects.equals(this.cancelDt, ordersItem.cancelDt) &&
        Objects.equals(this.sticker, ordersItem.sticker) &&
        Objects.equals(this.srid, ordersItem.srid);
  }

  @Override
  public int hashCode() {
    return Objects.hash(gNumber, date, lastChangeDate, supplierArticle, techSize, barcode, totalPrice, discountPercent, warehouseName, oblast, incomeID, odid, nmId, subject, category, brand, isCancel, cancelDt, sticker, srid);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class OrdersItem {\n");
    sb.append("    gNumber: ").append(toIndentedString(gNumber)).append("\n");
    sb.append("    date: ").append(toIndentedString(date)).append("\n");
    sb.append("    lastChangeDate: ").append(toIndentedString(lastChangeDate)).append("\n");
    sb.append("    supplierArticle: ").append(toIndentedString(supplierArticle)).append("\n");
    sb.append("    techSize: ").append(toIndentedString(techSize)).append("\n");
    sb.append("    barcode: ").append(toIndentedString(barcode)).append("\n");
    sb.append("    totalPrice: ").append(toIndentedString(totalPrice)).append("\n");
    sb.append("    discountPercent: ").append(toIndentedString(discountPercent)).append("\n");
    sb.append("    warehouseName: ").append(toIndentedString(warehouseName)).append("\n");
    sb.append("    oblast: ").append(toIndentedString(oblast)).append("\n");
    sb.append("    incomeID: ").append(toIndentedString(incomeID)).append("\n");
    sb.append("    odid: ").append(toIndentedString(odid)).append("\n");
    sb.append("    nmId: ").append(toIndentedString(nmId)).append("\n");
    sb.append("    subject: ").append(toIndentedString(subject)).append("\n");
    sb.append("    category: ").append(toIndentedString(category)).append("\n");
    sb.append("    brand: ").append(toIndentedString(brand)).append("\n");
    sb.append("    isCancel: ").append(toIndentedString(isCancel)).append("\n");
    sb.append("    cancelDt: ").append(toIndentedString(cancelDt)).append("\n");
    sb.append("    sticker: ").append(toIndentedString(sticker)).append("\n");
    sb.append("    srid: ").append(toIndentedString(srid)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

