package org.openapitools.model;

import java.net.URI;
import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonTypeName;
import org.openapitools.model.ApiV2OrdersStickersPdfPost200ResponseData;
import org.openapitools.jackson.nullable.JsonNullable;
import java.time.OffsetDateTime;
import javax.validation.Valid;
import javax.validation.constraints.*;
import io.swagger.v3.oas.annotations.media.Schema;


import java.util.*;
import javax.annotation.Generated;

/**
 * ApiV2OrdersStickersPdfPost200Response
 */

@JsonTypeName("_api_v2_orders_stickers_pdf_post_200_response")
@Generated(value = "org.openapitools.codegen.languages.SpringCodegen", date = "2022-10-16T20:47:05.606+03:00[Europe/Moscow]")
public class ApiV2OrdersStickersPdfPost200Response {

  @JsonProperty("error")
  private Boolean error;

  @JsonProperty("errorText")
  private String errorText;

  @JsonProperty("data")
  private ApiV2OrdersStickersPdfPost200ResponseData data;

  public ApiV2OrdersStickersPdfPost200Response error(Boolean error) {
    this.error = error;
    return this;
  }

  /**
   * Get error
   * @return error
  */
  
  @Schema(name = "error", example = "true", required = false)
  public Boolean getError() {
    return error;
  }

  public void setError(Boolean error) {
    this.error = error;
  }

  public ApiV2OrdersStickersPdfPost200Response errorText(String errorText) {
    this.errorText = errorText;
    return this;
  }

  /**
   * Get errorText
   * @return errorText
  */
  
  @Schema(name = "errorText", example = "string", required = false)
  public String getErrorText() {
    return errorText;
  }

  public void setErrorText(String errorText) {
    this.errorText = errorText;
  }

  public ApiV2OrdersStickersPdfPost200Response data(ApiV2OrdersStickersPdfPost200ResponseData data) {
    this.data = data;
    return this;
  }

  /**
   * Get data
   * @return data
  */
  @Valid 
  @Schema(name = "data", required = false)
  public ApiV2OrdersStickersPdfPost200ResponseData getData() {
    return data;
  }

  public void setData(ApiV2OrdersStickersPdfPost200ResponseData data) {
    this.data = data;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    ApiV2OrdersStickersPdfPost200Response apiV2OrdersStickersPdfPost200Response = (ApiV2OrdersStickersPdfPost200Response) o;
    return Objects.equals(this.error, apiV2OrdersStickersPdfPost200Response.error) &&
        Objects.equals(this.errorText, apiV2OrdersStickersPdfPost200Response.errorText) &&
        Objects.equals(this.data, apiV2OrdersStickersPdfPost200Response.data);
  }

  @Override
  public int hashCode() {
    return Objects.hash(error, errorText, data);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class ApiV2OrdersStickersPdfPost200Response {\n");
    sb.append("    error: ").append(toIndentedString(error)).append("\n");
    sb.append("    errorText: ").append(toIndentedString(errorText)).append("\n");
    sb.append("    data: ").append(toIndentedString(data)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

