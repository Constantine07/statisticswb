package org.openapitools.model;

import java.net.URI;
import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonTypeName;
import java.util.ArrayList;
import java.util.List;
import org.openapitools.jackson.nullable.JsonNullable;
import java.time.OffsetDateTime;
import javax.validation.Valid;
import javax.validation.constraints.*;
import io.swagger.v3.oas.annotations.media.Schema;


import java.util.*;
import javax.annotation.Generated;

/**
 * ApiV2StocksDelete200ResponseStocksInner
 */

@JsonTypeName("_api_v2_stocks_delete_200_response_stocks_inner")
@Generated(value = "org.openapitools.codegen.languages.SpringCodegen", date = "2022-10-16T20:47:05.606+03:00[Europe/Moscow]")
public class ApiV2StocksDelete200ResponseStocksInner {

  @JsonProperty("subject")
  private String subject;

  @JsonProperty("brand")
  private String brand;

  @JsonProperty("name")
  private String name;

  @JsonProperty("size")
  private String size;

  @JsonProperty("barcode")
  private String barcode;

  @JsonProperty("barcodes")
  @Valid
  private List<String> barcodes = null;

  @JsonProperty("article")
  private String article;

  @JsonProperty("stock")
  private Integer stock;

  @JsonProperty("warehouseName")
  private String warehouseName;

  @JsonProperty("warehouseId")
  private Integer warehouseId;

  @JsonProperty("id")
  private Integer id;

  @JsonProperty("chrtId")
  private Integer chrtId;

  @JsonProperty("nmId")
  private Integer nmId;

  @JsonProperty("isCargoDelivery")
  private Boolean isCargoDelivery;

  public ApiV2StocksDelete200ResponseStocksInner subject(String subject) {
    this.subject = subject;
    return this;
  }

  /**
   * Категория.
   * @return subject
  */
  
  @Schema(name = "subject", example = "Носки", description = "Категория.", required = false)
  public String getSubject() {
    return subject;
  }

  public void setSubject(String subject) {
    this.subject = subject;
  }

  public ApiV2StocksDelete200ResponseStocksInner brand(String brand) {
    this.brand = brand;
    return this;
  }

  /**
   * Бренд.
   * @return brand
  */
  
  @Schema(name = "brand", example = "Lincoln&Sharks", description = "Бренд.", required = false)
  public String getBrand() {
    return brand;
  }

  public void setBrand(String brand) {
    this.brand = brand;
  }

  public ApiV2StocksDelete200ResponseStocksInner name(String name) {
    this.name = name;
    return this;
  }

  /**
   * Наименование.
   * @return name
  */
  
  @Schema(name = "name", example = "носки", description = "Наименование.", required = false)
  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public ApiV2StocksDelete200ResponseStocksInner size(String size) {
    this.size = size;
    return this;
  }

  /**
   * Размер.
   * @return size
  */
  
  @Schema(name = "size", example = "10,5", description = "Размер.", required = false)
  public String getSize() {
    return size;
  }

  public void setSize(String size) {
    this.size = size;
  }

  public ApiV2StocksDelete200ResponseStocksInner barcode(String barcode) {
    this.barcode = barcode;
    return this;
  }

  /**
   * Баркод.
   * @return barcode
  */
  
  @Schema(name = "barcode", example = "2039893700627", description = "Баркод.", required = false)
  public String getBarcode() {
    return barcode;
  }

  public void setBarcode(String barcode) {
    this.barcode = barcode;
  }

  public ApiV2StocksDelete200ResponseStocksInner barcodes(List<String> barcodes) {
    this.barcodes = barcodes;
    return this;
  }

  public ApiV2StocksDelete200ResponseStocksInner addBarcodesItem(String barcodesItem) {
    if (this.barcodes == null) {
      this.barcodes = new ArrayList<>();
    }
    this.barcodes.add(barcodesItem);
    return this;
  }

  /**
   * Массив баркодов.
   * @return barcodes
  */
  
  @Schema(name = "barcodes", description = "Массив баркодов.", required = false)
  public List<String> getBarcodes() {
    return barcodes;
  }

  public void setBarcodes(List<String> barcodes) {
    this.barcodes = barcodes;
  }

  public ApiV2StocksDelete200ResponseStocksInner article(String article) {
    this.article = article;
    return this;
  }

  /**
   * Артикул поставщика.
   * @return article
  */
  
  @Schema(name = "article", example = "22224434", description = "Артикул поставщика.", required = false)
  public String getArticle() {
    return article;
  }

  public void setArticle(String article) {
    this.article = article;
  }

  public ApiV2StocksDelete200ResponseStocksInner stock(Integer stock) {
    this.stock = stock;
    return this;
  }

  /**
   * Остаток.
   * @return stock
  */
  
  @Schema(name = "stock", example = "1", description = "Остаток.", required = false)
  public Integer getStock() {
    return stock;
  }

  public void setStock(Integer stock) {
    this.stock = stock;
  }

  public ApiV2StocksDelete200ResponseStocksInner warehouseName(String warehouseName) {
    this.warehouseName = warehouseName;
    return this;
  }

  /**
   * Склад с товаром.
   * @return warehouseName
  */
  
  @Schema(name = "warehouseName", example = "string", description = "Склад с товаром.", required = false)
  public String getWarehouseName() {
    return warehouseName;
  }

  public void setWarehouseName(String warehouseName) {
    this.warehouseName = warehouseName;
  }

  public ApiV2StocksDelete200ResponseStocksInner warehouseId(Integer warehouseId) {
    this.warehouseId = warehouseId;
    return this;
  }

  /**
   * ID склада.
   * @return warehouseId
  */
  
  @Schema(name = "warehouseId", example = "205384680", description = "ID склада.", required = false)
  public Integer getWarehouseId() {
    return warehouseId;
  }

  public void setWarehouseId(Integer warehouseId) {
    this.warehouseId = warehouseId;
  }

  public ApiV2StocksDelete200ResponseStocksInner id(Integer id) {
    this.id = id;
    return this;
  }

  /**
   * Get id
   * @return id
  */
  
  @Schema(name = "id", example = "205384680", required = false)
  public Integer getId() {
    return id;
  }

  public void setId(Integer id) {
    this.id = id;
  }

  public ApiV2StocksDelete200ResponseStocksInner chrtId(Integer chrtId) {
    this.chrtId = chrtId;
    return this;
  }

  /**
   * Get chrtId
   * @return chrtId
  */
  
  @Schema(name = "chrtId", example = "205384680", required = false)
  public Integer getChrtId() {
    return chrtId;
  }

  public void setChrtId(Integer chrtId) {
    this.chrtId = chrtId;
  }

  public ApiV2StocksDelete200ResponseStocksInner nmId(Integer nmId) {
    this.nmId = nmId;
    return this;
  }

  /**
   * Get nmId
   * @return nmId
  */
  
  @Schema(name = "nmId", example = "114296358", required = false)
  public Integer getNmId() {
    return nmId;
  }

  public void setNmId(Integer nmId) {
    this.nmId = nmId;
  }

  public ApiV2StocksDelete200ResponseStocksInner isCargoDelivery(Boolean isCargoDelivery) {
    this.isCargoDelivery = isCargoDelivery;
    return this;
  }

  /**
   * Get isCargoDelivery
   * @return isCargoDelivery
  */
  
  @Schema(name = "isCargoDelivery", example = "false", required = false)
  public Boolean getIsCargoDelivery() {
    return isCargoDelivery;
  }

  public void setIsCargoDelivery(Boolean isCargoDelivery) {
    this.isCargoDelivery = isCargoDelivery;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    ApiV2StocksDelete200ResponseStocksInner apiV2StocksDelete200ResponseStocksInner = (ApiV2StocksDelete200ResponseStocksInner) o;
    return Objects.equals(this.subject, apiV2StocksDelete200ResponseStocksInner.subject) &&
        Objects.equals(this.brand, apiV2StocksDelete200ResponseStocksInner.brand) &&
        Objects.equals(this.name, apiV2StocksDelete200ResponseStocksInner.name) &&
        Objects.equals(this.size, apiV2StocksDelete200ResponseStocksInner.size) &&
        Objects.equals(this.barcode, apiV2StocksDelete200ResponseStocksInner.barcode) &&
        Objects.equals(this.barcodes, apiV2StocksDelete200ResponseStocksInner.barcodes) &&
        Objects.equals(this.article, apiV2StocksDelete200ResponseStocksInner.article) &&
        Objects.equals(this.stock, apiV2StocksDelete200ResponseStocksInner.stock) &&
        Objects.equals(this.warehouseName, apiV2StocksDelete200ResponseStocksInner.warehouseName) &&
        Objects.equals(this.warehouseId, apiV2StocksDelete200ResponseStocksInner.warehouseId) &&
        Objects.equals(this.id, apiV2StocksDelete200ResponseStocksInner.id) &&
        Objects.equals(this.chrtId, apiV2StocksDelete200ResponseStocksInner.chrtId) &&
        Objects.equals(this.nmId, apiV2StocksDelete200ResponseStocksInner.nmId) &&
        Objects.equals(this.isCargoDelivery, apiV2StocksDelete200ResponseStocksInner.isCargoDelivery);
  }

  @Override
  public int hashCode() {
    return Objects.hash(subject, brand, name, size, barcode, barcodes, article, stock, warehouseName, warehouseId, id, chrtId, nmId, isCargoDelivery);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class ApiV2StocksDelete200ResponseStocksInner {\n");
    sb.append("    subject: ").append(toIndentedString(subject)).append("\n");
    sb.append("    brand: ").append(toIndentedString(brand)).append("\n");
    sb.append("    name: ").append(toIndentedString(name)).append("\n");
    sb.append("    size: ").append(toIndentedString(size)).append("\n");
    sb.append("    barcode: ").append(toIndentedString(barcode)).append("\n");
    sb.append("    barcodes: ").append(toIndentedString(barcodes)).append("\n");
    sb.append("    article: ").append(toIndentedString(article)).append("\n");
    sb.append("    stock: ").append(toIndentedString(stock)).append("\n");
    sb.append("    warehouseName: ").append(toIndentedString(warehouseName)).append("\n");
    sb.append("    warehouseId: ").append(toIndentedString(warehouseId)).append("\n");
    sb.append("    id: ").append(toIndentedString(id)).append("\n");
    sb.append("    chrtId: ").append(toIndentedString(chrtId)).append("\n");
    sb.append("    nmId: ").append(toIndentedString(nmId)).append("\n");
    sb.append("    isCargoDelivery: ").append(toIndentedString(isCargoDelivery)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

