package org.openapitools.model;

import java.net.URI;
import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonTypeName;
import org.openapitools.jackson.nullable.JsonNullable;
import java.time.OffsetDateTime;
import javax.validation.Valid;
import javax.validation.constraints.*;
import io.swagger.v3.oas.annotations.media.Schema;


import java.util.*;
import javax.annotation.Generated;

/**
 * ApiV2OrdersStickersPost200ResponseDataInnerStickerWbStickerIdParts
 */

@JsonTypeName("_api_v2_orders_stickers_post_200_response_data_inner_sticker_wbStickerIdParts")
@Generated(value = "org.openapitools.codegen.languages.SpringCodegen", date = "2022-10-16T20:47:05.606+03:00[Europe/Moscow]")
public class ApiV2OrdersStickersPost200ResponseDataInnerStickerWbStickerIdParts {

  @JsonProperty("A")
  private String A;

  @JsonProperty("B")
  private String B;

  public ApiV2OrdersStickersPost200ResponseDataInnerStickerWbStickerIdParts A(String A) {
    this.A = A;
    return this;
  }

  /**
   * Первая часть идентификатора этикетки (для печати подписи).
   * @return A
  */
  
  @Schema(name = "A", example = "231648", description = "Первая часть идентификатора этикетки (для печати подписи).", required = false)
  public String getA() {
    return A;
  }

  public void setA(String A) {
    this.A = A;
  }

  public ApiV2OrdersStickersPost200ResponseDataInnerStickerWbStickerIdParts B(String B) {
    this.B = B;
    return this;
  }

  /**
   * Вторая часть идентификатора этикетки.
   * @return B
  */
  
  @Schema(name = "B", example = "9753", description = "Вторая часть идентификатора этикетки.", required = false)
  public String getB() {
    return B;
  }

  public void setB(String B) {
    this.B = B;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    ApiV2OrdersStickersPost200ResponseDataInnerStickerWbStickerIdParts apiV2OrdersStickersPost200ResponseDataInnerStickerWbStickerIdParts = (ApiV2OrdersStickersPost200ResponseDataInnerStickerWbStickerIdParts) o;
    return Objects.equals(this.A, apiV2OrdersStickersPost200ResponseDataInnerStickerWbStickerIdParts.A) &&
        Objects.equals(this.B, apiV2OrdersStickersPost200ResponseDataInnerStickerWbStickerIdParts.B);
  }

  @Override
  public int hashCode() {
    return Objects.hash(A, B);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class ApiV2OrdersStickersPost200ResponseDataInnerStickerWbStickerIdParts {\n");
    sb.append("    A: ").append(toIndentedString(A)).append("\n");
    sb.append("    B: ").append(toIndentedString(B)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

