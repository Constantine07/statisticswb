package org.openapitools.model;

import java.net.URI;
import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonTypeName;
import java.util.ArrayList;
import java.util.List;
import org.openapitools.model.ApiV2SuppliesIdOrdersGet200ResponseOrdersInner;
import org.openapitools.jackson.nullable.JsonNullable;
import java.time.OffsetDateTime;
import javax.validation.Valid;
import javax.validation.constraints.*;
import io.swagger.v3.oas.annotations.media.Schema;


import java.util.*;
import javax.annotation.Generated;

/**
 * ApiV2SuppliesIdOrdersGet200Response
 */

@JsonTypeName("_api_v2_supplies__id__orders_get_200_response")
@Generated(value = "org.openapitools.codegen.languages.SpringCodegen", date = "2022-10-16T20:47:05.606+03:00[Europe/Moscow]")
public class ApiV2SuppliesIdOrdersGet200Response {

  @JsonProperty("orders")
  @Valid
  private List<ApiV2SuppliesIdOrdersGet200ResponseOrdersInner> orders = null;

  public ApiV2SuppliesIdOrdersGet200Response orders(List<ApiV2SuppliesIdOrdersGet200ResponseOrdersInner> orders) {
    this.orders = orders;
    return this;
  }

  public ApiV2SuppliesIdOrdersGet200Response addOrdersItem(ApiV2SuppliesIdOrdersGet200ResponseOrdersInner ordersItem) {
    if (this.orders == null) {
      this.orders = new ArrayList<>();
    }
    this.orders.add(ordersItem);
    return this;
  }

  /**
   * Get orders
   * @return orders
  */
  @Valid 
  @Schema(name = "orders", required = false)
  public List<ApiV2SuppliesIdOrdersGet200ResponseOrdersInner> getOrders() {
    return orders;
  }

  public void setOrders(List<ApiV2SuppliesIdOrdersGet200ResponseOrdersInner> orders) {
    this.orders = orders;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    ApiV2SuppliesIdOrdersGet200Response apiV2SuppliesIdOrdersGet200Response = (ApiV2SuppliesIdOrdersGet200Response) o;
    return Objects.equals(this.orders, apiV2SuppliesIdOrdersGet200Response.orders);
  }

  @Override
  public int hashCode() {
    return Objects.hash(orders);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class ApiV2SuppliesIdOrdersGet200Response {\n");
    sb.append("    orders: ").append(toIndentedString(orders)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

