package org.openapitools.model;

import java.net.URI;
import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonTypeName;
import java.math.BigDecimal;
import org.openapitools.jackson.nullable.JsonNullable;
import java.time.OffsetDateTime;
import javax.validation.Valid;
import javax.validation.constraints.*;
import io.swagger.v3.oas.annotations.media.Schema;


import java.util.*;
import javax.annotation.Generated;

/**
 * PublicApiV1PricesPostRequestInner
 */

@JsonTypeName("_public_api_v1_prices_post_request_inner")
@Generated(value = "org.openapitools.codegen.languages.SpringCodegen", date = "2022-10-16T20:47:05.606+03:00[Europe/Moscow]")
public class PublicApiV1PricesPostRequestInner {

  @JsonProperty("nmId")
  private Integer nmId;

  @JsonProperty("price")
  private BigDecimal price;

  public PublicApiV1PricesPostRequestInner nmId(Integer nmId) {
    this.nmId = nmId;
    return this;
  }

  /**
   * Номенклатура
   * @return nmId
  */
  
  @Schema(name = "nmId", example = "1234567", description = "Номенклатура", required = false)
  public Integer getNmId() {
    return nmId;
  }

  public void setNmId(Integer nmId) {
    this.nmId = nmId;
  }

  public PublicApiV1PricesPostRequestInner price(BigDecimal price) {
    this.price = price;
    return this;
  }

  /**
   * Цена (указывать без копеек)
   * @return price
  */
  @Valid 
  @Schema(name = "price", example = "1000", description = "Цена (указывать без копеек)", required = false)
  public BigDecimal getPrice() {
    return price;
  }

  public void setPrice(BigDecimal price) {
    this.price = price;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    PublicApiV1PricesPostRequestInner publicApiV1PricesPostRequestInner = (PublicApiV1PricesPostRequestInner) o;
    return Objects.equals(this.nmId, publicApiV1PricesPostRequestInner.nmId) &&
        Objects.equals(this.price, publicApiV1PricesPostRequestInner.price);
  }

  @Override
  public int hashCode() {
    return Objects.hash(nmId, price);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class PublicApiV1PricesPostRequestInner {\n");
    sb.append("    nmId: ").append(toIndentedString(nmId)).append("\n");
    sb.append("    price: ").append(toIndentedString(price)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

