package org.openapitools.model;

import java.net.URI;
import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonTypeName;
import org.openapitools.jackson.nullable.JsonNullable;
import java.time.OffsetDateTime;
import javax.validation.Valid;
import javax.validation.constraints.*;
import io.swagger.v3.oas.annotations.media.Schema;


import java.util.*;
import javax.annotation.Generated;

/**
 * ApiV2OrdersStickersPdfPost200ResponseData
 */

@JsonTypeName("_api_v2_orders_stickers_pdf_post_200_response_data")
@Generated(value = "org.openapitools.codegen.languages.SpringCodegen", date = "2022-10-16T20:47:05.606+03:00[Europe/Moscow]")
public class ApiV2OrdersStickersPdfPost200ResponseData {

  @JsonProperty("file")
  private String file;

  @JsonProperty("name")
  private String name;

  @JsonProperty("mimeType")
  private String mimeType;

  public ApiV2OrdersStickersPdfPost200ResponseData file(String file) {
    this.file = file;
    return this;
  }

  /**
   * Pdf файл в base64.
   * @return file
  */
  
  @Schema(name = "file", example = "JVBERi0xLjMKMyAwIG9iago8PC9UeXBlIC9QYWdlCi9QYXJlbnQgMSAwIFIKL01lZGlhQm94IFswIDAgMTE0LjAwIDg1LjgwXQovUmVzb3VyY2VzIDIgMCBSCi9Db250ZW50cyA0IDAgUj4+CmVuZG9iago0IDAgb2JqCjw8L0ZpbHRlciAvRmxhdGVEZWNvZGUgL0xlbmd0aCAzNTk+PgpzdHJlYW0KeAGUlM+K4zAMxu9+Ch23h3rl//K10F3YW8EvsGmSQm8zl3n9IXIhKiKGOSX4k/X7PtsI4Z9BeBq0qcCXQYuI8Pf1fRgXbS5A3sYCzoYA55S3lc8FVuP4t4u4rb6JtK0ciN5bxJeo2voR0wum3lk3i53pN7w0FHili8ptEEwtCqYWRU5lKHL5ATPG/RC4TrqNaY+i29JgZ+LyA2YSTK6TzJT3tnxBb6LIqdxm7nXAzCNmFkzdtuyHoMSyvdUXk/HSbQmjnaP7LCKnakvsoufk5ySZNHpDNGLS6D6ryKmYVZwte5OG6ugNVZFTXbZDjt6Dqr4O2UdXuVBSz1zv+8joI+IcOAMPjUuD339mpFDSmuMyVcrunpf/S5zX+zpPc0iI6zRPSwgLuGQRoa1wbeYDHE+nB1waZBs8VBsKtBl+hZIz4gnaE64NbuZnEH8IQUsFvC2eKYmITtCecG1wM98BAAD//418GGcKZW5kc3RyZWFtCmVuZG9iago1IDAgb2JqCjw8L1R5cGUgL1BhZ2UKL1BhcmVudCAxIDAgUgovTWVkaWFCb3ggWzAgMCAxMTQuMDAgODUuODBdCi9SZXNvdXJjZXMgMiAwIFIKL0NvbnRlbnRzIDYgMCBSPj4KZW5kb2JqCjYgMCBvYmoKPDwvRmlsdGVyIC9GbGF0ZURlY29kZSAvTGVuZ3RoIDM3ND4+CnN0cmVhbQp4AaSUwYrjMAyG73kKHbeHemXLtpxrobuwt4JfYNMkA73NXOb1h8gDVRAxlLkF/f7/T7aIEP4NCI8BXWL4HC4Vfv+ZsRCnNcdlGkv297z8X+K83td5mikhrtM8LUQLhOQQoa5wrQM6RIS/AzpEhLcfJPnoMkMJLjJ4RwTnlLfKxwLr4OWzibhVd2LZKgdiCA7xWzSxoccMimmd49biAZPkeBPD1pvullLPqZjmnlSeVzFilEpjCl4zY+w5ew1FxTSxSSqNKXjNTOptrajuKSE7p5qnEbNkNaaMVjszPd/WirnzCJm1M/MulqWLxhS8ZnKPyYopITunelsTW6TSmMZZevMsap7mEYpimthRMeVTdztKVmvIioppY9U8JUTHepTzB7ke5f9pqhzU3rP0EdrKaCviTNtGakvjta3m1VZ7B9/2GVwqZEcBRkcMdYZfwUfOeIL6gGuF24uQcAhBVxiC4yAU4pRPUB9wrXAbvgIAAP//IdE4BQplbmRzdHJlYW0KZW5kb2JqCjEgMCBvYmoKPDwvVHlwZSAvUGFnZXMKL0tpZHMgWzMgMCBSIDUgMCBSIF0KL0NvdW50IDIKL01lZGlhQm94IFswIDAgNTk1LjI4IDg0MS44OV0KPj4KZW5kb2JqCjcgMCBvYmoKPDwvVHlwZSAvRm9udAovQmFzZUZvbnQgL1RpbWVzLVJvbWFuCi9TdWJ0eXBlIC9UeXBlMQovRW5jb2RpbmcgL1dpbkFuc2lFbmNvZGluZwo+PgplbmRvYmoKMiAwIG9iago8PAovUHJvY1NldCBbL1BERiAvVGV4dCAvSW1hZ2VCIC9JbWFnZUMgL0ltYWdlSV0KL0ZvbnQgPDwKL0ZkMDgzNzVmNjRlYjk4NjFjNmVhZTRkZmNmZGJkMzUwMGZiZGJlMzNlIDcgMCBSCj4+Ci9YT2JqZWN0IDw8Cj4+Ci9Db2xvclNwYWNlIDw8Cj4+Cj4+CmVuZG9iago4IDAgb2JqCjw8Ci9Qcm9kdWNlciAo/v8ARgBQAEQARgAgADEALgA3KQovQ3JlYXRpb25EYXRlIChEOjIwMjExMDA1MTMwMTIyKQovTW9kRGF0ZSAoRDoyMDIxMTAwNTEzMDEyMikKPj4KZW5kb2JqCjkgMCBvYmoKPDwKL1R5cGUgL0NhdGFsb2cKL1BhZ2VzIDEgMCBSCi9OYW1lcyA8PAovRW1iZWRkZWRGaWxlcyA8PCAvTmFtZXMgWwogIApdID4+Cj4+Cj4+CmVuZG9iagp4cmVmCjAgMTAKMDAwMDAwMDAwMCA2NTUzNSBmIAowMDAwMDAxMDk2IDAwMDAwIG4gCjAwMDAwMDEyODcgMDAwMDAgbiAKMDAwMDAwMDAwOSAwMDAwMCBuIAowMDAwMDAwMTE2IDAwMDAwIG4gCjAwMDAwMDA1NDUgMDAwMDAgbiAKMDAwMDAwMDY1MiAwMDAwMCBuIAowMDAwMDAxMTg5IDAwMDAwIG4gCjAwMDAwMDE0NDggMDAwMDAgbiAKMDAwMDAwMTU2MSAwMDAwMCBuIAp0cmFpbGVyCjw8Ci9TaXplIDEwCi9Sb290IDkgMCBSCi9JbmZvIDggMCBSCj4+CnN0YXJ0eHJlZgoxNjU4CiUlRU9GCg==", description = "Pdf файл в base64.", required = false)
  public String getFile() {
    return file;
  }

  public void setFile(String file) {
    this.file = file;
  }

  public ApiV2OrdersStickersPdfPost200ResponseData name(String name) {
    this.name = name;
    return this;
  }

  /**
   * Имя файла.
   * @return name
  */
  
  @Schema(name = "name", example = "stickers.pdf", description = "Имя файла.", required = false)
  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public ApiV2OrdersStickersPdfPost200ResponseData mimeType(String mimeType) {
    this.mimeType = mimeType;
    return this;
  }

  /**
   * MIME type файла.
   * @return mimeType
  */
  
  @Schema(name = "mimeType", example = "application/pdf", description = "MIME type файла.", required = false)
  public String getMimeType() {
    return mimeType;
  }

  public void setMimeType(String mimeType) {
    this.mimeType = mimeType;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    ApiV2OrdersStickersPdfPost200ResponseData apiV2OrdersStickersPdfPost200ResponseData = (ApiV2OrdersStickersPdfPost200ResponseData) o;
    return Objects.equals(this.file, apiV2OrdersStickersPdfPost200ResponseData.file) &&
        Objects.equals(this.name, apiV2OrdersStickersPdfPost200ResponseData.name) &&
        Objects.equals(this.mimeType, apiV2OrdersStickersPdfPost200ResponseData.mimeType);
  }

  @Override
  public int hashCode() {
    return Objects.hash(file, name, mimeType);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class ApiV2OrdersStickersPdfPost200ResponseData {\n");
    sb.append("    file: ").append(toIndentedString(file)).append("\n");
    sb.append("    name: ").append(toIndentedString(name)).append("\n");
    sb.append("    mimeType: ").append(toIndentedString(mimeType)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

