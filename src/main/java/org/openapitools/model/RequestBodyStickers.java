package org.openapitools.model;

import java.net.URI;
import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonTypeName;
import java.util.ArrayList;
import java.util.List;
import org.openapitools.jackson.nullable.JsonNullable;
import java.time.OffsetDateTime;
import javax.validation.Valid;
import javax.validation.constraints.*;
import io.swagger.v3.oas.annotations.media.Schema;


import java.util.*;
import javax.annotation.Generated;

/**
 * RequestBodyStickers
 */

@JsonTypeName("requestBodyStickers")
@Generated(value = "org.openapitools.codegen.languages.SpringCodegen", date = "2022-10-16T20:47:05.606+03:00[Europe/Moscow]")
public class RequestBodyStickers {

  @JsonProperty("orderIds")
  @Valid
  private List<Integer> orderIds = null;

  public RequestBodyStickers orderIds(List<Integer> orderIds) {
    this.orderIds = orderIds;
    return this;
  }

  public RequestBodyStickers addOrderIdsItem(Integer orderIdsItem) {
    if (this.orderIds == null) {
      this.orderIds = new ArrayList<>();
    }
    this.orderIds.add(orderIdsItem);
    return this;
  }

  /**
   * Идентификаторы сборочных заданий.
   * @return orderIds
  */
  
  @Schema(name = "orderIds", description = "Идентификаторы сборочных заданий.", required = false)
  public List<Integer> getOrderIds() {
    return orderIds;
  }

  public void setOrderIds(List<Integer> orderIds) {
    this.orderIds = orderIds;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    RequestBodyStickers requestBodyStickers = (RequestBodyStickers) o;
    return Objects.equals(this.orderIds, requestBodyStickers.orderIds);
  }

  @Override
  public int hashCode() {
    return Objects.hash(orderIds);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class RequestBodyStickers {\n");
    sb.append("    orderIds: ").append(toIndentedString(orderIds)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

