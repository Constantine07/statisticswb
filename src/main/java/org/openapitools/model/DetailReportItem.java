package org.openapitools.model;

import java.net.URI;
import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import java.math.BigDecimal;
import java.time.OffsetDateTime;
import org.springframework.format.annotation.DateTimeFormat;
import org.openapitools.jackson.nullable.JsonNullable;
import java.time.OffsetDateTime;
import javax.validation.Valid;
import javax.validation.constraints.*;
import io.swagger.v3.oas.annotations.media.Schema;


import java.util.*;
import javax.annotation.Generated;

/**
 * DetailReportItem
 */

@Generated(value = "org.openapitools.codegen.languages.SpringCodegen", date = "2022-10-16T20:47:05.606+03:00[Europe/Moscow]")
public class DetailReportItem {

  @JsonProperty("realizationreport_id")
  private Integer realizationreportId;

  @JsonProperty("date_from")
  @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
  private OffsetDateTime dateFrom;

  @JsonProperty("date_to")
  @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
  private OffsetDateTime dateTo;

  @JsonProperty("suppliercontract_code")
  private Object suppliercontractCode;

  @JsonProperty("rid")
  private Integer rid;

  @JsonProperty("rr_dt")
  @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
  private OffsetDateTime rrDt;

  @JsonProperty("rrd_id")
  private Integer rrdId;

  @JsonProperty("gi_id")
  private Integer giId;

  @JsonProperty("subject_name")
  private String subjectName;

  @JsonProperty("NM_id")
  private Integer nmId;

  @JsonProperty("brand_name")
  private String brandName;

  @JsonProperty("sa_name")
  private String saName;

  @JsonProperty("ts_name")
  private String tsName;

  @JsonProperty("barcode")
  private String barcode;

  /**
   * Тип документа
   */
  public enum DocTypeNameEnum {
    SEND("Продажа"),
    
    BACK("Возврат");

    private String value;

    DocTypeNameEnum(String value) {
      this.value = value;
    }

    @JsonValue
    public String getValue() {
      return value;
    }

    @Override
    public String toString() {
      return String.valueOf(value);
    }

    @JsonCreator
    public static DocTypeNameEnum fromValue(String value) {
      for (DocTypeNameEnum b : DocTypeNameEnum.values()) {
        if (b.value.equals(value)) {
          return b;
        }
      }
      throw new IllegalArgumentException("Unexpected value '" + value + "'");
    }
  }

  @JsonProperty("doc_type_name")
  private DocTypeNameEnum docTypeName;

  @JsonProperty("quantity")
  private Integer quantity;

  @JsonProperty("retail_price")
  private BigDecimal retailPrice;

  @JsonProperty("retail_amount")
  private BigDecimal retailAmount;

  @JsonProperty("sale_percent")
  private Integer salePercent;

  @JsonProperty("commission_percent")
  private BigDecimal commissionPercent;

  @JsonProperty("office_name")
  private String officeName;

  @JsonProperty("supplier_oper_name")
  private String supplierOperName;

  @JsonProperty("order_dt")
  @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
  private OffsetDateTime orderDt;

  @JsonProperty("sale_dt")
  @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
  private OffsetDateTime saleDt;

  @JsonProperty("shk_id")
  private Integer shkId;

  @JsonProperty("retail_price_withdisc_rub")
  private BigDecimal retailPriceWithdiscRub;

  @JsonProperty("delivery_amount")
  private Integer deliveryAmount;

  @JsonProperty("return_amount")
  private Integer returnAmount;

  @JsonProperty("delivery_rub")
  private BigDecimal deliveryRub;

  @JsonProperty("gi_box_type_name")
  private String giBoxTypeName;

  @JsonProperty("product_discount_for_report")
  private BigDecimal productDiscountForReport;

  @JsonProperty("supplier_promo")
  private BigDecimal supplierPromo;

  @JsonProperty("ppvz_spp_prc")
  private BigDecimal ppvzSppPrc;

  @JsonProperty("ppvz_kvw_prc_base")
  private BigDecimal ppvzKvwPrcBase;

  @JsonProperty("ppvz_kvw_prc")
  private BigDecimal ppvzKvwPrc;

  @JsonProperty("ppvz_sales_commission")
  private BigDecimal ppvzSalesCommission;

  @JsonProperty("ppvz_for_pay")
  private BigDecimal ppvzForPay;

  @JsonProperty("ppvz_reward")
  private BigDecimal ppvzReward;

  @JsonProperty("ppvz_vw")
  private BigDecimal ppvzVw;

  @JsonProperty("ppvz_vw_nds")
  private BigDecimal ppvzVwNds;

  @JsonProperty("ppvz_office_id")
  private Integer ppvzOfficeId;

  @JsonProperty("ppvz_office_name")
  private String ppvzOfficeName;

  @JsonProperty("ppvz_supplier_id")
  private Integer ppvzSupplierId;

  @JsonProperty("ppvz_supplier_name")
  private String ppvzSupplierName;

  @JsonProperty("ppvz_inn")
  private String ppvzInn;

  @JsonProperty("declaration_number")
  private String declarationNumber;

  @JsonProperty("sticker_id")
  private Integer stickerId;

  @JsonProperty("site_country")
  private String siteCountry;

  @JsonProperty("penalty")
  private BigDecimal penalty;

  @JsonProperty("additional_payment")
  private BigDecimal additionalPayment;

  @JsonProperty("bonus_type_name")
  private String bonusTypeName;

  @JsonProperty("srid")
  private String srid;

  public DetailReportItem realizationreportId(Integer realizationreportId) {
    this.realizationreportId = realizationreportId;
    return this;
  }

  /**
   * Номер отчета
   * @return realizationreportId
  */
  
  @Schema(name = "realizationreport_id", description = "Номер отчета", required = false)
  public Integer getRealizationreportId() {
    return realizationreportId;
  }

  public void setRealizationreportId(Integer realizationreportId) {
    this.realizationreportId = realizationreportId;
  }

  public DetailReportItem dateFrom(OffsetDateTime dateFrom) {
    this.dateFrom = dateFrom;
    return this;
  }

  /**
   * Дата начала отчетного периода <div class=\"version\">С версии 1.0.10</div>
   * @return dateFrom
  */
  @Valid 
  @Schema(name = "date_from", description = "Дата начала отчетного периода <div class=\"version\">С версии 1.0.10</div>", required = false)
  public OffsetDateTime getDateFrom() {
    return dateFrom;
  }

  public void setDateFrom(OffsetDateTime dateFrom) {
    this.dateFrom = dateFrom;
  }

  public DetailReportItem dateTo(OffsetDateTime dateTo) {
    this.dateTo = dateTo;
    return this;
  }

  /**
   * Дата конца отчетного периода <div class=\"version\">С версии 1.0.10</div>
   * @return dateTo
  */
  @Valid 
  @Schema(name = "date_to", description = "Дата конца отчетного периода <div class=\"version\">С версии 1.0.10</div>", required = false)
  public OffsetDateTime getDateTo() {
    return dateTo;
  }

  public void setDateTo(OffsetDateTime dateTo) {
    this.dateTo = dateTo;
  }

  public DetailReportItem suppliercontractCode(Object suppliercontractCode) {
    this.suppliercontractCode = suppliercontractCode;
    return this;
  }

  /**
   * Договор
   * @return suppliercontractCode
  */
  
  @Schema(name = "suppliercontract_code", description = "Договор", required = false)
  public Object getSuppliercontractCode() {
    return suppliercontractCode;
  }

  public void setSuppliercontractCode(Object suppliercontractCode) {
    this.suppliercontractCode = suppliercontractCode;
  }

  public DetailReportItem rid(Integer rid) {
    this.rid = rid;
    return this;
  }

  /**
   * Уникальный идентификатор позиции заказа
   * @return rid
  */
  
  @Schema(name = "rid", description = "Уникальный идентификатор позиции заказа", required = false)
  public Integer getRid() {
    return rid;
  }

  public void setRid(Integer rid) {
    this.rid = rid;
  }

  public DetailReportItem rrDt(OffsetDateTime rrDt) {
    this.rrDt = rrDt;
    return this;
  }

  /**
   * Дата операции. Присылается с явным указанием часового пояса.
   * @return rrDt
  */
  @Valid 
  @Schema(name = "rr_dt", description = "Дата операции. Присылается с явным указанием часового пояса.", required = false)
  public OffsetDateTime getRrDt() {
    return rrDt;
  }

  public void setRrDt(OffsetDateTime rrDt) {
    this.rrDt = rrDt;
  }

  public DetailReportItem rrdId(Integer rrdId) {
    this.rrdId = rrdId;
    return this;
  }

  /**
   * Номер строки
   * @return rrdId
  */
  
  @Schema(name = "rrd_id", description = "Номер строки", required = false)
  public Integer getRrdId() {
    return rrdId;
  }

  public void setRrdId(Integer rrdId) {
    this.rrdId = rrdId;
  }

  public DetailReportItem giId(Integer giId) {
    this.giId = giId;
    return this;
  }

  /**
   * Номер поставки
   * @return giId
  */
  
  @Schema(name = "gi_id", description = "Номер поставки", required = false)
  public Integer getGiId() {
    return giId;
  }

  public void setGiId(Integer giId) {
    this.giId = giId;
  }

  public DetailReportItem subjectName(String subjectName) {
    this.subjectName = subjectName;
    return this;
  }

  /**
   * Предмет
   * @return subjectName
  */
  
  @Schema(name = "subject_name", description = "Предмет", required = false)
  public String getSubjectName() {
    return subjectName;
  }

  public void setSubjectName(String subjectName) {
    this.subjectName = subjectName;
  }

  public DetailReportItem nmId(Integer nmId) {
    this.nmId = nmId;
    return this;
  }

  /**
   * Артикул
   * @return nmId
  */
  
  @Schema(name = "NM_id", description = "Артикул", required = false)
  public Integer getNmId() {
    return nmId;
  }

  public void setNmId(Integer nmId) {
    this.nmId = nmId;
  }

  public DetailReportItem brandName(String brandName) {
    this.brandName = brandName;
    return this;
  }

  /**
   * Бренд
   * @return brandName
  */
  
  @Schema(name = "brand_name", description = "Бренд", required = false)
  public String getBrandName() {
    return brandName;
  }

  public void setBrandName(String brandName) {
    this.brandName = brandName;
  }

  public DetailReportItem saName(String saName) {
    this.saName = saName;
    return this;
  }

  /**
   * Артикул поставщика
   * @return saName
  */
  
  @Schema(name = "sa_name", description = "Артикул поставщика", required = false)
  public String getSaName() {
    return saName;
  }

  public void setSaName(String saName) {
    this.saName = saName;
  }

  public DetailReportItem tsName(String tsName) {
    this.tsName = tsName;
    return this;
  }

  /**
   * Размер
   * @return tsName
  */
  
  @Schema(name = "ts_name", description = "Размер", required = false)
  public String getTsName() {
    return tsName;
  }

  public void setTsName(String tsName) {
    this.tsName = tsName;
  }

  public DetailReportItem barcode(String barcode) {
    this.barcode = barcode;
    return this;
  }

  /**
   * Бар-код
   * @return barcode
  */
  
  @Schema(name = "barcode", description = "Бар-код", required = false)
  public String getBarcode() {
    return barcode;
  }

  public void setBarcode(String barcode) {
    this.barcode = barcode;
  }

  public DetailReportItem docTypeName(DocTypeNameEnum docTypeName) {
    this.docTypeName = docTypeName;
    return this;
  }

  /**
   * Тип документа
   * @return docTypeName
  */
  
  @Schema(name = "doc_type_name", description = "Тип документа", required = false)
  public DocTypeNameEnum getDocTypeName() {
    return docTypeName;
  }

  public void setDocTypeName(DocTypeNameEnum docTypeName) {
    this.docTypeName = docTypeName;
  }

  public DetailReportItem quantity(Integer quantity) {
    this.quantity = quantity;
    return this;
  }

  /**
   * Количество
   * @return quantity
  */
  
  @Schema(name = "quantity", description = "Количество", required = false)
  public Integer getQuantity() {
    return quantity;
  }

  public void setQuantity(Integer quantity) {
    this.quantity = quantity;
  }

  public DetailReportItem retailPrice(BigDecimal retailPrice) {
    this.retailPrice = retailPrice;
    return this;
  }

  /**
   * Цена розничная
   * @return retailPrice
  */
  @Valid 
  @Schema(name = "retail_price", description = "Цена розничная", required = false)
  public BigDecimal getRetailPrice() {
    return retailPrice;
  }

  public void setRetailPrice(BigDecimal retailPrice) {
    this.retailPrice = retailPrice;
  }

  public DetailReportItem retailAmount(BigDecimal retailAmount) {
    this.retailAmount = retailAmount;
    return this;
  }

  /**
   * Сумма продаж (возвратов)
   * @return retailAmount
  */
  @Valid 
  @Schema(name = "retail_amount", description = "Сумма продаж (возвратов)", required = false)
  public BigDecimal getRetailAmount() {
    return retailAmount;
  }

  public void setRetailAmount(BigDecimal retailAmount) {
    this.retailAmount = retailAmount;
  }

  public DetailReportItem salePercent(Integer salePercent) {
    this.salePercent = salePercent;
    return this;
  }

  /**
   * Согласованная скидка
   * @return salePercent
  */
  
  @Schema(name = "sale_percent", description = "Согласованная скидка", required = false)
  public Integer getSalePercent() {
    return salePercent;
  }

  public void setSalePercent(Integer salePercent) {
    this.salePercent = salePercent;
  }

  public DetailReportItem commissionPercent(BigDecimal commissionPercent) {
    this.commissionPercent = commissionPercent;
    return this;
  }

  /**
   * Процент комиссии
   * @return commissionPercent
  */
  @Valid 
  @Schema(name = "commission_percent", description = "Процент комиссии", required = false)
  public BigDecimal getCommissionPercent() {
    return commissionPercent;
  }

  public void setCommissionPercent(BigDecimal commissionPercent) {
    this.commissionPercent = commissionPercent;
  }

  public DetailReportItem officeName(String officeName) {
    this.officeName = officeName;
    return this;
  }

  /**
   * Склад
   * @return officeName
  */
  
  @Schema(name = "office_name", description = "Склад", required = false)
  public String getOfficeName() {
    return officeName;
  }

  public void setOfficeName(String officeName) {
    this.officeName = officeName;
  }

  public DetailReportItem supplierOperName(String supplierOperName) {
    this.supplierOperName = supplierOperName;
    return this;
  }

  /**
   * Обоснование для оплаты
   * @return supplierOperName
  */
  
  @Schema(name = "supplier_oper_name", description = "Обоснование для оплаты", required = false)
  public String getSupplierOperName() {
    return supplierOperName;
  }

  public void setSupplierOperName(String supplierOperName) {
    this.supplierOperName = supplierOperName;
  }

  public DetailReportItem orderDt(OffsetDateTime orderDt) {
    this.orderDt = orderDt;
    return this;
  }

  /**
   * Дата заказа. Присылается с явным указанием часового пояса.
   * @return orderDt
  */
  @Valid 
  @Schema(name = "order_dt", description = "Дата заказа. Присылается с явным указанием часового пояса.", required = false)
  public OffsetDateTime getOrderDt() {
    return orderDt;
  }

  public void setOrderDt(OffsetDateTime orderDt) {
    this.orderDt = orderDt;
  }

  public DetailReportItem saleDt(OffsetDateTime saleDt) {
    this.saleDt = saleDt;
    return this;
  }

  /**
   * Дата продажи. Присылается с явным указанием часового пояса.
   * @return saleDt
  */
  @Valid 
  @Schema(name = "sale_dt", description = "Дата продажи. Присылается с явным указанием часового пояса.", required = false)
  public OffsetDateTime getSaleDt() {
    return saleDt;
  }

  public void setSaleDt(OffsetDateTime saleDt) {
    this.saleDt = saleDt;
  }

  public DetailReportItem shkId(Integer shkId) {
    this.shkId = shkId;
    return this;
  }

  /**
   * Штрих-код
   * @return shkId
  */
  
  @Schema(name = "shk_id", description = "Штрих-код", required = false)
  public Integer getShkId() {
    return shkId;
  }

  public void setShkId(Integer shkId) {
    this.shkId = shkId;
  }

  public DetailReportItem retailPriceWithdiscRub(BigDecimal retailPriceWithdiscRub) {
    this.retailPriceWithdiscRub = retailPriceWithdiscRub;
    return this;
  }

  /**
   * Цена розничная с учетом согласованной скидки
   * @return retailPriceWithdiscRub
  */
  @Valid 
  @Schema(name = "retail_price_withdisc_rub", description = "Цена розничная с учетом согласованной скидки", required = false)
  public BigDecimal getRetailPriceWithdiscRub() {
    return retailPriceWithdiscRub;
  }

  public void setRetailPriceWithdiscRub(BigDecimal retailPriceWithdiscRub) {
    this.retailPriceWithdiscRub = retailPriceWithdiscRub;
  }

  public DetailReportItem deliveryAmount(Integer deliveryAmount) {
    this.deliveryAmount = deliveryAmount;
    return this;
  }

  /**
   * Количество доставок
   * @return deliveryAmount
  */
  
  @Schema(name = "delivery_amount", description = "Количество доставок", required = false)
  public Integer getDeliveryAmount() {
    return deliveryAmount;
  }

  public void setDeliveryAmount(Integer deliveryAmount) {
    this.deliveryAmount = deliveryAmount;
  }

  public DetailReportItem returnAmount(Integer returnAmount) {
    this.returnAmount = returnAmount;
    return this;
  }

  /**
   * Количество возвратов
   * @return returnAmount
  */
  
  @Schema(name = "return_amount", description = "Количество возвратов", required = false)
  public Integer getReturnAmount() {
    return returnAmount;
  }

  public void setReturnAmount(Integer returnAmount) {
    this.returnAmount = returnAmount;
  }

  public DetailReportItem deliveryRub(BigDecimal deliveryRub) {
    this.deliveryRub = deliveryRub;
    return this;
  }

  /**
   * Стоимость логистики
   * @return deliveryRub
  */
  @Valid 
  @Schema(name = "delivery_rub", description = "Стоимость логистики", required = false)
  public BigDecimal getDeliveryRub() {
    return deliveryRub;
  }

  public void setDeliveryRub(BigDecimal deliveryRub) {
    this.deliveryRub = deliveryRub;
  }

  public DetailReportItem giBoxTypeName(String giBoxTypeName) {
    this.giBoxTypeName = giBoxTypeName;
    return this;
  }

  /**
   * Тип коробов
   * @return giBoxTypeName
  */
  
  @Schema(name = "gi_box_type_name", description = "Тип коробов", required = false)
  public String getGiBoxTypeName() {
    return giBoxTypeName;
  }

  public void setGiBoxTypeName(String giBoxTypeName) {
    this.giBoxTypeName = giBoxTypeName;
  }

  public DetailReportItem productDiscountForReport(BigDecimal productDiscountForReport) {
    this.productDiscountForReport = productDiscountForReport;
    return this;
  }

  /**
   * Согласованный продуктовый дисконт
   * @return productDiscountForReport
  */
  @Valid 
  @Schema(name = "product_discount_for_report", description = "Согласованный продуктовый дисконт", required = false)
  public BigDecimal getProductDiscountForReport() {
    return productDiscountForReport;
  }

  public void setProductDiscountForReport(BigDecimal productDiscountForReport) {
    this.productDiscountForReport = productDiscountForReport;
  }

  public DetailReportItem supplierPromo(BigDecimal supplierPromo) {
    this.supplierPromo = supplierPromo;
    return this;
  }

  /**
   * Промокод
   * @return supplierPromo
  */
  @Valid 
  @Schema(name = "supplier_promo", description = "Промокод", required = false)
  public BigDecimal getSupplierPromo() {
    return supplierPromo;
  }

  public void setSupplierPromo(BigDecimal supplierPromo) {
    this.supplierPromo = supplierPromo;
  }

  public DetailReportItem ppvzSppPrc(BigDecimal ppvzSppPrc) {
    this.ppvzSppPrc = ppvzSppPrc;
    return this;
  }

  /**
   * Скидка постоянного покупателя
   * @return ppvzSppPrc
  */
  @Valid 
  @Schema(name = "ppvz_spp_prc", description = "Скидка постоянного покупателя", required = false)
  public BigDecimal getPpvzSppPrc() {
    return ppvzSppPrc;
  }

  public void setPpvzSppPrc(BigDecimal ppvzSppPrc) {
    this.ppvzSppPrc = ppvzSppPrc;
  }

  public DetailReportItem ppvzKvwPrcBase(BigDecimal ppvzKvwPrcBase) {
    this.ppvzKvwPrcBase = ppvzKvwPrcBase;
    return this;
  }

  /**
   * Размер кВВ без НДС, % базовый
   * @return ppvzKvwPrcBase
  */
  @Valid 
  @Schema(name = "ppvz_kvw_prc_base", description = "Размер кВВ без НДС, % базовый", required = false)
  public BigDecimal getPpvzKvwPrcBase() {
    return ppvzKvwPrcBase;
  }

  public void setPpvzKvwPrcBase(BigDecimal ppvzKvwPrcBase) {
    this.ppvzKvwPrcBase = ppvzKvwPrcBase;
  }

  public DetailReportItem ppvzKvwPrc(BigDecimal ppvzKvwPrc) {
    this.ppvzKvwPrc = ppvzKvwPrc;
    return this;
  }

  /**
   * Итоговый кВВ без НДС, %
   * @return ppvzKvwPrc
  */
  @Valid 
  @Schema(name = "ppvz_kvw_prc", description = "Итоговый кВВ без НДС, %", required = false)
  public BigDecimal getPpvzKvwPrc() {
    return ppvzKvwPrc;
  }

  public void setPpvzKvwPrc(BigDecimal ppvzKvwPrc) {
    this.ppvzKvwPrc = ppvzKvwPrc;
  }

  public DetailReportItem ppvzSalesCommission(BigDecimal ppvzSalesCommission) {
    this.ppvzSalesCommission = ppvzSalesCommission;
    return this;
  }

  /**
   * Вознаграждение с продаж до вычета услуг поверенного, без НДС
   * @return ppvzSalesCommission
  */
  @Valid 
  @Schema(name = "ppvz_sales_commission", description = "Вознаграждение с продаж до вычета услуг поверенного, без НДС", required = false)
  public BigDecimal getPpvzSalesCommission() {
    return ppvzSalesCommission;
  }

  public void setPpvzSalesCommission(BigDecimal ppvzSalesCommission) {
    this.ppvzSalesCommission = ppvzSalesCommission;
  }

  public DetailReportItem ppvzForPay(BigDecimal ppvzForPay) {
    this.ppvzForPay = ppvzForPay;
    return this;
  }

  /**
   * К перечислению продавцу за реализованный товар
   * @return ppvzForPay
  */
  @Valid 
  @Schema(name = "ppvz_for_pay", description = "К перечислению продавцу за реализованный товар", required = false)
  public BigDecimal getPpvzForPay() {
    return ppvzForPay;
  }

  public void setPpvzForPay(BigDecimal ppvzForPay) {
    this.ppvzForPay = ppvzForPay;
  }

  public DetailReportItem ppvzReward(BigDecimal ppvzReward) {
    this.ppvzReward = ppvzReward;
    return this;
  }

  /**
   * Возмещение расходов услуг поверенного
   * @return ppvzReward
  */
  @Valid 
  @Schema(name = "ppvz_reward", description = "Возмещение расходов услуг поверенного", required = false)
  public BigDecimal getPpvzReward() {
    return ppvzReward;
  }

  public void setPpvzReward(BigDecimal ppvzReward) {
    this.ppvzReward = ppvzReward;
  }

  public DetailReportItem ppvzVw(BigDecimal ppvzVw) {
    this.ppvzVw = ppvzVw;
    return this;
  }

  /**
   * Вознаграждение WB без НДС
   * @return ppvzVw
  */
  @Valid 
  @Schema(name = "ppvz_vw", description = "Вознаграждение WB без НДС", required = false)
  public BigDecimal getPpvzVw() {
    return ppvzVw;
  }

  public void setPpvzVw(BigDecimal ppvzVw) {
    this.ppvzVw = ppvzVw;
  }

  public DetailReportItem ppvzVwNds(BigDecimal ppvzVwNds) {
    this.ppvzVwNds = ppvzVwNds;
    return this;
  }

  /**
   * НДС с вознаграждения WB
   * @return ppvzVwNds
  */
  @Valid 
  @Schema(name = "ppvz_vw_nds", description = "НДС с вознаграждения WB", required = false)
  public BigDecimal getPpvzVwNds() {
    return ppvzVwNds;
  }

  public void setPpvzVwNds(BigDecimal ppvzVwNds) {
    this.ppvzVwNds = ppvzVwNds;
  }

  public DetailReportItem ppvzOfficeId(Integer ppvzOfficeId) {
    this.ppvzOfficeId = ppvzOfficeId;
    return this;
  }

  /**
   * Номер офиса
   * @return ppvzOfficeId
  */
  
  @Schema(name = "ppvz_office_id", description = "Номер офиса", required = false)
  public Integer getPpvzOfficeId() {
    return ppvzOfficeId;
  }

  public void setPpvzOfficeId(Integer ppvzOfficeId) {
    this.ppvzOfficeId = ppvzOfficeId;
  }

  public DetailReportItem ppvzOfficeName(String ppvzOfficeName) {
    this.ppvzOfficeName = ppvzOfficeName;
    return this;
  }

  /**
   * Наименование офиса доставки
   * @return ppvzOfficeName
  */
  
  @Schema(name = "ppvz_office_name", description = "Наименование офиса доставки", required = false)
  public String getPpvzOfficeName() {
    return ppvzOfficeName;
  }

  public void setPpvzOfficeName(String ppvzOfficeName) {
    this.ppvzOfficeName = ppvzOfficeName;
  }

  public DetailReportItem ppvzSupplierId(Integer ppvzSupplierId) {
    this.ppvzSupplierId = ppvzSupplierId;
    return this;
  }

  /**
   * Номер партнера
   * @return ppvzSupplierId
  */
  
  @Schema(name = "ppvz_supplier_id", description = "Номер партнера", required = false)
  public Integer getPpvzSupplierId() {
    return ppvzSupplierId;
  }

  public void setPpvzSupplierId(Integer ppvzSupplierId) {
    this.ppvzSupplierId = ppvzSupplierId;
  }

  public DetailReportItem ppvzSupplierName(String ppvzSupplierName) {
    this.ppvzSupplierName = ppvzSupplierName;
    return this;
  }

  /**
   * Партнер
   * @return ppvzSupplierName
  */
  
  @Schema(name = "ppvz_supplier_name", description = "Партнер", required = false)
  public String getPpvzSupplierName() {
    return ppvzSupplierName;
  }

  public void setPpvzSupplierName(String ppvzSupplierName) {
    this.ppvzSupplierName = ppvzSupplierName;
  }

  public DetailReportItem ppvzInn(String ppvzInn) {
    this.ppvzInn = ppvzInn;
    return this;
  }

  /**
   * ИНН партнера
   * @return ppvzInn
  */
  
  @Schema(name = "ppvz_inn", description = "ИНН партнера", required = false)
  public String getPpvzInn() {
    return ppvzInn;
  }

  public void setPpvzInn(String ppvzInn) {
    this.ppvzInn = ppvzInn;
  }

  public DetailReportItem declarationNumber(String declarationNumber) {
    this.declarationNumber = declarationNumber;
    return this;
  }

  /**
   * Номер таможенной декларации
   * @return declarationNumber
  */
  
  @Schema(name = "declaration_number", description = "Номер таможенной декларации", required = false)
  public String getDeclarationNumber() {
    return declarationNumber;
  }

  public void setDeclarationNumber(String declarationNumber) {
    this.declarationNumber = declarationNumber;
  }

  public DetailReportItem stickerId(Integer stickerId) {
    this.stickerId = stickerId;
    return this;
  }

  /**
   * Цифровое значение стикера, который клеится на товар в процессе сборки заказа по системе Маркетплейс.
   * @return stickerId
  */
  
  @Schema(name = "sticker_id", description = "Цифровое значение стикера, который клеится на товар в процессе сборки заказа по системе Маркетплейс.", required = false)
  public Integer getStickerId() {
    return stickerId;
  }

  public void setStickerId(Integer stickerId) {
    this.stickerId = stickerId;
  }

  public DetailReportItem siteCountry(String siteCountry) {
    this.siteCountry = siteCountry;
    return this;
  }

  /**
   * Страна продажи
   * @return siteCountry
  */
  
  @Schema(name = "site_country", description = "Страна продажи", required = false)
  public String getSiteCountry() {
    return siteCountry;
  }

  public void setSiteCountry(String siteCountry) {
    this.siteCountry = siteCountry;
  }

  public DetailReportItem penalty(BigDecimal penalty) {
    this.penalty = penalty;
    return this;
  }

  /**
   * Штрафы
   * @return penalty
  */
  @Valid 
  @Schema(name = "penalty", description = "Штрафы", required = false)
  public BigDecimal getPenalty() {
    return penalty;
  }

  public void setPenalty(BigDecimal penalty) {
    this.penalty = penalty;
  }

  public DetailReportItem additionalPayment(BigDecimal additionalPayment) {
    this.additionalPayment = additionalPayment;
    return this;
  }

  /**
   * Доплаты
   * @return additionalPayment
  */
  @Valid 
  @Schema(name = "additional_payment", description = "Доплаты", required = false)
  public BigDecimal getAdditionalPayment() {
    return additionalPayment;
  }

  public void setAdditionalPayment(BigDecimal additionalPayment) {
    this.additionalPayment = additionalPayment;
  }

  public DetailReportItem bonusTypeName(String bonusTypeName) {
    this.bonusTypeName = bonusTypeName;
    return this;
  }

  /**
   * Обоснование штрафов и доплат. <br> Поле будет в ответе, если заполнены(о) поля `penalty` или `additional_payment`. 
   * @return bonusTypeName
  */
  
  @Schema(name = "bonus_type_name", description = "Обоснование штрафов и доплат. <br> Поле будет в ответе, если заполнены(о) поля `penalty` или `additional_payment`. ", required = false)
  public String getBonusTypeName() {
    return bonusTypeName;
  }

  public void setBonusTypeName(String bonusTypeName) {
    this.bonusTypeName = bonusTypeName;
  }

  public DetailReportItem srid(String srid) {
    this.srid = srid;
    return this;
  }

  /**
   * Уникальный идентификатор заказа, функционально аналогичный `odid`/`rid`.  Данный параметр введен в июле'22 и в течение переходного периода может быть заполнен не во всех ответах. Примечание для работающих по системе Маркетплейс: `srid` равен `rid` в ответе на метод `GET /api/v2/orders`. 
   * @return srid
  */
  
  @Schema(name = "srid", description = "Уникальный идентификатор заказа, функционально аналогичный `odid`/`rid`.  Данный параметр введен в июле'22 и в течение переходного периода может быть заполнен не во всех ответах. Примечание для работающих по системе Маркетплейс: `srid` равен `rid` в ответе на метод `GET /api/v2/orders`. ", required = false)
  public String getSrid() {
    return srid;
  }

  public void setSrid(String srid) {
    this.srid = srid;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    DetailReportItem detailReportItem = (DetailReportItem) o;
    return Objects.equals(this.realizationreportId, detailReportItem.realizationreportId) &&
        Objects.equals(this.dateFrom, detailReportItem.dateFrom) &&
        Objects.equals(this.dateTo, detailReportItem.dateTo) &&
        Objects.equals(this.suppliercontractCode, detailReportItem.suppliercontractCode) &&
        Objects.equals(this.rid, detailReportItem.rid) &&
        Objects.equals(this.rrDt, detailReportItem.rrDt) &&
        Objects.equals(this.rrdId, detailReportItem.rrdId) &&
        Objects.equals(this.giId, detailReportItem.giId) &&
        Objects.equals(this.subjectName, detailReportItem.subjectName) &&
        Objects.equals(this.nmId, detailReportItem.nmId) &&
        Objects.equals(this.brandName, detailReportItem.brandName) &&
        Objects.equals(this.saName, detailReportItem.saName) &&
        Objects.equals(this.tsName, detailReportItem.tsName) &&
        Objects.equals(this.barcode, detailReportItem.barcode) &&
        Objects.equals(this.docTypeName, detailReportItem.docTypeName) &&
        Objects.equals(this.quantity, detailReportItem.quantity) &&
        Objects.equals(this.retailPrice, detailReportItem.retailPrice) &&
        Objects.equals(this.retailAmount, detailReportItem.retailAmount) &&
        Objects.equals(this.salePercent, detailReportItem.salePercent) &&
        Objects.equals(this.commissionPercent, detailReportItem.commissionPercent) &&
        Objects.equals(this.officeName, detailReportItem.officeName) &&
        Objects.equals(this.supplierOperName, detailReportItem.supplierOperName) &&
        Objects.equals(this.orderDt, detailReportItem.orderDt) &&
        Objects.equals(this.saleDt, detailReportItem.saleDt) &&
        Objects.equals(this.shkId, detailReportItem.shkId) &&
        Objects.equals(this.retailPriceWithdiscRub, detailReportItem.retailPriceWithdiscRub) &&
        Objects.equals(this.deliveryAmount, detailReportItem.deliveryAmount) &&
        Objects.equals(this.returnAmount, detailReportItem.returnAmount) &&
        Objects.equals(this.deliveryRub, detailReportItem.deliveryRub) &&
        Objects.equals(this.giBoxTypeName, detailReportItem.giBoxTypeName) &&
        Objects.equals(this.productDiscountForReport, detailReportItem.productDiscountForReport) &&
        Objects.equals(this.supplierPromo, detailReportItem.supplierPromo) &&
        Objects.equals(this.ppvzSppPrc, detailReportItem.ppvzSppPrc) &&
        Objects.equals(this.ppvzKvwPrcBase, detailReportItem.ppvzKvwPrcBase) &&
        Objects.equals(this.ppvzKvwPrc, detailReportItem.ppvzKvwPrc) &&
        Objects.equals(this.ppvzSalesCommission, detailReportItem.ppvzSalesCommission) &&
        Objects.equals(this.ppvzForPay, detailReportItem.ppvzForPay) &&
        Objects.equals(this.ppvzReward, detailReportItem.ppvzReward) &&
        Objects.equals(this.ppvzVw, detailReportItem.ppvzVw) &&
        Objects.equals(this.ppvzVwNds, detailReportItem.ppvzVwNds) &&
        Objects.equals(this.ppvzOfficeId, detailReportItem.ppvzOfficeId) &&
        Objects.equals(this.ppvzOfficeName, detailReportItem.ppvzOfficeName) &&
        Objects.equals(this.ppvzSupplierId, detailReportItem.ppvzSupplierId) &&
        Objects.equals(this.ppvzSupplierName, detailReportItem.ppvzSupplierName) &&
        Objects.equals(this.ppvzInn, detailReportItem.ppvzInn) &&
        Objects.equals(this.declarationNumber, detailReportItem.declarationNumber) &&
        Objects.equals(this.stickerId, detailReportItem.stickerId) &&
        Objects.equals(this.siteCountry, detailReportItem.siteCountry) &&
        Objects.equals(this.penalty, detailReportItem.penalty) &&
        Objects.equals(this.additionalPayment, detailReportItem.additionalPayment) &&
        Objects.equals(this.bonusTypeName, detailReportItem.bonusTypeName) &&
        Objects.equals(this.srid, detailReportItem.srid);
  }

  @Override
  public int hashCode() {
    return Objects.hash(realizationreportId, dateFrom, dateTo, suppliercontractCode, rid, rrDt, rrdId, giId, subjectName, nmId, brandName, saName, tsName, barcode, docTypeName, quantity, retailPrice, retailAmount, salePercent, commissionPercent, officeName, supplierOperName, orderDt, saleDt, shkId, retailPriceWithdiscRub, deliveryAmount, returnAmount, deliveryRub, giBoxTypeName, productDiscountForReport, supplierPromo, ppvzSppPrc, ppvzKvwPrcBase, ppvzKvwPrc, ppvzSalesCommission, ppvzForPay, ppvzReward, ppvzVw, ppvzVwNds, ppvzOfficeId, ppvzOfficeName, ppvzSupplierId, ppvzSupplierName, ppvzInn, declarationNumber, stickerId, siteCountry, penalty, additionalPayment, bonusTypeName, srid);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class DetailReportItem {\n");
    sb.append("    realizationreportId: ").append(toIndentedString(realizationreportId)).append("\n");
    sb.append("    dateFrom: ").append(toIndentedString(dateFrom)).append("\n");
    sb.append("    dateTo: ").append(toIndentedString(dateTo)).append("\n");
    sb.append("    suppliercontractCode: ").append(toIndentedString(suppliercontractCode)).append("\n");
    sb.append("    rid: ").append(toIndentedString(rid)).append("\n");
    sb.append("    rrDt: ").append(toIndentedString(rrDt)).append("\n");
    sb.append("    rrdId: ").append(toIndentedString(rrdId)).append("\n");
    sb.append("    giId: ").append(toIndentedString(giId)).append("\n");
    sb.append("    subjectName: ").append(toIndentedString(subjectName)).append("\n");
    sb.append("    nmId: ").append(toIndentedString(nmId)).append("\n");
    sb.append("    brandName: ").append(toIndentedString(brandName)).append("\n");
    sb.append("    saName: ").append(toIndentedString(saName)).append("\n");
    sb.append("    tsName: ").append(toIndentedString(tsName)).append("\n");
    sb.append("    barcode: ").append(toIndentedString(barcode)).append("\n");
    sb.append("    docTypeName: ").append(toIndentedString(docTypeName)).append("\n");
    sb.append("    quantity: ").append(toIndentedString(quantity)).append("\n");
    sb.append("    retailPrice: ").append(toIndentedString(retailPrice)).append("\n");
    sb.append("    retailAmount: ").append(toIndentedString(retailAmount)).append("\n");
    sb.append("    salePercent: ").append(toIndentedString(salePercent)).append("\n");
    sb.append("    commissionPercent: ").append(toIndentedString(commissionPercent)).append("\n");
    sb.append("    officeName: ").append(toIndentedString(officeName)).append("\n");
    sb.append("    supplierOperName: ").append(toIndentedString(supplierOperName)).append("\n");
    sb.append("    orderDt: ").append(toIndentedString(orderDt)).append("\n");
    sb.append("    saleDt: ").append(toIndentedString(saleDt)).append("\n");
    sb.append("    shkId: ").append(toIndentedString(shkId)).append("\n");
    sb.append("    retailPriceWithdiscRub: ").append(toIndentedString(retailPriceWithdiscRub)).append("\n");
    sb.append("    deliveryAmount: ").append(toIndentedString(deliveryAmount)).append("\n");
    sb.append("    returnAmount: ").append(toIndentedString(returnAmount)).append("\n");
    sb.append("    deliveryRub: ").append(toIndentedString(deliveryRub)).append("\n");
    sb.append("    giBoxTypeName: ").append(toIndentedString(giBoxTypeName)).append("\n");
    sb.append("    productDiscountForReport: ").append(toIndentedString(productDiscountForReport)).append("\n");
    sb.append("    supplierPromo: ").append(toIndentedString(supplierPromo)).append("\n");
    sb.append("    ppvzSppPrc: ").append(toIndentedString(ppvzSppPrc)).append("\n");
    sb.append("    ppvzKvwPrcBase: ").append(toIndentedString(ppvzKvwPrcBase)).append("\n");
    sb.append("    ppvzKvwPrc: ").append(toIndentedString(ppvzKvwPrc)).append("\n");
    sb.append("    ppvzSalesCommission: ").append(toIndentedString(ppvzSalesCommission)).append("\n");
    sb.append("    ppvzForPay: ").append(toIndentedString(ppvzForPay)).append("\n");
    sb.append("    ppvzReward: ").append(toIndentedString(ppvzReward)).append("\n");
    sb.append("    ppvzVw: ").append(toIndentedString(ppvzVw)).append("\n");
    sb.append("    ppvzVwNds: ").append(toIndentedString(ppvzVwNds)).append("\n");
    sb.append("    ppvzOfficeId: ").append(toIndentedString(ppvzOfficeId)).append("\n");
    sb.append("    ppvzOfficeName: ").append(toIndentedString(ppvzOfficeName)).append("\n");
    sb.append("    ppvzSupplierId: ").append(toIndentedString(ppvzSupplierId)).append("\n");
    sb.append("    ppvzSupplierName: ").append(toIndentedString(ppvzSupplierName)).append("\n");
    sb.append("    ppvzInn: ").append(toIndentedString(ppvzInn)).append("\n");
    sb.append("    declarationNumber: ").append(toIndentedString(declarationNumber)).append("\n");
    sb.append("    stickerId: ").append(toIndentedString(stickerId)).append("\n");
    sb.append("    siteCountry: ").append(toIndentedString(siteCountry)).append("\n");
    sb.append("    penalty: ").append(toIndentedString(penalty)).append("\n");
    sb.append("    additionalPayment: ").append(toIndentedString(additionalPayment)).append("\n");
    sb.append("    bonusTypeName: ").append(toIndentedString(bonusTypeName)).append("\n");
    sb.append("    srid: ").append(toIndentedString(srid)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

